package com.example.pa165_project_movies.common.util;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HexFormat;
import java.util.UUID;

public final class UUID5 {
    public static final String NAMESPACE_DNS = "6ba7b810-9dad-11d1-80b4-00c04fd430c8";
    public static final String NAMESPACE_URL = "6ba7b811-9dad-11d1-80b4-00c04fd430c8";
    public static final String NAMESPACE_OID = "6ba7b812-9dad-11d1-80b4-00c04fd430c8";
    public static final String NAMESPACE_X500 = "6ba7b814-9dad-11d1-80b4-00c04fd430c8";

    public static UUID from(UUID namespace, String name) throws NoSuchAlgorithmException {
        return from(namespace.toString(), name);
    }

    public static UUID from(String namespace, String name) throws NoSuchAlgorithmException {
        String namespaceWithoutDashes = namespace.replace("-", "");
        assert namespaceWithoutDashes.length() == 32;
        byte[] namespaceBytes = HexFormat.of().parseHex(namespaceWithoutDashes);
        byte[] nameBytes = name.getBytes(StandardCharsets.UTF_8);

        MessageDigest hash = MessageDigest.getInstance("SHA-1");
        hash.update(namespaceBytes);
        hash.update(nameBytes);
        byte[] digest = hash.digest();

        // mask bits 12 to 15 of time_hi_and_version
        // and set them to 0101 (5)
        digest[6] &= 0b0000_1111;
        digest[6] |= 0b0101_0000;
        // mask bits 7 and 6 of clock_seq_hi_and_reserved
        // and set them to 1 and 0, respectively
        digest[8] &= 0b0011_1111;
        digest[8] |= 0b1000_0000;

        ByteBuffer buffer = ByteBuffer.wrap(digest);
        return new UUID(buffer.getLong(), buffer.getLong());
    }
}
