package com.example.pa165_project_movies.common.config;

public final class SecurityConst {

    public static final String UUID5_NAMESPACE_EMAIL = "f59e7f8d-1a56-4126-9e30-39ec5749819a";

    public static final String SECURITY_SCHEME_NAME = "MUNI";
    public static final String AUTHORIZATION_URL = "https://oidc.muni.cz/oidc/authorize";
    public static final String TOKE_URL = "https://oidc.muni.cz/oidc/token";
}
