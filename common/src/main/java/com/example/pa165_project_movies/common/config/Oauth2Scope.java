package com.example.pa165_project_movies.common.config;

public final class Oauth2Scope {
    public static final String AUTH_READ = "SCOPE_test_read";
    public static final String AUTH_WRITE = "SCOPE_test_write";
    public static final String ADMIN = "SCOPE_test_1";

    // _S for short
    public static final String AUTH_READ_S = "test_read";
    public static final String AUTH_WRITE_S = "test_write";
    public static final String ADMIN_S = "test_1";

    private Oauth2Scope() {}
}
