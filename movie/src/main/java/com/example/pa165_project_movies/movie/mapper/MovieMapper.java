package com.example.pa165_project_movies.movie.mapper;

import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.service.MovieCategoryService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = {MovieCategoryMapper.class, MovieCategoryService.class})
public interface MovieMapper {

    MovieDetailDto mapToDetailViewDto(Movie movie);

    List<MovieDetailDto> mapToList(List<Movie> movies);

    Movie mapToEntity(MovieDetailDto movie);

    @Mapping(source = "categories", target = "categories")
    Movie mapToEntity(MovieNewDto movie);

    default Page<MovieDetailDto> mapToPageDto(Page<Movie> movies) {
        return new PageImpl<>(
                mapToList(movies.getContent()),
                movies.getPageable(),
                movies.getTotalPages()
        );
    }
}
