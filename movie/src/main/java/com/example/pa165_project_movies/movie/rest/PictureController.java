package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.facade.PictureFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.pictures}")
public class PictureController {

    private final PictureFacade pictureFacade;

    @Autowired
    public PictureController(PictureFacade pictureFacade) {
        this.pictureFacade = pictureFacade;
    }

    @GetMapping
    public ResponseEntity<Page<PictureDto>> getMoviePictures(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(pictureFacade.findAll(pageable));
    }

    @Operation(
            summary = "Get movie picture by id",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "entity not found"),
            }
    )
    @GetMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<PictureDto> getPictureById(@PathVariable UUID id) {
        return ResponseEntity.ok(pictureFacade.findById(id));
    }

    @Operation(
            summary = "Create new movie picture",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<PictureDto> createPicture(@Valid @RequestBody PictureCreateDto pictureCreateDto) {
        return new ResponseEntity<>(pictureFacade.create(pictureCreateDto), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update movie picture",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<PictureDto> updatePicture(@PathVariable UUID id, @RequestBody PictureDto picture) {
        return ResponseEntity.ok(pictureFacade.update(id, picture));
    }

    @Operation(
            summary = "Delete movie picture",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<Void> deletePicture(@PathVariable UUID id) {
        pictureFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}
