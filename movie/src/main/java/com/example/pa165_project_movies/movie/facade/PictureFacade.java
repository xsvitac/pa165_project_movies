package com.example.pa165_project_movies.movie.facade;

import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.mapper.PictureMapper;
import com.example.pa165_project_movies.movie.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class PictureFacade {
    private final PictureService pictureService;
    private final PictureMapper pictureMapper;

    @Autowired
    public PictureFacade(PictureService pictureService, PictureMapper pictureMapper) {
        this.pictureService = pictureService;
        this.pictureMapper = pictureMapper;
    }

    @Transactional(readOnly = true)
    public Page<PictureDto> findAll(Pageable pageable) {
        return pictureMapper.mapToPageDto(pictureService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public PictureDto findById(UUID id) {
        return pictureMapper.mapToDto(pictureService.findById(id));
    }

    public PictureDto create(PictureCreateDto picture) {
        return pictureMapper.mapToDto(pictureService.create(pictureMapper.mapToEntity(picture)));
    }

    public PictureDto update(UUID id, PictureDto picture) {
        picture.setId(id);
        return pictureMapper.mapToDto(pictureService.update(pictureMapper.mapToEntity(picture)));
    }

    public void delete(UUID id) {
        pictureService.delete(pictureService.findById(id));
    }
}
