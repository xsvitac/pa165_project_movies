package com.example.pa165_project_movies.movie.dto.filterDto;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class MovieFilterDto {

    private String title;
    private String description;
    private OffsetDateTime dateOfCreationFrom;
    private OffsetDateTime dateOfCreationTo;
    private List<ObjectIdFilterDto> movieCategoryIdFilter;
    // private List<ObjectIdFilter> personnelIdFilter;

    public MovieFilterDto() {
        title = "";
        description = "";
        dateOfCreationFrom = null;
        dateOfCreationTo = null;
        movieCategoryIdFilter = new ArrayList<>();
        // personnelIdFilter = new ArrayList<>();
    }

    public boolean isEmpty() {
        return (
                (title == null || title.isEmpty())
                        && (description == null || description.isEmpty())
                        && dateOfCreationFrom == null
                        && dateOfCreationTo == null
                        && (movieCategoryIdFilter == null || movieCategoryIdFilter.isEmpty())
                        // && (personnelIdFilter == null || personnelIdFilter.isEmpty())
        );
    }
}
