package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "Movie category", description = "Category which the movie belongs to")
@Data
public class MovieCategoryDto {
    @Schema(description = "ID of the movie category")
    @NotNull
    private UUID id;

    @Schema(description = "Movie category name")
    @NotBlank
    private String name;

    @Schema(description = "Movie category description")
    @NotBlank
    private String description;
}
