package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.facade.MovieCategoryFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.categories}")
public class MovieCategoryController {

    private final MovieCategoryFacade movieCategoryFacade;

    @Autowired
    public MovieCategoryController(MovieCategoryFacade movieCategoryFacade) {
        this.movieCategoryFacade = movieCategoryFacade;
    }

    @Operation(summary = "Paged movie categories")
    @GetMapping
    public ResponseEntity<Page<MovieCategoryDto>> getMovieCategories(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(movieCategoryFacade.findAll(pageable));
    }

    @Operation(
            summary = "Get movie category by id",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "entity not found"),
            }
    )
    @GetMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<MovieCategoryDto> getMovieCategoryById(@PathVariable UUID id) {
        return ResponseEntity.ok(movieCategoryFacade.findById(id));
    }

    @Operation(
            summary = "Create new movie category",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<MovieCategoryDto> createMovieCategory(@Valid @RequestBody MovieCategoryCreateDto movieCategoryCreateDto) {
        return new ResponseEntity<>(movieCategoryFacade.create(movieCategoryCreateDto), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update movie category",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<MovieCategoryDto> updateMovieCategory(@PathVariable UUID id, @RequestBody MovieCategoryDto movieCategory) {
        return ResponseEntity.ok(movieCategoryFacade.update(id, movieCategory));
    }

    @Operation(
            summary = "Delete movie category",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteMovieCategory(@PathVariable UUID id) {
        movieCategoryFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}
