package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.dto.filterDto.ObjectIdFilterDto;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import org.springframework.data.jpa.domain.Specification;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MovieSpecification {

    public static final String DATE_OF_CREATION = "dateOfCreation";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String PERSONNEL_IDS = "personnelIds";
    public static final String CATEGORIES = "categories";

    private MovieSpecification() {
        //empty
    }

    public static Specification<Movie> filterBy(MovieFilterDto movieFilter) {
        if (movieFilter.isEmpty())
            return null;
        return Specification.where(containsInTitle(movieFilter.getTitle()))
                .and(containsInDescription(movieFilter.getDescription()))
                .and(hasDateOfCreationGreaterThan(movieFilter.getDateOfCreationFrom()))
                .and(hasDateOfCreationLessThan(movieFilter.getDateOfCreationTo()))
                .and(movieCategoryIdsFilter(movieFilter.getMovieCategoryIdFilter()));
    }

    private static Specification<Movie> containsInTitle(String title) {
        return ((root, query, cb) -> title == null || title.isEmpty() ?
                cb.conjunction() : cb.like(cb.lower(root.get(TITLE)), "%" + title.toLowerCase() + "%"));
    }

    private static Specification<Movie> containsInDescription(String description) {
        return ((root, query, cb) -> description == null || description.isEmpty() ?
                cb.conjunction() : cb.like(cb.lower(root.get(DESCRIPTION)), "%" + description.toLowerCase() + "%"));
    }

    private static Specification<Movie> movieCategoryIdsFilter(List<ObjectIdFilterDto> movieCategoryIdFilter) {
        if (movieCategoryIdFilter == null || movieCategoryIdFilter.isEmpty()) {
            return (root, query, cb) -> cb.conjunction();
        }
        List<UUID> includedMovieCategories = movieCategoryIdFilter.stream()
                .filter(ObjectIdFilterDto::isToBeIncluded)
                .map(ObjectIdFilterDto::getId).toList();
        List<UUID> excludedMovieCategories = movieCategoryIdFilter.stream()
                .filter(item -> !item.isToBeIncluded())
                .map(ObjectIdFilterDto::getId).toList();
        return (
                (root, query, cb) -> {
                    List<Predicate> includePredicates = new ArrayList<>();
                    List<Predicate> excludePredicates = new ArrayList<>();

                    for (UUID excludeId : excludedMovieCategories) {
                        Subquery<UUID> subquery = query.subquery(UUID.class);
                        Root<Movie> subqueryMovie = subquery.from(Movie.class);
                        Join<MovieCategory, Movie> subqueryCategory = subqueryMovie.join("categories");

                        subquery.select(subqueryMovie.get("id")).where(cb.equal(subqueryCategory.get("id"), excludeId));
                        excludePredicates.add(cb.not(cb.in(root.get("id")).value(subquery)));
                    }

                    for (UUID includeId : includedMovieCategories) {
                        Subquery<UUID> subquery = query.subquery(UUID.class);
                        Root<Movie> subqueryMovie = subquery.from(Movie.class);
                        Join<MovieCategory, Movie> subqueryCategory = subqueryMovie.join("categories");

                        subquery.select(subqueryMovie.get("id")).where(cb.equal(subqueryCategory.get("id"), includeId));
                        includePredicates.add(cb.in(root.get("id")).value(subquery));
                    }

                    Predicate excludeOrIgnore = excludedMovieCategories.isEmpty()
                            ? cb.equal(cb.literal(1), cb.literal(1))
                            : cb.and(excludePredicates.toArray(new Predicate[excludePredicates.size()]));
                    Predicate includeOrIgnore = includedMovieCategories.isEmpty()
                            ? cb.equal(cb.literal(1), cb.literal(1))
                            : cb.and(includePredicates.toArray(new Predicate[includePredicates.size()]));
                    return cb.and(excludeOrIgnore, includeOrIgnore);
                });
    }

    private static Specification<Movie> hasDateOfCreationGreaterThan(OffsetDateTime dateOfCreationFrom) {
        return (root, query, cb) -> dateOfCreationFrom == null ?
                cb.conjunction() : cb.greaterThan(root.get(DATE_OF_CREATION), dateOfCreationFrom);
    }

    private static Specification<Movie> hasDateOfCreationLessThan(OffsetDateTime dateOfCreationTo) {
        return (root, query, cb) -> dateOfCreationTo == null ?
                cb.conjunction() : cb.lessThan(root.get(DATE_OF_CREATION), dateOfCreationTo);
    }

}
