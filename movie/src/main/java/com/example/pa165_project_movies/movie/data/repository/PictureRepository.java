package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PictureRepository extends JpaRepository<Picture, UUID> {
}
