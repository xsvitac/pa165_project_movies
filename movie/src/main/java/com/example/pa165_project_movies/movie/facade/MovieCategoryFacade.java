package com.example.pa165_project_movies.movie.facade;

import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.mapper.MovieCategoryMapper;
import com.example.pa165_project_movies.movie.service.MovieCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class MovieCategoryFacade {
    private final MovieCategoryService movieCategoryService;
    private final MovieCategoryMapper movieCategoryMapper;

    @Autowired
    public MovieCategoryFacade(MovieCategoryService movieCategoryService, MovieCategoryMapper movieCategoryMapper) {
        this.movieCategoryService = movieCategoryService;
        this.movieCategoryMapper = movieCategoryMapper;
    }

    @Transactional(readOnly = true)
    public Page<MovieCategoryDto> findAll(Pageable pageable) {
        return movieCategoryMapper.mapToPageDto(movieCategoryService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public MovieCategoryDto findById(UUID id) {
        return movieCategoryMapper.mapToDto(movieCategoryService.findById(id));
    }

    public MovieCategoryDto create(MovieCategoryCreateDto movieCategory) {
        return movieCategoryMapper.mapToDto(movieCategoryService.create(movieCategoryMapper.mapToEntity(movieCategory)));
    }

    public MovieCategoryDto update(UUID id, MovieCategoryDto movieCategory) {
        movieCategory.setId(id);
        return movieCategoryMapper.mapToDto(movieCategoryService.update(movieCategoryMapper.mapToEntity(movieCategory)));
    }

    public void delete(UUID id) {
        movieCategoryService.delete(movieCategoryService.findById(id));
    }

    public void deleteAll(){
        movieCategoryService.deleteAll();
    }
}
