package com.example.pa165_project_movies.movie.rest.exceptionhandling;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.dto.ApiErrorDto;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.util.UrlPathHelper;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomRestGlobalExceptionHandling {
    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<ApiErrorDto> handleResourceNotFound(final ResourceNotFoundException ex, final HttpServletRequest request) {
        final ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiErrorDto, new HttpHeaders(), apiErrorDto.getStatus());
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ApiErrorDto> handleAccessDenied(final AccessDeniedException ex, final HttpServletRequest request) {
        final ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiErrorDto, new HttpHeaders(), apiErrorDto.getStatus());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiErrorDto> handleAllExceptions(final Exception ex, final HttpServletRequest request) {
        final ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.INTERNAL_SERVER_ERROR, getInitialException(ex).getLocalizedMessage(), URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiErrorDto, new HttpHeaders(), apiErrorDto.getStatus());
    }

    private Exception getInitialException(Exception ex) {
        while (ex.getCause() != null) {
            ex = (Exception) ex.getCause();
        }
        return ex;
    }
}
