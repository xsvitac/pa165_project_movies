package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Picture;
import com.example.pa165_project_movies.movie.data.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class PictureService {
    private final PictureRepository pictureRepository;

    @Autowired
    public PictureService(PictureRepository pictureRepository) {
        this.pictureRepository = pictureRepository;
    }

    @Transactional(readOnly = true)
    public Picture findById(UUID id) {
        return pictureRepository
                .findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException("No existing movie has \"" + id + "\"  this id"));
    }

    public Picture create(Picture picture) {
        return pictureRepository.save(picture);
    }

    public Picture update(Picture picture) {
        Picture savedPicture = this.findById(picture.getId());
        // Dto does not have from which movie picture is from, so we have to keep the one from saved entity
        savedPicture.setPictureAlt(picture.getPictureAlt());
        savedPicture.setPictureUrl(picture.getPictureUrl());
        return pictureRepository.save(savedPicture);
    }

    public void delete(Picture picture) {
        pictureRepository.delete(picture);
    }

    public Page<Picture> findAll(Pageable pageable) {
        return pictureRepository.findAll(pageable);
    }
}
