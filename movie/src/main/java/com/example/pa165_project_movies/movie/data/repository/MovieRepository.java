package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

@Repository
public interface MovieRepository extends JpaRepository<Movie, UUID>, JpaSpecificationExecutor<Movie> {

    
    Page<Movie> findAll(@Nullable Specification<Movie> spec, @Nonnull Pageable pageable);

    @Override
    @Modifying
    @Query(value = "UPDATE MOVIE SET POSTER_ID = null;DELETE FROM MOVIE_PERSONNEL_IDS; DELETE FROM PICTURE; DELETE FROM MOVIE_CATEGORIES; DELETE FROM MOVIE", nativeQuery = true)
    void deleteAll();
}
