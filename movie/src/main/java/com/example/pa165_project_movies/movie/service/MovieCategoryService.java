package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import com.example.pa165_project_movies.movie.data.repository.MovieCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class MovieCategoryService {
    private final MovieCategoryRepository movieCategoryRepository;

    @Autowired
    public MovieCategoryService(MovieCategoryRepository movieCategoryRepository) {
        this.movieCategoryRepository = movieCategoryRepository;
    }

    @Transactional(readOnly = true)
    public MovieCategory findById(UUID id) {
        MovieCategory movieCategory = movieCategoryRepository
                .findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException("No existing movie has \"" + id + "\"  this id"));
        return movieCategory;
    }

    public MovieCategory create(MovieCategory movieCategory) {
        return movieCategoryRepository.save(movieCategory);
    }

    public MovieCategory update(MovieCategory movieCategory) {
        MovieCategory savedMovieCategory = this.findById(movieCategory.getId()); // dto Does not have date of creation which is @NotNull, so we must fill it
        savedMovieCategory.setName(movieCategory.getName());
        savedMovieCategory.setDescription(movieCategory.getDescription());
        return movieCategoryRepository.save(savedMovieCategory);
    }

    public void delete(MovieCategory movieCategory) {
        // https://stackoverflow.com/questions/32354474/manytomany-with-cascade-cascadetype-remove-removes-associations-and-entities
        Set<Movie> movieSet = movieCategory.getMoviesInCategory();
        if(movieSet != null){
            for (Movie movie : movieSet){
                movie.getCategories().remove(movieCategory);
            }
            movieCategory.getMoviesInCategory().clear();
        }
        movieCategoryRepository.delete(movieCategory);
    }

    public void deleteAll(){
        movieCategoryRepository.deleteAll();
    }

    public Page<MovieCategory> findAll(Pageable pageable) {
        return movieCategoryRepository.findAll(pageable);
    }
}
