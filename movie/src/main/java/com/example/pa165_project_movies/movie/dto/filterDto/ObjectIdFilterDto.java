package com.example.pa165_project_movies.movie.dto.filterDto;

import java.util.UUID;

public class ObjectIdFilterDto {
    private UUID id;
    private boolean isToBeIncluded;

    public ObjectIdFilterDto(UUID id, boolean isToBeIncluded) {
        this.id = id;
        this.isToBeIncluded = isToBeIncluded;
    }

    public ObjectIdFilterDto() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean isToBeIncluded() {
        return isToBeIncluded;
    }

    public void setToBeIncluded(boolean toBeIncluded) {
        isToBeIncluded = toBeIncluded;
    }
}
