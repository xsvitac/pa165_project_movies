package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Schema(title = "New empty movie", description = "Used when creating a new movie")
@Data
public class MovieNewDto {
    @Schema(description = "Movie title")
    @NotBlank
    private String title;

    @Schema(description = "Movie description")
    @NotBlank
    private String description;

    @Schema(description = "Set of category UUIDs which will be linked to the movie")
    private Set<UUID> categories;
}
