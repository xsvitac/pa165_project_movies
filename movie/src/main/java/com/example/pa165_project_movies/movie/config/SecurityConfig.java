package com.example.pa165_project_movies.movie.config;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Security configuration has to be in separate file,
 * otherwise @DataJpaTest fails to create context
 */
@Configuration
public class SecurityConfig {

    @Value("${rest.path.movies}")
    private URI moviesRestPath;

    @Value("${rest.path.categories}")
    private URI categoriesRestPath;

    @Value("${rest.path.pictures}")
    private URI picturesRestPath;

    @Value("${rest.path.dbclear}")
    private URI dbClearRestPath;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, UriComponentsBuilder.fromUri(moviesRestPath).path("/**").build().toString()).permitAll()
                        .requestMatchers(HttpMethod.GET, UriComponentsBuilder.fromUri(categoriesRestPath).path("/**").build().toString()).permitAll()
                        .requestMatchers(HttpMethod.GET, UriComponentsBuilder.fromUri(picturesRestPath).path("/**").build().toString()).permitAll()

                        .requestMatchers(UriComponentsBuilder.fromUri(moviesRestPath).path("/**").build().toString()).hasAuthority(Oauth2Scope.ADMIN)
                        .requestMatchers(UriComponentsBuilder.fromUri(categoriesRestPath).path("/**").build().toString()).hasAuthority(Oauth2Scope.ADMIN)
                        .requestMatchers(UriComponentsBuilder.fromUri(picturesRestPath).path("/**").build().toString()).hasAuthority(Oauth2Scope.ADMIN)
                        .requestMatchers(UriComponentsBuilder.fromUri(dbClearRestPath).path("/**").build().toString()).hasAuthority(Oauth2Scope.ADMIN)

                        .anyRequest().permitAll())
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .csrf(AbstractHttpConfigurer::disable);
        return httpSecurity.build();
    }

    /**
     * Add security definitions to generated openapi.yaml.
     */
    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> openApi.getComponents()

                .addSecuritySchemes(SecurityConst.SECURITY_SCHEME_NAME,
                        new SecurityScheme()
                                .type(SecurityScheme.Type.OAUTH2)
                                .description("get access token with OAuth 2 Authorization Code Grant")
                                .flows(new OAuthFlows()
                                        .authorizationCode(new OAuthFlow()
                                                .authorizationUrl(SecurityConst.AUTHORIZATION_URL)
                                                .tokenUrl(SecurityConst.TOKE_URL)
                                                .scopes(new Scopes()
                                                        .addString(Oauth2Scope.AUTH_READ_S, "for this module does nothing")
                                                        .addString(Oauth2Scope.AUTH_WRITE_S, "for this module does nothing")
                                                        .addString(Oauth2Scope.ADMIN_S, "creating, updating, deleting movies")
                                                )
                                        )
                                )
                );
    }
}
