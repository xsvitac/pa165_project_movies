package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.facade.MovieFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.movies}")
public class MovieController {

    private final MovieFacade movieFacade;

    @Autowired
    public MovieController(MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    @GetMapping
    public ResponseEntity<Page<MovieDetailDto>> getMovies(
            @ParameterObject Pageable pageable,
            @RequestBody Optional<MovieFilterDto> movieFilter
    ) {
        if (movieFilter.isPresent()) {
            return ResponseEntity.ok(movieFacade.findAll(pageable, movieFilter.get()));
        }
        return ResponseEntity.ok(movieFacade.findAll(pageable, new MovieFilterDto()));
    }


    @Operation(
            summary = "Get movie by id",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "entity not found"),
            }
    )
    @GetMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<MovieDetailDto> getMovieById(@PathVariable UUID id) {
        return ResponseEntity.ok(movieFacade.findById(id));
    }

    @Operation(
            summary = "Create new movie",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<MovieDetailDto> createMovie(
            @Valid @RequestBody MovieNewDto movie
    ) {
        return new ResponseEntity<>(movieFacade.create(movie), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update movie",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<MovieDetailDto> updateMovie(
            @PathVariable UUID id,
            @RequestBody  MovieDetailDto movie
    ) {
        return ResponseEntity.ok(movieFacade.update(id, movie));
    }

    @Operation(
            summary = "Delete movie",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id:^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$}")
    public ResponseEntity<Void> deleteMovie(@PathVariable UUID id) {
        movieFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}
