package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "Involved person", description = "Person involved in a movie's making")
@Data
public class PersonDto {
    @Schema(description = "ID of the person")
    @NotNull
    private UUID id;

    @Schema(description = "Person's given (first) name")
    @NotBlank
    private String givenName;

    @Schema(description = "Person's family (last) name")
    @NotBlank
    private String familyName;

    @Schema(description = "Person's description")
    @NotBlank
    private String description;
}
