package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Schema(title = "New movie category", description = "New category which the movie can belong to")
@Data
public class MovieCategoryCreateDto {
    @Schema(description = "Movie category name")
    @NotBlank
    private String name;

    @Schema(description = "Movie category description")
    @NotBlank
    private String description;
}
