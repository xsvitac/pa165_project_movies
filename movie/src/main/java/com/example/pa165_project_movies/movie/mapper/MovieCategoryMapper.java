package com.example.pa165_project_movies.movie.mapper;

import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface MovieCategoryMapper {

    MovieCategoryDto mapToDto(MovieCategory movieCategory);

    Set<MovieCategoryDto> mapToSet(Set<MovieCategory> movieCategories);

    List<MovieCategoryDto> mapToList(List<MovieCategory> movieCategories);

//    default Set<UUID> mapToIds(Set<MovieCategory> movieCategories) {
//        return movieCategories.stream().map(MovieCategory::getId).collect(Collectors.toSet());
//    }

    MovieCategory mapToEntity(MovieCategoryDto movieCategory);

    MovieCategory mapToEntity(MovieCategoryCreateDto movieCategory);

    default Page<MovieCategoryDto> mapToPageDto(Page<MovieCategory> movieCategoryPage) {
        return new PageImpl<>(this.mapToList(movieCategoryPage.getContent()), movieCategoryPage.getPageable(), movieCategoryPage.getTotalPages());
    }
}
