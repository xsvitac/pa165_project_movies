package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.data.repository.MovieRepository;
import com.example.pa165_project_movies.movie.data.repository.MovieSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class MovieService {
    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Transactional(readOnly = true)
    public Movie findById(UUID id) {
        Movie movie = movieRepository
                .findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException("No existing movie has \"" + id + "\"  this id"));
        return movie;
    }

    @Transactional(readOnly = true)
    public Page<Movie> findAll(Pageable pageable, MovieFilterDto movieFilter) {
        if (movieFilter == null || movieFilter.isEmpty()) {
            return movieRepository.findAll(pageable);
        }
        return movieRepository.findAll(
                MovieSpecification.filterBy(movieFilter),
                pageable
        );
    }

    public Movie create(Movie movie) {
        return movieRepository.save(movie);
    }

    public Movie update(Movie movie) {
        this.findById(movie.getId());
        return movieRepository.save(movie);
    }

    public void delete(Movie movie) {
        movieRepository.delete(movie);
    }

    public void deleteAll(){
        movieRepository.deleteAll();
    }
}
