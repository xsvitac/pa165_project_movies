package com.example.pa165_project_movies.movie.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "Picture", description = "Picture associated with a movie")
@Data
public class PictureDto {
    @Schema(description = "ID of the picture")
    @NotNull
    private UUID id;

    @Schema(description = "URL of the picture")
    @NotBlank
    private String pictureUrl;

    @Schema(description = "Description of the picture in case of its URL not working")
    @NotBlank
    private String pictureAlt;

    public PictureDto() {
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public PictureDto(@JsonProperty("id")UUID id,
                      @JsonProperty("pictureUrl")String pictureUrl,
                      @JsonProperty("pictureAlt")String pictureAlt) {
        this.id = id;
        this.pictureUrl = pictureUrl;
        this.pictureAlt = pictureAlt;
    }
}
