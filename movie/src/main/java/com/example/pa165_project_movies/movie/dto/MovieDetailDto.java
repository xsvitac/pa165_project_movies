package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Schema(title = "Detailed movie information", description = "Contains movie's detailed information")
@Data
public class MovieDetailDto {
    @Schema(description = "ID of the movie")
    @NotNull
    private UUID id;

    @Schema(description = "Movie title")
    @NotBlank
    private String title;

    @Schema(description = "Movie description")
    @NotBlank
    private String description;

    @Schema(description = "Set of categories to which the movie belongs")
    @NotNull
    private Set<MovieCategoryDto> categories;

    @Schema(description = "Date of movie addition to the system")
    @NotNull
    private OffsetDateTime dateOfCreation;

    @Schema(description = "Date of last change to the movie record")
    @NotNull
    private OffsetDateTime dateOfLastChange;

    @Schema(description = "List of categories to which the movie belongs")
    @NotNull
    private List<PictureDto> pictures;

    @Schema(description = "The movie's poster")
    @Nullable
    private PictureDto poster;

    @Schema(description = "People involved in the movie's making")
    @NotNull
    private List<PersonRoleDto> personnel;
}
