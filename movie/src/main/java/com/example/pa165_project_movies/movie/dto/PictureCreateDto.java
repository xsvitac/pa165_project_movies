package com.example.pa165_project_movies.movie.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "New picture", description = "New picture associated with a movie")
@Data
public class PictureCreateDto {
    @Schema(description = "URL of the picture")
    @NotBlank
    private String pictureUrl;

    @Schema(description = "Description of the picture in case of its URL not working")
    @NotBlank
    private String pictureAlt;

    @Schema(description = "Id of the film which this picture belongs to")
    @NotNull
    private UUID movieId;
}
