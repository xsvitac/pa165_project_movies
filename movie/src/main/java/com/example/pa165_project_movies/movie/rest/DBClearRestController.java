package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.movie.facade.MovieCategoryFacade;
import com.example.pa165_project_movies.movie.facade.MovieFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${rest.path.dbclear}", produces = MediaType.APPLICATION_JSON_VALUE)
public class DBClearRestController {

    private final MovieCategoryFacade movieCategoryFacade;
    private final MovieFacade movieFacade;

    @Autowired
    public DBClearRestController(MovieCategoryFacade movieCategoryFacade, MovieFacade movieFacade) {
        this.movieCategoryFacade = movieCategoryFacade;
        this.movieFacade = movieFacade;
    }

    @Operation(
            summary = "Delete all movies categories and pictures in the database",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping
    public ResponseEntity<Void> deleteMoviesAndCategories() {
        movieFacade.deleteAll();
        movieCategoryFacade.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
