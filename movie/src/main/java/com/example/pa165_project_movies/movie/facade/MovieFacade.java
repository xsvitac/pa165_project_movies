package com.example.pa165_project_movies.movie.facade;

import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.mapper.MovieMapper;
import com.example.pa165_project_movies.movie.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class MovieFacade {
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    @Autowired
    public MovieFacade(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @Transactional(readOnly = true)
    public Page<MovieDetailDto> findAll(
            Pageable pageable,
            MovieFilterDto movieFilter
    ) {
        return movieMapper.mapToPageDto(
                movieService.findAll(pageable, movieFilter)
        );
    }

    @Transactional(readOnly = true)
    public MovieDetailDto findById(UUID id) {
        final Movie movie = movieService.findById(id);
        final MovieDetailDto movieDto = movieMapper.mapToDetailViewDto(movie);
        return movieDto;
    }

    public MovieDetailDto create(MovieNewDto movie) {
        return movieMapper.mapToDetailViewDto(movieService.create(movieMapper.mapToEntity(movie)));
    }

    public MovieDetailDto update(UUID id, MovieDetailDto movie) {
        movie.setId(id);
        return movieMapper.mapToDetailViewDto(movieService.update(movieMapper.mapToEntity(movie)));
    }

    public void delete(UUID id) {
        movieService.delete(movieService.findById(id));
    }

    public void deleteAll(){
        movieService.deleteAll();
    }
}
