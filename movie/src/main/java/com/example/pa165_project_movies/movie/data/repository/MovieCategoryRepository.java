package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieCategoryRepository extends JpaRepository<MovieCategory, UUID> {
    @Override
    @Modifying
    @Query(value = "DELETE FROM MOVIE_CATEGORIES; DELETE FROM MOVIE_CATEGORY", nativeQuery = true)
    void deleteAll();
}
