package com.example.pa165_project_movies.movie.mapper;

import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.data.model.Picture;
import com.example.pa165_project_movies.movie.service.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = {MovieService.class})
public interface PictureMapper {

    PictureDto mapToDto(Picture picture);

    List<PictureDto> mapToList(List<Picture> pictures);

    Picture mapToEntity(PictureDto picture);

    @Mapping(source = "movieId", target = "fromMovie")
    Picture mapToEntity(PictureCreateDto picture);

    default Page<PictureDto> mapToPageDto(Page<Picture> picturePage) {
        return new PageImpl<>(this.mapToList(picturePage.getContent()), picturePage.getPageable(), picturePage.getTotalPages());
    }
}
