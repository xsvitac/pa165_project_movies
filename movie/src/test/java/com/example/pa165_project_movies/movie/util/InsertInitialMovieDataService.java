package com.example.pa165_project_movies.movie.util;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import com.example.pa165_project_movies.movie.data.model.Picture;
import com.example.pa165_project_movies.movie.data.repository.MovieCategoryRepository;
import com.example.pa165_project_movies.movie.data.repository.MovieRepository;
import com.example.pa165_project_movies.movie.data.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class InsertInitialMovieDataService {

    private final MovieRepository movieRepository;
    private final MovieCategoryRepository movieCategoryRepository;
    private final PictureRepository pictureRepository;

    // wanted to manually set id, but that didn't go so well, so static variable it is
    public static UUID ACTION_MOVIE_CATEGORY;// = UUID.fromString("71d633aa-5aae-4b0d-9b65-9509d9d9ba74");
    public static UUID SCI_FI_MOVIE_CATEGORY;// = UUID.fromString("4cd641bf-365e-4caf-9418-8e8df00770bd");
    public static UUID ROMANCE_MOVIE_CATEGORY;// = UUID.fromString("b4cfab9c-8a1c-4c54-b1c9-db2a9b5004b7");
    public static UUID DRAMA_MOVIE_CATEGORY;// = UUID.fromString("12c75932-88bc-461a-b089-b1569690ce50");
    public static UUID HORROR_MOVIE_CATEGORY;// = UUID.fromString("0c474ade-d3df-4972-87bd-d28007cd56bd");
    public static UUID COMEDY_MOVIE_CATEGORY;// = UUID.fromString("6b6cd88f-7ed6-40a1-a019-d930919a6ba0");
    public static UUID FROZEN_MOVIE;// = UUID.fromString("076325e1-6353-4241-b921-d2e06d70c083");
    public static UUID FROZEN_MOVIE_PICTURE;// = UUID.fromString("37eb6f1e-276b-42fc-9831-b9c41b68aa63");
    public static UUID SCARY_MOVIE;// = UUID.fromString("8eedb8cc-d5c1-46c0-9c92-5935537eb969");
    public static UUID SCARY_MOVIE_PICTURE;// = UUID.fromString("8e357512-bb1f-46fb-a875-cbef3e33ef21");

    @Autowired
    public InsertInitialMovieDataService(MovieRepository movieRepository,
                                         MovieCategoryRepository movieCategoryRepository,
                                         PictureRepository pictureRepository) {
        this.movieRepository = movieRepository;
        this.movieCategoryRepository = movieCategoryRepository;
        this.pictureRepository = pictureRepository;
    }

    @PostConstruct
    public void insertDummyData(){
        insertMovieCategories();
        insertMovies();
        insertPosters();
    }

    public void removeDummyData(){
        pictureRepository.deleteAll();
        movieRepository.deleteAll();
        movieCategoryRepository.deleteAll();
    }

    private void insertMovieCategories(){
        MovieCategory actionCategory = new MovieCategory();
        actionCategory.setName("Action");
        actionCategory.setDescription("Movie full of action");
        movieCategoryRepository.save(actionCategory);
        ACTION_MOVIE_CATEGORY = actionCategory.getId();

        MovieCategory sciFiCategory = new MovieCategory();
        sciFiCategory.setName("Sci-Fi");
        sciFiCategory.setDescription("Movie full of science fiction");
        movieCategoryRepository.save(sciFiCategory);
        SCI_FI_MOVIE_CATEGORY = sciFiCategory.getId();

        MovieCategory romanceCategory = new MovieCategory();
        romanceCategory.setName("Romance");
        romanceCategory.setDescription("Movie full of romance");
        movieCategoryRepository.save(romanceCategory);
        ROMANCE_MOVIE_CATEGORY = romanceCategory.getId();

        MovieCategory dramaCategory = new MovieCategory();
        dramaCategory.setName("Drama");
        dramaCategory.setDescription("Movie full of drama");
        movieCategoryRepository.save(dramaCategory);
        DRAMA_MOVIE_CATEGORY = dramaCategory.getId();

        MovieCategory horrorCategory = new MovieCategory();
        horrorCategory.setName("Horror");
        horrorCategory.setDescription("Movie full of scary stunts");
        movieCategoryRepository.save(horrorCategory);
        HORROR_MOVIE_CATEGORY = horrorCategory.getId();

        MovieCategory comedyCategory = new MovieCategory();
        comedyCategory.setName("Comedy");
        comedyCategory.setDescription("Movie full of comedy stunts");
        movieCategoryRepository.save(comedyCategory);
        COMEDY_MOVIE_CATEGORY = comedyCategory.getId();
    }

    private void insertMovies(){
        Movie frozenMovie = new Movie();
        frozenMovie.setTitle("Frozen");
        frozenMovie.setDescription("Some movie about a scary chick");
        frozenMovie.setCategories(Set.of(
                movieCategoryRepository.findById(HORROR_MOVIE_CATEGORY).orElseThrow(ResourceNotFoundException::new),
                movieCategoryRepository.findById(DRAMA_MOVIE_CATEGORY).orElseThrow(ResourceNotFoundException::new)
        ));
        movieRepository.save(frozenMovie);
        FROZEN_MOVIE = frozenMovie.getId();

        Movie scaryMovie = new Movie();
        scaryMovie.setTitle("Scary movie");
        scaryMovie.setDescription("Great film with everything you could want from a film");
        scaryMovie.setCategories(Set.of(
                movieCategoryRepository.findById(ROMANCE_MOVIE_CATEGORY).orElseThrow(ResourceNotFoundException::new),
                movieCategoryRepository.findById(SCI_FI_MOVIE_CATEGORY).orElseThrow(ResourceNotFoundException::new),
                movieCategoryRepository.findById(ACTION_MOVIE_CATEGORY).orElseThrow(ResourceNotFoundException::new)
        ));
        movieRepository.save(scaryMovie);
        SCARY_MOVIE = scaryMovie.getId();
    }

    private void insertPosters(){
        Picture frozenPicture = new Picture();
        frozenPicture.setPictureAlt("Poster of the most frozen movie");
        frozenPicture.setPictureUrl("https://image.pmgstatic.com/cache/resized/w140/files/images/film/posters/168/138/168138064_9ed6lb.jpg");
        Movie fromMovie = movieRepository.findById(FROZEN_MOVIE).orElseThrow(ResourceNotFoundException::new);
        frozenPicture.setFromMovie(fromMovie);
        frozenPicture.setPosterOfMovie(fromMovie);
        pictureRepository.save(frozenPicture);
        FROZEN_MOVIE_PICTURE = frozenPicture.getId();

        Picture scaryPicture = new Picture();
        scaryPicture.setPictureAlt("Poster of the most wholesome movie");
        scaryPicture.setPictureUrl("https://image.pmgstatic.com/cache/resized/w140/files/images/film/posters/162/262/162262742_d3fa7c.jpg");
        fromMovie = movieRepository.findById(SCARY_MOVIE).orElseThrow(ResourceNotFoundException::new);
        scaryPicture.setFromMovie(fromMovie);
        scaryPicture.setPosterOfMovie(fromMovie);
        pictureRepository.save(scaryPicture);
        SCARY_MOVIE_PICTURE = scaryPicture.getId();
    }
}
