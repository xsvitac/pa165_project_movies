package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.data.repository.MovieRepository;
import com.example.pa165_project_movies.movie.util.InsertInitialMovieDataService;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
public class DBClearRestControllerITest {
    @Autowired
    private MockMvc mockMvc;

    @Value("${rest.path.dbclear}")
    private String restPath;

    @Autowired
    private InsertInitialMovieDataService insertInitialMovieDataService;

    @Autowired
    private MovieRepository movieRepository;

    @BeforeEach
    void resetPersistentState() {
        insertInitialMovieDataService.removeDummyData();
        insertInitialMovieDataService.insertDummyData();
    }

    @Test
    @Transactional
    void deleteMoviesAndCategories_noInput_resultSuccess() throws Exception {

        mockMvc.perform(
                        delete(restPath)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        assertThat(movieRepository.findAll().size()).isEqualTo(0);
    }
}

