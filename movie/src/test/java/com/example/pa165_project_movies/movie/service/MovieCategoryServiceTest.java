package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import com.example.pa165_project_movies.movie.data.repository.MovieCategoryRepository;
import com.example.pa165_project_movies.movie.service.MovieCategoryService;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MovieCategoryServiceTest {
    @Mock
    private MovieCategoryRepository movieCategoryRepository;

    @InjectMocks
    private MovieCategoryService movieCategoryService;

    @Test
    void findById_movieCategoryFound_returnsMovieCategory() {
        // Arrange
        Mockito.when(movieCategoryRepository.findById(new UUID(0, 1))).thenReturn(Optional.ofNullable(TestDataFactory.movieCategoryEntity));

        // Act
        MovieCategory foundEntity = movieCategoryService.findById(new UUID(0, 1));

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.movieCategoryEntity);
    }

    @Test
    void findById_movieCategoryNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieCategoryService.findById(new UUID(0, 1)))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void create_movieCategoryCreated_returnsMovieCategory() {
        // Arrange
        Mockito.when(movieCategoryRepository.save(TestDataFactory.movieCategoryEntity)).thenReturn(TestDataFactory.movieCategoryEntity);

        // Act
        MovieCategory createdEntity = movieCategoryService.create(TestDataFactory.movieCategoryEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieCategoryEntity);
    }

    @Test
    void create_movieCategoryNotCreated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieCategoryService.create(TestDataFactory.movieCategoryEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void update_movieCategoryUpdated_movieCategoryReturned() {
        // Arrange
        Mockito.when(movieCategoryRepository.findById(TestDataFactory.movieCategoryEntity.getId())).thenReturn(Optional.ofNullable(TestDataFactory.movieCategoryEntity));
        Mockito.when(movieCategoryRepository.save(TestDataFactory.movieCategoryEntity)).thenReturn(TestDataFactory.movieCategoryEntity);

        // Act
        MovieCategory createdEntity = movieCategoryService.update(TestDataFactory.movieCategoryEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieCategoryEntity);
    }

    @Test
    void update_movieCategoryUpdated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieCategoryService.update(TestDataFactory.movieCategoryEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void delete_movieCategoryDeleted_doesNothing() {
        // Arrange
        Mockito.doNothing().when(movieCategoryRepository).delete(TestDataFactory.movieCategoryEntity);

        // Act
        movieCategoryService.delete(TestDataFactory.movieCategoryEntity);

        // Assert
        Mockito.verify(movieCategoryRepository, Mockito.times(1)).delete(TestDataFactory.movieCategoryEntity);
    }

    @Test
    void deleteAll_noReturn() {
        // Arrange

        Mockito.doNothing().when(movieCategoryRepository).deleteAll();

        // Act
        movieCategoryService.deleteAll();

        // Assert
        Mockito.verify(movieCategoryRepository, Mockito.times(1)).deleteAll();
    }
}
