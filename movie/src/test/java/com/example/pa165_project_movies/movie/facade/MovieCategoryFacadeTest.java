package com.example.pa165_project_movies.movie.facade;


import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import com.example.pa165_project_movies.movie.mapper.MovieCategoryMapper;
import com.example.pa165_project_movies.movie.service.MovieCategoryService;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class MovieCategoryFacadeTest {

    @Mock
    private MovieCategoryService movieCategoryService;
    @Mock
    private MovieCategoryMapper movieCategoryMapper;
    @InjectMocks
    private MovieCategoryFacade movieCategoryFacade;

    @Test
    void findById_movieCategoryFound_returnsMovieCategoryDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(movieCategoryService.findById(uuid)).thenReturn(TestDataFactory.movieCategoryEntity);
        Mockito.when(movieCategoryMapper.mapToDto(any())).thenReturn(TestDataFactory.movieCategoryDto);

        // Act
        MovieCategoryDto foundEntity = movieCategoryFacade.findById(uuid);

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.movieCategoryDto);
    }

    @Test
    void findAll_movieCategoriesExist_returnsPageOfMovieCategories() {
        // Arrange
        Page<MovieCategory> movieCategories = Mockito.mock(Page.class);
        Page<MovieCategoryDto> movieCategoryDtos = Mockito.mock(Page.class);
        Mockito.when(movieCategoryService.findAll(Mockito.any(Pageable.class))).thenReturn(movieCategories);
        Mockito.when(movieCategoryMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(movieCategoryDtos);

        // Act
        Page<MovieCategoryDto> foundDtos = movieCategoryFacade.findAll(PageRequest.of(0, 20));

        // Assert
        assertThat(foundDtos).isEqualTo(movieCategoryDtos).isNotNull();
    }

    @Test
    void findAll_movieCategoriesExist_returnsPageOfMovieCategoriesWithData() {
        // Arrange
        Page<MovieCategory> movieCategories = Mockito.mock(Page.class);
        Page<MovieCategoryDto> movieCategoryDtos = Mockito.mock(Page.class);
        Mockito.when(movieCategoryService.findAll(Mockito.any(Pageable.class))).thenReturn(movieCategories);
        Mockito.when(movieCategoryMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(movieCategoryDtos);

        // Act
        Page<MovieCategoryDto> foundDtos = movieCategoryFacade.findAll(PageRequest.of(0, 1));

        // Assert
        assertThat(foundDtos).isEqualTo(movieCategoryDtos);
    }


    @Test
    void create_movieCategoryCrated_returnsMovieCategoryCreateDto() {
        // Arrange
        Mockito.when(movieCategoryService.create(any())).thenReturn(TestDataFactory.movieCategoryEntity);
        Mockito.when(movieCategoryMapper.mapToDto(any())).thenReturn(TestDataFactory.movieCategoryDto);

        // Act
        MovieCategoryDto createdEntity = movieCategoryFacade.create(TestDataFactory.movieCategoryCreateDto);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieCategoryDto);
    }

    @Test
    void update_movieCategoryUpdated_returnsMovieCategoryDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(movieCategoryService.update(any())).thenReturn(TestDataFactory.movieCategoryEntity);
        Mockito.when(movieCategoryMapper.mapToDto(any())).thenReturn(TestDataFactory.movieCategoryDto);

        // Act
        MovieCategoryDto updatedEntity = movieCategoryFacade.update(uuid, TestDataFactory.movieCategoryDto);

        // Assert
        assertThat(updatedEntity).isEqualTo(TestDataFactory.movieCategoryDto);
    }

    @Test
    void delete_movieCategoryDeleted_doesNothing() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.doNothing().when(movieCategoryService).delete(any());

        // Act
        movieCategoryFacade.delete(uuid);

        // Assert
        Mockito.verify(movieCategoryService, Mockito.times(1)).findById(uuid);
        Mockito.verify(movieCategoryService, Mockito.times(1)).delete(any());
    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(movieCategoryService).deleteAll();

        // Act
        movieCategoryFacade.deleteAll();

        // Assert
        Mockito.verify(movieCategoryService, Mockito.times(1)).deleteAll();
    }
}

