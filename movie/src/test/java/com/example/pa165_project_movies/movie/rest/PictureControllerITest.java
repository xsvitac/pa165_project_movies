package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.util.InsertInitialMovieDataService;
import com.example.pa165_project_movies.movie.util.ObjectConverter;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PictureControllerITest {
    @Autowired
    private MockMvc mockMvc;

    @Value("${rest.path.pictures}")
    private String restPath;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private InsertInitialMovieDataService insertInitialMovieDataService;

    @BeforeEach
    void resetPersistentState(){
        insertInitialMovieDataService.removeDummyData();
        insertInitialMovieDataService.insertDummyData();
    }

    @Test
    void getMoviePictures_listPictures() throws Exception {
        String responseJson = mockMvc.perform(get(restPath)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Page<PictureDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<PictureDto>>() {});
        assertThat(response.getTotalElements()).isGreaterThanOrEqualTo(2);
        List<PictureDto> moviePictures = response.getContent();
        assertThat(moviePictures).anyMatch(moviePicture -> moviePicture.getPictureAlt().equals("Poster of the most wholesome movie"));
        assertThat(moviePictures).anyMatch(moviePicture -> moviePicture.getPictureAlt().equals("Poster of the most frozen movie"));
    }

//    @Test
//    void getMovieCategories_listNoMovies() throws Exception {
//        String responseJson = mockMvc.perform(get(restPath + "?pageNumber=0&pageSize=0")
//                        .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andReturn()
//                .getResponse()
//                .getContentAsString(StandardCharsets.UTF_8);
//        Page<MovieCategoryDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieCategoryDto>>() {});
//        assertThat(response.getTotalElements()).isEqualTo(0);
//    }

    @Test
    void getPictureById_existingId_resultSuccess() throws Exception{
        UUID moviePictureId = InsertInitialMovieDataService.FROZEN_MOVIE_PICTURE;

        String responseJson =
                mockMvc.perform(
                                get(restPath + "/" + moviePictureId)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        PictureDto movieCategory = ObjectConverter.convertJsonToObject(responseJson, PictureDto.class);
        assertThat(movieCategory.getPictureAlt()).isEqualTo("Poster of the most frozen movie");

    }

    @Test
    void getPictureById_notExistingId_resultFailure() throws Exception{
        UUID unknownCategoryId = UUID.randomUUID();


        mockMvc.perform(
                        get(restPath + "/" + unknownCategoryId)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());

    }

    @Test
    void createPicture_validInput_resultSuccess() throws Exception{

        PictureCreateDto pictureCreateDto = new PictureCreateDto();
        pictureCreateDto.setPictureAlt("Some random picture");
        pictureCreateDto.setPictureUrl("www.seznam.cz/picture.jpg");
        pictureCreateDto.setMovieId(InsertInitialMovieDataService.FROZEN_MOVIE);

        String responseJson =
                mockMvc.perform(
                                post(restPath)
                                        .with(TestDataFactory.mockAdminUser())
                                        .content(objectMapper.writeValueAsString(pictureCreateDto))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isCreated())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        PictureDto pictureDto = ObjectConverter.convertJsonToObject(responseJson, PictureDto.class);
        assertThat(pictureDto.getPictureAlt()).isEqualTo("Some random picture");
        assertThat(pictureDto.getId()).isNotNull();
    }

    @Test
    void createPicture_invalidInput_resultFailure() throws Exception{

        MovieCategoryDto movieCategory = new MovieCategoryDto();

        mockMvc.perform(
                        post(restPath)
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieCategory))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void updatePicture_validInput_resultSuccess() throws Exception{

        PictureDto pictureDto = new PictureDto();
        pictureDto.setId(InsertInitialMovieDataService.FROZEN_MOVIE_PICTURE);
        pictureDto.setPictureAlt("Some random picture");
        pictureDto.setPictureUrl("www.seznam.cz/picture.jpg");

        String responseJson =
                mockMvc.perform(
                                put(restPath + "/" + pictureDto.getId())
                                        .with(TestDataFactory.mockAdminUser())
                                        .content(objectMapper.writeValueAsString(pictureDto))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        PictureDto savedPictureDto = ObjectConverter.convertJsonToObject(responseJson, PictureDto.class);
        assertThat(savedPictureDto.getPictureAlt()).isEqualTo("Some random picture");
        assertThat(savedPictureDto.getId()).isNotNull();
    }

    @Test
    void updatePicture_invalidInput_resultFailure() throws Exception{

        MovieCategoryDto movieCategory = new MovieCategoryDto();
        movieCategory.setId(InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY);

        mockMvc.perform(
                        put(restPath + "/" + movieCategory.getId())
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieCategory))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void deletePicture_validInput_resultSuccess() throws Exception{

        UUID moviePictureId = InsertInitialMovieDataService.FROZEN_MOVIE_PICTURE;

        mockMvc.perform(
                        delete(restPath + "/" + moviePictureId)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void deletePicture_invalidInput_resultFailure() throws Exception{

        UUID randomUUID = UUID.randomUUID();

        mockMvc.perform(
                        delete(restPath + "/" + randomUUID)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

}
