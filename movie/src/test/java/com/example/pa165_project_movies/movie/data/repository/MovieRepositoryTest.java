package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.Movie;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class MovieRepositoryTest {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void newMovie_save_success() {
        Movie movie = new Movie();
        Movie insertedMovie = movieRepository.save(movie);
        assertThat(entityManager.find(Movie.class, insertedMovie.getId()) ).isEqualTo(movie);
    }

    @Test
    void movieCreated_findById_found() {
        Movie movie = new Movie();
        entityManager.persist(movie);
        Optional<Movie> retrievedMovie = movieRepository.findById(movie.getId());
        assertThat(retrievedMovie).contains(movie);
    }

    @Test
    void movieCreated_update_isUpdated() {
        Movie movie = new Movie();
        entityManager.persist(movie);
        String newTitle = "New Movie 001";
        movie.setTitle(newTitle);
        movieRepository.save(movie);
        assertThat(entityManager.find(Movie.class, movie.getId()).getTitle()).isEqualTo(newTitle);
    }

    @Test
    void movieCreated_delete_success() {
        Movie movie = new Movie();
        entityManager.persist(movie);
        movieRepository.delete(movie);
        assertThat(entityManager.find(Movie.class, movie.getId())).isNull();
    }
}
