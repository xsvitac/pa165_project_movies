package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.dto.filterDto.ObjectIdFilterDto;
import com.example.pa165_project_movies.movie.util.InsertInitialMovieDataService;
import com.example.pa165_project_movies.movie.util.ObjectConverter;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MovieRestControllerITest {
    @Autowired
    private MockMvc mockMvc;

    @Value("${rest.path.movies}")
    private String restPath;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private InsertInitialMovieDataService insertInitialMovieDataService;

    @BeforeEach
    void resetPersistentState(){
        insertInitialMovieDataService.removeDummyData();
        insertInitialMovieDataService.insertDummyData();
    }


    @Test
    void getMovies_withoutFilter_listAllMovies() throws Exception {
        String responseJson = mockMvc.perform(get(restPath)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(2);
        List<MovieDetailDto> movies = response.getContent();
        assertThat(movies).anyMatch(movie -> movie.getTitle().equals("Frozen"));
        assertThat(movies).anyMatch(movie -> movie.getTitle().equals("Scary movie"));

    }

    @Test
    void getMovies_withFilterOnTitle_listFrozen() throws Exception {
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setTitle("Fro");

        String responseJson =
            mockMvc.perform(
                    get(restPath)
                            .content(objectMapper.writeValueAsString(movieFilter))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(1);
        List<MovieDetailDto> movies = response.getContent();
        assertThat(movies).anyMatch(movie -> movie.getTitle().equals("Frozen"));
    }

    @Test
    void getMovies_withFilterOnTitle_listEmpty() throws Exception {
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setTitle("No name");

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(0);
    }

    @Test
    void getMovies_withFilterOnDescription_listFrozen() throws Exception {
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setDescription("scary");

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(1);
        List<MovieDetailDto> movies = response.getContent();
        assertThat(movies).anyMatch(movie -> movie.getTitle().equals("Frozen"));
    }
    @Test
    void getMovieById_existingId_success() throws Exception {
        UUID frozenMovie = InsertInitialMovieDataService.FROZEN_MOVIE;

        String responseJson =
            mockMvc.perform(
                        get(restPath + "/" + frozenMovie)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        MovieDetailDto movieDetailUpdated = ObjectConverter.convertJsonToObject(responseJson, MovieDetailDto.class);
        assertThat(movieDetailUpdated.getTitle()).isEqualTo("Frozen");
    }

    @Test
    void getMovieById_notExistingId_failure() throws Exception {
        UUID randomId = UUID.randomUUID();

        mockMvc.perform(
                        get(restPath + "/" + randomId)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void createMovie_ActionMovie_success() throws Exception {
        MovieNewDto movieNewDto = TestDataFactory.movieNewDto;

        String responseJson =
            mockMvc.perform(
                        post(restPath)
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieNewDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        MovieDetailDto movieDetailUpdated = ObjectConverter.convertJsonToObject(responseJson, MovieDetailDto.class);
        assertThat(movieDetailUpdated.getTitle()).isEqualTo(movieNewDto.getTitle());
        assertThat(movieDetailUpdated.getId()).isNotNull();
    }

    @Test
    void createMovie_emptyMovie_failure() throws Exception {
        MovieNewDto movieNewDto = new MovieNewDto();

        mockMvc.perform(
                        post(restPath)
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieNewDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void updateMovie_ActionMovie_success() throws Exception {
        UUID movieId = InsertInitialMovieDataService.SCARY_MOVIE;
        MovieDetailDto movieDetailDto = TestDataFactory.movieDetailDto;
        movieDetailDto.setId(movieId);

        String responseJson =
            mockMvc.perform(
                        put(restPath + "/" + movieId)
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieDetailDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        MovieDetailDto movieDetailUpdated = ObjectConverter.convertJsonToObject(responseJson, MovieDetailDto.class);
        assertThat(movieDetailUpdated.getTitle()).isEqualTo(movieDetailDto.getTitle());
    }

    @Test
    void updateMovie_ActionMovie_failure() throws Exception {
        MovieDetailDto movieDetailDto = TestDataFactory.movieDetailDto;
        movieDetailDto.setId(UUID.randomUUID());

        mockMvc.perform(
                        put(restPath + "/" + movieDetailDto.getId())
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieDetailDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }
    @Test
    void deleteMovie_existingId_success() throws Exception {
        UUID frozenMovie = InsertInitialMovieDataService.FROZEN_MOVIE;

        mockMvc.perform(
                        delete(restPath + "/" + frozenMovie)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteMovie_notExistingId_failure() throws Exception {
        UUID randomUUID = UUID.randomUUID();

        mockMvc.perform(
                        delete(restPath + "/" + randomUUID)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void getMovies_withFilterOnIncludedCategory_listEmpty() throws Exception {
        UUID horrorCategory = InsertInitialMovieDataService.HORROR_MOVIE_CATEGORY;
        UUID actionCategory = InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY;
        UUID romanceCategory = InsertInitialMovieDataService.ROMANCE_MOVIE_CATEGORY;
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setMovieCategoryIdFilter(List.of(
                new ObjectIdFilterDto(horrorCategory, true),
                new ObjectIdFilterDto(actionCategory, true),
                new ObjectIdFilterDto(romanceCategory, true)
                ));

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(0);
    }

    @Test
    void getMovies_withFilterOnIncludedCategory_listFrozen() throws Exception {
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setMovieCategoryIdFilter(List.of(
                new ObjectIdFilterDto(InsertInitialMovieDataService.HORROR_MOVIE_CATEGORY, true),
                new ObjectIdFilterDto(InsertInitialMovieDataService.DRAMA_MOVIE_CATEGORY, true)
        ));

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(1);
        assertThat(response.getContent().get(0).getTitle()).isEqualTo("Frozen");
    }

    @Test
    void getMovies_mixedFilterIncludeExclude_listEmpty() throws Exception {
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setMovieCategoryIdFilter(List.of(
                new ObjectIdFilterDto(InsertInitialMovieDataService.HORROR_MOVIE_CATEGORY, true),
                new ObjectIdFilterDto(InsertInitialMovieDataService.DRAMA_MOVIE_CATEGORY, false)
        ));

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(0);
    }

    @Test
    void getMovies_emptyFilterList_listAll() throws Exception {
        UUID horrorCategory = InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY;
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setMovieCategoryIdFilter(List.of(
        ));

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isEqualTo(2);
    }

    @Test
    void getMovies_withFilterOnExistingExcludedCategory_listAll() throws Exception {
        UUID randomCategory = UUID.randomUUID();
        MovieFilterDto movieFilter = new MovieFilterDto();
        movieFilter.setMovieCategoryIdFilter(List.of(
                new ObjectIdFilterDto(randomCategory, false)
        ));

        String responseJson =
                mockMvc.perform(
                                get(restPath)
                                        .content(objectMapper.writeValueAsString(movieFilter))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieDetailDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieDetailDto>>() {});
        assertThat(response.getTotalElements()).isGreaterThanOrEqualTo(2);
    }
}
