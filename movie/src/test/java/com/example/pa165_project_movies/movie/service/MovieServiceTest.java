package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.data.repository.MovieRepository;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {
    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieService movieService;

    @Test
    void findById_movieFound_returnsMovie() {
        // Arrange
        Mockito.when(movieRepository.findById(new UUID(0, 1))).thenReturn(Optional.ofNullable(TestDataFactory.movieEntity));

        // Act
        Movie foundEntity = movieService.findById(new UUID(0, 1));

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.movieEntity);
    }

    @Test
    void findById_movieNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieService.findById(new UUID(0, 1)))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_moviesExist_returnsPageOfMovies() {
        // Arrange
        Page<Movie> movies = Mockito.mock(Page.class);
        MovieFilterDto movieFilter = Mockito.mock(MovieFilterDto.class);
        Mockito.when(movieRepository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(movies);

        // Act
        Page<Movie> foundEntities = movieService.findAll(PageRequest.of(0, 20), movieFilter);

        // Assert
        assertThat(foundEntities).isEqualTo(movies).isNotNull();
    }

    @Test
    void findAll_moviesExist_returnsPageOfMoviesWithData() {
        // Arrange
        Page<Movie> movies = new PageImpl<>(List.of(TestDataFactory.movieEntity));
        MovieFilterDto movieFilter = Mockito.mock(MovieFilterDto.class);
        Mockito.when(movieRepository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(movies);

        // Act
        Page<Movie> foundEntities = movieService.findAll(PageRequest.of(0, 1), movieFilter);

        // Assert
        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(TestDataFactory.movieEntity)));
    }


    @Test
    void create_movieCreated_returnsMovie() {
        // Arrange
        Mockito.when(movieRepository.save(TestDataFactory.movieEntity)).thenReturn(TestDataFactory.movieEntity);

        // Act
        Movie createdEntity = movieService.create(TestDataFactory.movieEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieEntity);
    }

    @Test
    void create_movieNotCreated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieService.create(TestDataFactory.movieEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void update_movieUpdated_movieReturned() {
        // Arrange
        Mockito.when(movieRepository.findById(TestDataFactory.movieEntity.getId())).thenReturn(Optional.ofNullable(TestDataFactory.movieEntity));
        Mockito.when(movieRepository.save(TestDataFactory.movieEntity)).thenReturn(TestDataFactory.movieEntity);

        // Act
        Movie createdEntity = movieService.update(TestDataFactory.movieEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieEntity);
    }

    @Test
    void update_movieUpdated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieService.update(TestDataFactory.movieEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void delete_movieDeleted_doesNothing() {
        // Arrange
        Mockito.doNothing().when(movieRepository).delete(TestDataFactory.movieEntity);

        // Act
        movieService.delete(TestDataFactory.movieEntity);

        // Assert
        Mockito.verify(movieRepository, Mockito.times(1)).delete(TestDataFactory.movieEntity);
    }

    @Test
    void deleteAll_noReturn() {
        // Arrange

        Mockito.doNothing().when(movieRepository).deleteAll();

        // Act
        movieService.deleteAll();

        // Assert
        Mockito.verify(movieRepository, Mockito.times(1)).deleteAll();
    }


}
