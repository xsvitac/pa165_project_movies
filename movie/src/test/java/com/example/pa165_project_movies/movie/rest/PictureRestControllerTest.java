package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.facade.PictureFacade;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PictureRestControllerTest {

    @Mock
    private PictureFacade pictureFacade;

    @InjectMocks
    private PictureController pictureController;

    @Test
    void getMoviePictures_picturesExist_returnsPageOfPicturesWithData() {
        // Arrange
        Page<PictureDto> pictures = new PageImpl<>(List.of(TestDataFactory.pictureDto));
        Mockito.when(pictureFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pictures);

        // Act
        Page<PictureDto> foundDtos = pictureController.getMoviePictures(PageRequest.of(0, 20)).getBody();

        // Assert
        assertThat(foundDtos).isNotNull();
        assertThat(foundDtos).isEqualTo(pictures);
    }

    @Test
    void getPictureById_pictureFound_returnsPicture() {
        // Arrange
        PictureDto picture = TestDataFactory.pictureDto;
        Mockito.when(pictureFacade.findById(Mockito.any(UUID.class))).thenReturn(picture);

        // Act
        PictureDto foundDto = pictureController.getPictureById(UUID.randomUUID()).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(picture);
    }

    @Test
    void createPicture_validInput_returnsPicture() {
        // Arrange
        PictureDto picture = TestDataFactory.pictureDto;
        PictureCreateDto pictureNew = TestDataFactory.pictureCreateDto;
        Mockito.when(pictureFacade.create(Mockito.any(PictureCreateDto.class))).thenReturn(picture);

        // Act
        PictureDto foundDto = pictureController.createPicture(pictureNew).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(picture);
    }

    @Test
    void updatePicture_validInput_returnsPicture() {
        // Arrange
        PictureDto picture = TestDataFactory.pictureDto;
        Mockito.when(pictureFacade.update(picture.getId(), picture)).thenReturn(picture);

        // Act
        PictureDto foundDto = pictureController.updatePicture(picture.getId(), picture).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(picture);
    }

    @Test
    void deletePicture_pictureFound_returnsPicture() {
        // Arrange
        PictureCreateDto picture = TestDataFactory.pictureCreateDto;
        UUID id = UUID.fromString("52657669-6577-5265-706f-7369746f7279");
        Mockito.doNothing().when(pictureFacade).delete(id);

        // Act
        pictureController.deletePicture(id);

        // Assert
        Mockito.verify(pictureFacade, Mockito.times(1)).delete(id);
    }

}
