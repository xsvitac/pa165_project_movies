package com.example.pa165_project_movies.movie.facade;


import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.data.model.Picture;
import com.example.pa165_project_movies.movie.mapper.PictureMapper;
import com.example.pa165_project_movies.movie.service.PictureService;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class PictureFacadeTest {

    @Mock
    private PictureService pictureService;
    @Mock
    private PictureMapper pictureMapper;
    @InjectMocks
    private PictureFacade pictureFacade;

    @Test
    void findById_pictureFound_returnsPictureDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(pictureService.findById(uuid)).thenReturn(TestDataFactory.pictureEntity);
        Mockito.when(pictureMapper.mapToDto(any())).thenReturn(TestDataFactory.pictureDto);

        // Act
        PictureDto foundEntity = pictureFacade.findById(uuid);

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.pictureDto);
    }

    @Test
    void findAll_picturesExist_returnsPageOfPictures() {
        // Arrange
        Page<Picture> pictures = Mockito.mock(Page.class);
        Page<PictureDto> pictureDtos = Mockito.mock(Page.class);
        Mockito.when(pictureService.findAll(Mockito.any(Pageable.class))).thenReturn(pictures);
        Mockito.when(pictureMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(pictureDtos);

        // Act
        Page<PictureDto> foundDtos = pictureFacade.findAll(PageRequest.of(0, 20));

        // Assert
        assertThat(foundDtos).isEqualTo(pictureDtos).isNotNull();
    }

    @Test
    void findAll_picturesExist_returnsPageOfPicturesWithData() {
        // Arrange
        Page<Picture> pictures = Mockito.mock(Page.class);
        Page<PictureDto> pictureDtos = Mockito.mock(Page.class);
        Mockito.when(pictureService.findAll(Mockito.any(Pageable.class))).thenReturn(pictures);
        Mockito.when(pictureMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(pictureDtos);

        // Act
        Page<PictureDto> foundDtos = pictureFacade.findAll(PageRequest.of(0, 1));

        // Assert
        assertThat(foundDtos).isEqualTo(pictureDtos);
    }


    @Test
    void create_pictureCrated_returnsPictureNewDto() {
        // Arrange
        Mockito.when(pictureService.create(any())).thenReturn(TestDataFactory.pictureEntity);
        Mockito.when(pictureMapper.mapToDto(any())).thenReturn(TestDataFactory.pictureDto);

        // Act
        PictureDto createdEntity = pictureFacade.create(TestDataFactory.pictureCreateDto);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.pictureDto);
    }

    @Test
    void update_pictureUpdated_returnsPictureDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(pictureService.update(any())).thenReturn(TestDataFactory.pictureEntity);
        Mockito.when(pictureMapper.mapToDto(any())).thenReturn(TestDataFactory.pictureDto);

        // Act
        PictureDto updatedEntity = pictureFacade.update(uuid, TestDataFactory.pictureDto);

        // Assert
        assertThat(updatedEntity).isEqualTo(TestDataFactory.pictureDto);
    }

    @Test
    void delete_pictureDeleted_doesNothing() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.doNothing().when(pictureService).delete(any());

        // Act
        pictureFacade.delete(uuid);

        // Assert
        Mockito.verify(pictureService, Mockito.times(1)).findById(uuid);
        Mockito.verify(pictureService, Mockito.times(1)).delete(any());
    }
}

