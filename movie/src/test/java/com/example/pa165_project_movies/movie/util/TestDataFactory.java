package com.example.pa165_project_movies.movie.util;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.dto.PictureCreateDto;
import com.example.pa165_project_movies.movie.dto.PictureDto;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import com.example.pa165_project_movies.movie.data.model.Picture;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

public class TestDataFactory {

    public static Movie movieEntity = getMovieEntityFactory();
    public static MovieCategory movieCategoryEntity = getMovieCategoryFactory();

    public static MovieCategoryDto movieCategoryDto = getMovieCategoryDtoFactory();

    public static MovieCategoryCreateDto movieCategoryCreateDto = getMovieCategoryCreateDtoFactory();
    public static MovieDetailDto movieDetailDto = getMovieDetailDtoFactory();
    public static MovieNewDto movieNewDto = getMovieNewDtoFactory();

    public static Picture pictureEntity = getPictureFactory();

    public static PictureDto pictureDto = getPictureDtoFactory();

    public static PictureCreateDto pictureCreateDto = getPictureCreateDtoFactory();

    public static RequestPostProcessor mockAdminUser() {
        return SecurityMockMvcRequestPostProcessors.user("Admin").authorities(new SimpleGrantedAuthority(Oauth2Scope.ADMIN));
    }

    private static Movie getMovieEntityFactory() {
        Movie movie = new Movie();
        HashSet<MovieCategory> categories = new HashSet<MovieCategory>();
        categories.add(getMovieCategoryFactory());

        movie.setId(new UUID(0, 1));
        movie.setTitle("Amazing move");
        movie.setDescription("Description of the Amazing movie");
        movie.setCategories(categories);

        return movie;
    }

    private static MovieCategory getMovieCategoryFactory() {
        MovieCategory movieCategory = new MovieCategory();

        movieCategory.setName("Action");
        movieCategory.setDescription("A quick movie full of violence");

        return movieCategory;
    }

    private static MovieCategoryDto getMovieCategoryDtoFactory() {
        MovieCategoryDto movieCategoryDto = new MovieCategoryDto();

        movieCategoryDto.setName("Action");
        movieCategoryDto.setDescription("A quick movie full of violence");

        return movieCategoryDto;
    }

    private static MovieCategoryCreateDto getMovieCategoryCreateDtoFactory() {
        MovieCategoryCreateDto movieCategoryCreateDto = new MovieCategoryCreateDto();

        movieCategoryCreateDto.setName("Action");
        movieCategoryCreateDto.setDescription("A quick movie full of violence");

        return movieCategoryCreateDto;
    }

    private static MovieDetailDto getMovieDetailDtoFactory() {
        MovieDetailDto movieDetailDto = new MovieDetailDto();

        movieDetailDto.setId(new UUID(0, 1));
        movieDetailDto.setTitle("Amazing move");
        movieDetailDto.setDescription("Description of the Amazing movie");
        movieDetailDto.setDateOfCreation(OffsetDateTime.now());
        movieDetailDto.setDateOfLastChange(OffsetDateTime.now());
        movieDetailDto.setCategories(new HashSet<>());
        movieDetailDto.setPictures(new ArrayList<>());
        movieDetailDto.setPoster(null);

        return  movieDetailDto;
    }

    private static MovieNewDto getMovieNewDtoFactory() {
        MovieNewDto movieNewDto = new MovieNewDto();

        movieNewDto.setTitle("Amazing move");
        movieNewDto.setDescription("Description of the Amazing movie");

        return  movieNewDto;
    }

    private static Picture getPictureFactory() {
        Picture picture = new Picture();

        picture.setPictureUrl("picture-url.com");
        picture.setPictureAlt("alt-url.com");

        return picture;
    }

    private static PictureDto getPictureDtoFactory() {
        PictureDto pictureDto = new PictureDto();

        pictureDto.setPictureUrl("pictureDto-url.com");
        pictureDto.setPictureAlt("alt-url.com");

        return pictureDto;
    }

    private static PictureCreateDto getPictureCreateDtoFactory() {
        PictureCreateDto pictureDto = new PictureCreateDto();

        pictureDto.setPictureUrl("pictureDto-url.com");
        pictureDto.setPictureAlt("alt-url.com");

        return pictureDto;
    }

}
