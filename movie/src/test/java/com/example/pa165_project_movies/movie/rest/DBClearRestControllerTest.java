package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.facade.MovieCategoryFacade;
import com.example.pa165_project_movies.movie.facade.MovieFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DBClearRestControllerTest {
    @Mock
    private MovieFacade movieFacade;

    @Mock
    private MovieCategoryFacade movieCategoryFacade;

    @InjectMocks
    private DBClearRestController dbClearRestController;

    @Test
    void deleteMoviesAndCategories_moviesFound_returnsNothing() {
        // Arrange
        Mockito.doNothing().when(movieFacade).deleteAll();
        Mockito.doNothing().when(movieCategoryFacade).deleteAll();

        // Act
        dbClearRestController.deleteMoviesAndCategories();

        // Assert
        Mockito.verify(movieFacade, Mockito.times(1)).deleteAll();
        Mockito.verify(movieCategoryFacade, Mockito.times(1)).deleteAll();
    }
}
