package com.example.pa165_project_movies.movie.facade;


import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.data.model.Movie;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.mapper.MovieMapper;
import com.example.pa165_project_movies.movie.service.MovieService;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class MovieFacadeTest {

    @Mock
    private MovieService movieService;
    @Mock
    private MovieMapper movieMapper;
    @InjectMocks
    private MovieFacade movieFacade;

    @Test
    void findById_movieFound_returnsMovieDetailDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(movieService.findById(uuid)).thenReturn(TestDataFactory.movieEntity);
        Mockito.when(movieMapper.mapToDetailViewDto(any())).thenReturn(TestDataFactory.movieDetailDto);

        // Act
        MovieDetailDto foundEntity = movieFacade.findById(uuid);

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.movieDetailDto);
    }

    @Test
    void findAll_moviesExist_returnsPageOfMovies() {
        // Arrange
        Page<Movie> movies = Mockito.mock(Page.class);
        Page<MovieDetailDto> movieDtos = Mockito.mock(Page.class);
        MovieFilterDto movieFilter = Mockito.mock(MovieFilterDto.class);
        Mockito.when(movieService.findAll(Mockito.any(Pageable.class), Mockito.any(MovieFilterDto.class))).thenReturn(movies);
        Mockito.when(movieMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(movieDtos);

        // Act
        Page<MovieDetailDto> foundDtos = movieFacade.findAll(PageRequest.of(0, 20), movieFilter);

        // Assert
        assertThat(foundDtos).isEqualTo(movieDtos).isNotNull();
    }

    @Test
    void findAll_moviesExist_returnsPageOfMoviesWithData() {
        // Arrange
        Page<Movie> movies = Mockito.mock(Page.class);
        Page<MovieDetailDto> movieDtos = Mockito.mock(Page.class);
        MovieFilterDto movieFilter = Mockito.mock(MovieFilterDto.class);
        Mockito.when(movieService.findAll(Mockito.any(Pageable.class), Mockito.any(MovieFilterDto.class))).thenReturn(movies);
        Mockito.when(movieMapper.mapToPageDto(Mockito.any(Page.class))).thenReturn(movieDtos);

        // Act
        Page<MovieDetailDto> foundDtos = movieFacade.findAll(PageRequest.of(0, 1), movieFilter);

        // Assert
        assertThat(foundDtos).isEqualTo(movieDtos);
    }


    @Test
    void create_movieCrated_returnsMovieNewDto() {
        // Arrange
        Mockito.when(movieService.create(any())).thenReturn(TestDataFactory.movieEntity);
        Mockito.when(movieMapper.mapToDetailViewDto(any())).thenReturn(TestDataFactory.movieDetailDto);

        // Act
        MovieDetailDto createdEntity = movieFacade.create(TestDataFactory.movieNewDto);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.movieDetailDto);
    }

    @Test
    void update_movieUpdated_returnsMovieDetailDto() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.when(movieService.update(any())).thenReturn(TestDataFactory.movieEntity);
        Mockito.when(movieMapper.mapToDetailViewDto(any())).thenReturn(TestDataFactory.movieDetailDto);

        // Act
        MovieDetailDto updatedEntity = movieFacade.update(uuid, TestDataFactory.movieDetailDto);

        // Assert
        assertThat(updatedEntity).isEqualTo(TestDataFactory.movieDetailDto);
    }

    @Test
    void delete_movieDeleted_doesNothing() {
        // Arrange
        UUID uuid = new UUID(0, 1);
        Mockito.doNothing().when(movieService).delete(any());

        // Act
        movieFacade.delete(uuid);

        // Assert
        Mockito.verify(movieService, Mockito.times(1)).findById(uuid);
        Mockito.verify(movieService, Mockito.times(1)).delete(any());
    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(movieService).deleteAll();

        // Act
        movieFacade.deleteAll();

        // Assert
        Mockito.verify(movieService, Mockito.times(1)).deleteAll();
    }
}

