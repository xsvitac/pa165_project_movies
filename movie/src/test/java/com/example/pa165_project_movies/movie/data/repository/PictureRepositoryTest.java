package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.Picture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class PictureRepositoryTest {

    @Autowired
    PictureRepository pictureRepository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void newPicture_save_success() {
        Picture picture = new Picture();
        Picture insertedPicture = pictureRepository.save(picture);
        assertThat(entityManager.find(Picture.class, insertedPicture.getId()) ).isEqualTo(picture);
    }

    @Test
    void pictureCreated_findById_found() {
        Picture picture = new Picture();
        entityManager.persist(picture);
        Optional<Picture> retrievedPicture = pictureRepository.findById(picture.getId());
        assertThat(retrievedPicture).contains(picture);
    }

    @Test
    void pictureCreated_update_isUpdated() {
        Picture picture = new Picture();
        entityManager.persist(picture);
        String newPictureUrl = "New Picture URL 001";
        picture.setPictureUrl(newPictureUrl);
        pictureRepository.save(picture);
        assertThat(entityManager.find(Picture.class, picture.getId()).getPictureUrl()).isEqualTo(newPictureUrl);
    }

    @Test
    void pictureCreated_delete_success() {
        Picture picture = new Picture();
        entityManager.persist(picture);
        pictureRepository.delete(picture);
        assertThat(entityManager.find(Picture.class, picture.getId())).isNull();
    }
}
