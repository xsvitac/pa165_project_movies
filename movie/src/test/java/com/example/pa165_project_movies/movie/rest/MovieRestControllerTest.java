package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.MovieDetailDto;
import com.example.pa165_project_movies.movie.dto.MovieNewDto;
import com.example.pa165_project_movies.movie.dto.filterDto.MovieFilterDto;
import com.example.pa165_project_movies.movie.facade.MovieFacade;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class MovieRestControllerTest {

    @Mock
    private MovieFacade movieFacade;

    @InjectMocks
    private MovieController movieController;

    @Test
    void getAllMovies_moviesExist_returnsPageOfMoviesWithData() {
        // Arrange
        Page<MovieDetailDto> movies = new PageImpl<>(List.of(TestDataFactory.movieDetailDto));
        MovieFilterDto movieFilter = Mockito.mock(MovieFilterDto.class);
        Mockito.when(movieFacade.findAll(Mockito.any(Pageable.class), Mockito.any(MovieFilterDto.class))).thenReturn(movies);

        // Act
        Page<MovieDetailDto> foundDtos = movieController.getMovies(PageRequest.of(0, 20), Optional.of(movieFilter)).getBody();

        // Assert
        assertThat(foundDtos).isNotNull();
        assertThat(foundDtos).isEqualTo(movies);
    }

    @Test
    void getMovieById_movieFound_returnsMovie() {
        // Arrange
        MovieDetailDto movie = TestDataFactory.movieDetailDto;
        Mockito.when(movieFacade.findById(Mockito.any(UUID.class))).thenReturn(movie);

        // Act
        MovieDetailDto foundDto = movieController.getMovieById(UUID.randomUUID()).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movie);
    }

    @Test
    void createMovie_validInput_returnsMovie() {
        // Arrange
        MovieDetailDto movie = TestDataFactory.movieDetailDto;
        MovieNewDto movieNew = TestDataFactory.movieNewDto;
        Mockito.when(movieFacade.create(Mockito.any(MovieNewDto.class))).thenReturn(movie);

        // Act
        MovieDetailDto foundDto = movieController.createMovie(movieNew).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movie);
    }

    @Test
    void updateMovie_validInput_returnsMovie() {
        // Arrange
        MovieDetailDto movie = TestDataFactory.movieDetailDto;
        Mockito.when(movieFacade.update(movie.getId(), movie)).thenReturn(movie);

        // Act
        MovieDetailDto foundDto = movieController.updateMovie(movie.getId(), movie).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movie);
    }

    @Test
    void deleteMovie_movieFound_returnsMovie() {
        // Arrange
        MovieNewDto movie = TestDataFactory.movieNewDto;
        UUID id = UUID.fromString("52657669-6577-5265-706f-7369746f7279");
        Mockito.doNothing().when(movieFacade).delete(id);

        // Act
        movieController.deleteMovie(id);

        // Assert
        Mockito.verify(movieFacade, Mockito.times(1)).delete(id);
    }

}
