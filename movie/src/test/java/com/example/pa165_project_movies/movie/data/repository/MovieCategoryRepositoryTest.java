package com.example.pa165_project_movies.movie.data.repository;

import com.example.pa165_project_movies.movie.data.model.MovieCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class MovieCategoryRepositoryTest {

    @Autowired
    MovieCategoryRepository movieCategoryRepository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void newMovieCategory_save_success() {
        MovieCategory movieCategory = new MovieCategory();
        MovieCategory insertedMovieCategory = movieCategoryRepository.save(movieCategory);
        assertThat(entityManager.find(MovieCategory.class, insertedMovieCategory.getId()) ).isEqualTo(movieCategory);
    }

    @Test
    void movieCategoryCreated_findById_found() {
        MovieCategory movieCategory = new MovieCategory();
        entityManager.persist(movieCategory);
        Optional<MovieCategory> retrievedMovieCategory = movieCategoryRepository.findById(movieCategory.getId());
        assertThat(retrievedMovieCategory).contains(movieCategory);
    }

    @Test
    void movieCategoryCreated_update_isUpdated() {
        MovieCategory movieCategory = new MovieCategory();
        entityManager.persist(movieCategory);
        String newName = "New MovieCategory 001";
        movieCategory.setName(newName);
        movieCategoryRepository.save(movieCategory);
        assertThat(entityManager.find(MovieCategory.class, movieCategory.getId()).getName()).isEqualTo(newName);
    }

    @Test
    void movieCategoryCreated_delete_success() {
        MovieCategory movieCategory = new MovieCategory();
        entityManager.persist(movieCategory);
        movieCategoryRepository.delete(movieCategory);
        assertThat(entityManager.find(MovieCategory.class, movieCategory.getId())).isNull();
    }
}
