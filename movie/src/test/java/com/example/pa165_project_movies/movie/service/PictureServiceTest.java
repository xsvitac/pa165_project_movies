package com.example.pa165_project_movies.movie.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.movie.data.model.Picture;
import com.example.pa165_project_movies.movie.data.repository.PictureRepository;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class PictureServiceTest {
    @Mock
    private PictureRepository pictureRepository;

    @InjectMocks
    private PictureService pictureService;

    @Test
    void findById_pictureFound_returnsPicture() {
        // Arrange
        Mockito.when(pictureRepository.findById(new UUID(0, 1))).thenReturn(Optional.ofNullable(TestDataFactory.pictureEntity));

        // Act
        Picture foundEntity = pictureService.findById(new UUID(0, 1));

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.pictureEntity);
    }

    @Test
    void findById_pictureNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(pictureService.findById(new UUID(0, 1)))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_picturesExist_returnsPageOfPictures() {
        // Arrange
        Page<Picture> pictures = Mockito.mock(Page.class);
        Mockito.when(pictureRepository.findAll(Mockito.any(Pageable.class))).thenReturn(pictures);

        // Act
        Page<Picture> foundEntities = pictureService.findAll(PageRequest.of(0, 20));

        // Assert
        assertThat(foundEntities).isEqualTo(pictures).isNotNull();
    }

    @Test
    void findAll_picturesExist_returnsPageOfPicturesWithData() {
        // Arrange
        Page<Picture> pictures = new PageImpl<>(List.of(TestDataFactory.pictureEntity));
        Mockito.when(pictureRepository.findAll(Mockito.any(Pageable.class))).thenReturn(pictures);

        // Act
        Page<Picture> foundEntities = pictureService.findAll(PageRequest.of(0, 1));

        // Assert
        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(TestDataFactory.pictureEntity)));
    }


    @Test
    void create_pictureCreated_returnsPicture() {
        // Arrange
        Mockito.when(pictureRepository.save(TestDataFactory.pictureEntity)).thenReturn(TestDataFactory.pictureEntity);

        // Act
        Picture createdEntity = pictureService.create(TestDataFactory.pictureEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.pictureEntity);
    }

    @Test
    void create_pictureNotCreated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(pictureService.create(TestDataFactory.pictureEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void update_pictureUpdated_pictureReturned() {
        // Arrange
        Mockito.when(pictureRepository.findById(TestDataFactory.pictureEntity.getId())).thenReturn(Optional.ofNullable(TestDataFactory.pictureEntity));
        Mockito.when(pictureRepository.save(TestDataFactory.pictureEntity)).thenReturn(TestDataFactory.pictureEntity);

        // Act
        Picture createdEntity = pictureService.update(TestDataFactory.pictureEntity);

        // Assert
        assertThat(createdEntity).isEqualTo(TestDataFactory.pictureEntity);
    }

    @Test
    void update_pictureUpdated_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(pictureService.update(TestDataFactory.pictureEntity))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void delete_pictureDeleted_doesNothing() {
        // Arrange
        Mockito.doNothing().when(pictureRepository).delete(TestDataFactory.pictureEntity);

        // Act
        pictureService.delete(TestDataFactory.pictureEntity);

        // Assert
        Mockito.verify(pictureRepository, Mockito.times(1)).delete(TestDataFactory.pictureEntity);
    }


}
