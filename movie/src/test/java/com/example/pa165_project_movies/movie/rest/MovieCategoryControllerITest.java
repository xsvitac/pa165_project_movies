package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.util.InsertInitialMovieDataService;
import com.example.pa165_project_movies.movie.util.ObjectConverter;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MovieCategoryControllerITest {
    @Autowired
    private MockMvc mockMvc;

    @Value("${rest.path.categories}")
    private String restPath;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private InsertInitialMovieDataService insertInitialMovieDataService;

    @BeforeEach
    void resetPersistentState(){
        insertInitialMovieDataService.removeDummyData();
        insertInitialMovieDataService.insertDummyData();
    }

    @Test
    void getMovieCategories_listCategories() throws Exception {
        String responseJson = mockMvc.perform(get(restPath)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieCategoryDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieCategoryDto>>() {});
        assertThat(response.getTotalElements()).isGreaterThanOrEqualTo(5);
        List<MovieCategoryDto> movieCategories = response.getContent();
        assertThat(movieCategories).anyMatch(movieCategory -> movieCategory.getName().equals("Action"));
        assertThat(movieCategories).anyMatch(movieCategory -> movieCategory.getName().equals("Sci-Fi"));
        assertThat(movieCategories).anyMatch(movieCategory -> movieCategory.getName().equals("Romance"));
        assertThat(movieCategories).anyMatch(movieCategory -> movieCategory.getName().equals("Drama"));
        assertThat(movieCategories).anyMatch(movieCategory -> movieCategory.getName().equals("Horror"));
    }

    @Test
    void getMovieCategories_pageSizeOne_listOneMovies() throws Exception {
        String responseJson = mockMvc.perform(get(restPath + "?page=0&size=1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Page<MovieCategoryDto> response = ObjectConverter.convertJsonToObject(responseJson, new TypeReference<RestPage<MovieCategoryDto>>() {});
        assertThat(response.getSize()).isEqualTo(1);
    }

    @Test
    void getMovieCategoryById_existingId_resultSuccess() throws Exception{
        UUID actionCategoryId = InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY;

        String responseJson =
                mockMvc.perform(
                                get(restPath + "/" + actionCategoryId)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        MovieCategoryDto movieCategory = ObjectConverter.convertJsonToObject(responseJson, MovieCategoryDto.class);
        assertThat(movieCategory.getName()).isEqualTo("Action");

    }

    @Test
    void getMovieCategoryById_notExistingId_resultFailure() throws Exception{
        UUID unknownCategoryId = UUID.randomUUID();


        mockMvc.perform(
                        get(restPath + "/" + unknownCategoryId)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());

    }

    @Test
    void createMovieCategory_validInput_resultSuccess() throws Exception{

        MovieCategoryCreateDto movieCategory = new MovieCategoryCreateDto();
        movieCategory.setName("Strange");
        movieCategory.setDescription("MOvie full of strange");

        String responseJson =
                mockMvc.perform(
                                post(restPath)
                                        .with(TestDataFactory.mockAdminUser())
                                        .content(objectMapper.writeValueAsString(movieCategory))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isCreated())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        MovieCategoryDto savedMovieCategory = ObjectConverter.convertJsonToObject(responseJson, MovieCategoryDto.class);
        assertThat(movieCategory.getName()).isEqualTo("Strange");
        assertThat(savedMovieCategory.getId()).isNotNull();
    }

    @Test
    void createMovieCategory_invalidInput_resultFailure() throws Exception{

        MovieCategoryDto movieCategory = new MovieCategoryDto();

        mockMvc.perform(
                        post(restPath)
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieCategory))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void updateMovieCategory_validInput_resultSuccess() throws Exception{

        MovieCategoryDto movieCategory = new MovieCategoryDto();
        movieCategory.setId(InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY);
        movieCategory.setName("Strange");
        movieCategory.setDescription("MOvie full of strange");

        String responseJson =
                mockMvc.perform(
                                put(restPath + "/" + movieCategory.getId())
                                        .with(TestDataFactory.mockAdminUser())
                                        .content(objectMapper.writeValueAsString(movieCategory))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8);
        MovieCategoryDto savedMovieCategory = ObjectConverter.convertJsonToObject(responseJson, MovieCategoryDto.class);
        assertThat(movieCategory.getName()).isEqualTo("Strange");
        assertThat(savedMovieCategory.getId()).isNotNull();
    }

    @Test
    void updateMovieCategory_invalidInput_resultFailure() throws Exception{

        MovieCategoryDto movieCategory = new MovieCategoryDto();
        movieCategory.setId(InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY);

        mockMvc.perform(
                        put(restPath + "/" + movieCategory.getId())
                                .with(TestDataFactory.mockAdminUser())
                                .content(objectMapper.writeValueAsString(movieCategory))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void deleteMovieCategory_validInput_resultSuccess() throws Exception{

        UUID movieCategoryId = InsertInitialMovieDataService.ACTION_MOVIE_CATEGORY;

        mockMvc.perform(
                        delete(restPath + "/" + movieCategoryId)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteMovieCategory_validInputCategoryInNoMovie_resultSuccess() throws Exception{

        UUID movieCategoryId = InsertInitialMovieDataService.COMEDY_MOVIE_CATEGORY;

        mockMvc.perform(
                        delete(restPath + "/" + movieCategoryId)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteMovieCategory_invalidInput_resultFailure() throws Exception{

        UUID randomUUID = UUID.randomUUID();

        mockMvc.perform(
                        delete(restPath + "/" + randomUUID)
                                .with(TestDataFactory.mockAdminUser())
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }
}
