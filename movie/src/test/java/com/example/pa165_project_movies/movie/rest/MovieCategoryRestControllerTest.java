package com.example.pa165_project_movies.movie.rest;

import com.example.pa165_project_movies.movie.dto.MovieCategoryCreateDto;
import com.example.pa165_project_movies.movie.dto.MovieCategoryDto;
import com.example.pa165_project_movies.movie.facade.MovieCategoryFacade;
import com.example.pa165_project_movies.movie.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class MovieCategoryRestControllerTest {

    @Mock
    private MovieCategoryFacade movieCategoryFacade;

    @InjectMocks
    private MovieCategoryController movieCategoryController;

    @Test
    void getAllMovieCategories_movieCategoriesExist_returnsPageOfMovieCategoriesWithData() {
        // Arrange
        Page<MovieCategoryDto> movieCategories = new PageImpl<>(List.of(TestDataFactory.movieCategoryDto));
        Mockito.when(movieCategoryFacade.findAll(Mockito.any(Pageable.class))).thenReturn(movieCategories);

        // Act
        Page<MovieCategoryDto> foundDtos = movieCategoryController.getMovieCategories(PageRequest.of(0, 20)).getBody();

        // Assert
        assertThat(foundDtos).isNotNull();
        assertThat(foundDtos).isEqualTo(movieCategories);
    }

    @Test
    void getMovieCategoryById_movieCategoryFound_returnsMovieCategory() {
        // Arrange
        MovieCategoryDto movieCategory = TestDataFactory.movieCategoryDto;
        Mockito.when(movieCategoryFacade.findById(Mockito.any(UUID.class))).thenReturn(movieCategory);

        // Act
        MovieCategoryDto foundDto = movieCategoryController.getMovieCategoryById(UUID.randomUUID()).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movieCategory);
    }

    @Test
    void createMovieCategory_validInput_returnsMovieCategory() {
        // Arrange
        MovieCategoryDto movieCategory = TestDataFactory.movieCategoryDto;
        MovieCategoryCreateDto movieCategoryNew = TestDataFactory.movieCategoryCreateDto;
        Mockito.when(movieCategoryFacade.create(Mockito.any(MovieCategoryCreateDto.class))).thenReturn(movieCategory);

        // Act
        MovieCategoryDto foundDto = movieCategoryController.createMovieCategory(movieCategoryNew).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movieCategory);
    }

    @Test
    void updateMovieCategory_validInput_returnsMovieCategory() {
        // Arrange
        MovieCategoryDto movieCategory = TestDataFactory.movieCategoryDto;
        Mockito.when(movieCategoryFacade.update(movieCategory.getId(), movieCategory)).thenReturn(movieCategory);

        // Act
        MovieCategoryDto foundDto = movieCategoryController.updateMovieCategory(movieCategory.getId(), movieCategory).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(movieCategory);
    }

    @Test
    void deleteMovieCategory_movieCategoryFound_returnsMovieCategory() {
        // Arrange
        UUID id = UUID.fromString("52657669-6577-5265-706f-7369746f7279");
        Mockito.doNothing().when(movieCategoryFacade).delete(id);

        // Act
        movieCategoryController.deleteMovieCategory(id);

        // Assert
        Mockito.verify(movieCategoryFacade, Mockito.times(1)).delete(id);
    }

}
