package com.example.pa165_project_movies.oauth2client;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Oauth2ClientRestController {

    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal OidcUser user) {
        // put obtained user data into a model attribute
        model.addAttribute("user", user);

        // return the name of a Thymeleaf HTML template that
        // will be searched in src/main/resources/templates with .html suffix
        return "index";
    }

    @GetMapping("/oauth2token")
    public String getOauth2Token(Model model, @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oAuth2AuthorizedClient) {
        model.addAttribute("oauth2token", oAuth2AuthorizedClient.getAccessToken());
        return "oauth2token";
    }
}
