package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.personnel.dto.MovieRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.facade.MovieRoleFacade;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class MovieRoleRestControllerTest {

    @Mock
    private MovieRoleFacade movieRoleFacade;
    @InjectMocks
    private MovieRoleRestController movieRoleRestController;

    @Test
    void create_validInput_returns_movieRole() {
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        MovieRoleCreateDto movieRoleCreateDto = new MovieRoleCreateDto();
        movieRoleCreateDto.setDescription(movieRoleDto.getDescription());
        movieRoleCreateDto.setName(movieRoleDto.getName());
        Mockito.when(movieRoleFacade.create(movieRoleCreateDto)).thenReturn(movieRoleDto);

        MovieRoleDto savedMovieRoleDto = movieRoleRestController.createMovieRole(movieRoleCreateDto).getBody();

        assertThat(savedMovieRoleDto).isEqualTo(movieRoleDto);
    }

    @Test
    void update_validInput_returns_movieRole() {
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        Mockito.when(movieRoleFacade.update(movieRoleDto.getId(), movieRoleDto)).thenReturn(movieRoleDto);

        MovieRoleDto updatedDto = movieRoleRestController.updateMovieRole(movieRoleDto.getId(), movieRoleDto).getBody();

        assertThat(updatedDto).isEqualTo(movieRoleDto);
        Mockito.verify(movieRoleFacade, Mockito.times(1)).update(movieRoleDto.getId(), movieRoleDto);

    }

    @Test
    void delete_validInput_noReturn() {
        Person person = PersonnelTestDataFactory.personEntity;
        UUID id = person.getId();
        Mockito.doNothing().when(movieRoleFacade).delete(id);

        movieRoleRestController.deleteMovieRole(id);

        Mockito.verify(movieRoleFacade, Mockito.times(1)).delete(id);
    }

    @Test
    void findById_movieRoleFound_returnsMovieRole() {
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        UUID personId = movieRoleDto.getId();
        // Arrange
        Mockito.when(movieRoleFacade.findById(personId)).thenReturn(movieRoleDto);

        // Act
        MovieRoleDto foundDto = movieRoleRestController.getMovieRoleById(personId).getBody();

        // Assert
        assertThat(foundDto).isEqualTo(movieRoleDto);
    }

    @Test
    void findById_movieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.ACTOR_MOVIE_ROLE_ID);
        Mockito.when(movieRoleFacade.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> movieRoleRestController.getMovieRoleById(notPersonId));
    }

    @Test
    void findAll_movieRolesExist_returnsPageOfMovieRole() {
        Page<MovieRoleDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(movieRoleFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<MovieRoleDto> foundDtos = movieRoleRestController.getMovieRoles(PageRequest.of(0, 20)).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_movieRoleExist_returnsPageOfMovieRoleData() {
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        Page<MovieRoleDto> pageDtos = new PageImpl<>(List.of(movieRoleDto));
        Mockito.when(movieRoleFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<MovieRoleDto> foundDtos = movieRoleRestController.getMovieRoles(PageRequest.of(0, 1)).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos);

    }
}
