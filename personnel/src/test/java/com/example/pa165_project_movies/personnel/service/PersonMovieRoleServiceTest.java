package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import com.example.pa165_project_movies.personnel.data.repository.PersonMovieRoleRepository;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
class PersonMovieRoleServiceTest {

    @Mock
    private PersonMovieRoleRepository personMovieRoleRepository;

    @InjectMocks
    private PersonMovieRoleService personMovieRoleService;

    @Test
    void create_validInput_returns_personMovieRole() {
        PersonMovieRole personMovieRole = PersonnelTestDataFactory.personMovieRoleEntity;
        Mockito.when(personMovieRoleRepository.save(personMovieRole)).thenReturn(personMovieRole);

        PersonMovieRole savedPersonMovieRole = personMovieRoleService.create(personMovieRole);

        assertThat(savedPersonMovieRole).isEqualTo(personMovieRole);
    }

    @Test
    void update_validInput_returns_personMovieRole() {
        PersonMovieRole personMovieRole = PersonnelTestDataFactory.personMovieRoleEntity;
        Mockito.when(personMovieRoleRepository.findById(personMovieRole.getId())).thenReturn(Optional.of(personMovieRole));
        Mockito.when(personMovieRoleRepository.save(personMovieRole)).thenReturn(personMovieRole);

        PersonMovieRole updatedEntity = personMovieRoleService.update(personMovieRole);

        assertThat(updatedEntity).isEqualTo(personMovieRole);
        Mockito.verify(personMovieRoleRepository, Mockito.times(1)).findById(personMovieRole.getId());
        Mockito.verify(personMovieRoleRepository, Mockito.times(1)).save(personMovieRole);

    }

    @Test
    void delete_validInput_noReturn() {
        PersonMovieRole personMovieRole = PersonnelTestDataFactory.personMovieRoleEntity;
        Mockito.doNothing().when(personMovieRoleRepository).delete(personMovieRole);

        personMovieRoleService.delete(personMovieRole);

        Mockito.verify(personMovieRoleRepository, Mockito.times(1)).delete(personMovieRole);
    }

    @Test
    void findById_personMovieRoleFound_returnsPersonMovieRole() {
        PersonMovieRole personMovieRole = PersonnelTestDataFactory.personMovieRoleEntity;
        UUID personId = personMovieRole.getId();
        // Arrange
        Mockito.when(personMovieRoleRepository.findById(personId))
                .thenReturn(Optional.ofNullable(personMovieRole));

        // Act
        PersonMovieRole foundEntity = personMovieRoleService.findById(personId);

        // Assert
        assertThat(foundEntity).isEqualTo(personMovieRole);
    }

    @Test
    void findById_personMovieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonMovieRoleId = UUID.fromString(PersonnelTestDataFactory.MOVIE_ID);
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(personMovieRoleService.findById(notPersonMovieRoleId))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRole() {
        Page<PersonMovieRole> page = Mockito.mock(Page.class);
        Mockito.when(personMovieRoleRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<PersonMovieRole> foundEntities = personMovieRoleService.findAll(PageRequest.of(0, 20));

        assertThat(foundEntities).isEqualTo(page).isNotNull();

    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRoleData() {
        PersonMovieRole personMovieRole = PersonnelTestDataFactory.personMovieRoleEntity;
        Page<PersonMovieRole> page = new PageImpl<>(List.of(personMovieRole));
        Mockito.when(personMovieRoleRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<PersonMovieRole> foundEntities = personMovieRoleService.findAll(PageRequest.of(0, 1));

        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(personMovieRole)));

    }

    @Test
    void deleteAll_noReturn() {
        // Arrange

        Mockito.doNothing().when(personMovieRoleRepository).deleteAll();

        // Act
        personMovieRoleService.deleteAll();

        // Assert
        Mockito.verify(personMovieRoleRepository, Mockito.times(1)).deleteAll();
    }
}
