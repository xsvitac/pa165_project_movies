package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.personnel.facade.MovieRoleFacade;
import com.example.pa165_project_movies.personnel.facade.PersonFacade;
import com.example.pa165_project_movies.personnel.facade.PersonMovieRoleFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DBClearRestControllerTest {

    @Mock
    private PersonFacade personFacade;
    @Mock
    private MovieRoleFacade movieRoleFacade;
    @Mock
    private PersonMovieRoleFacade personMovieRoleFacade;

    @InjectMocks
    private DBClearRestController dbClearRestController;
    @Test
    void deletePersonnelAndRoles_personnelFound_returnsNothing() {
        // Arrange
        Mockito.doNothing().when(personFacade).deleteAll();
        Mockito.doNothing().when(movieRoleFacade).deleteAll();
        Mockito.doNothing().when(personMovieRoleFacade).deleteAll();

        // Act
        dbClearRestController.deletePersonnelAndRoles();

        // Assert
        Mockito.verify(personFacade, Mockito.times(1)).deleteAll();
        Mockito.verify(movieRoleFacade, Mockito.times(1)).deleteAll();
        Mockito.verify(personMovieRoleFacade, Mockito.times(1)).deleteAll();
    }
}
