package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.personnel.dto.ApiErrorDto;
import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.mappers.PersonMapper;
import com.example.pa165_project_movies.personnel.service.PersonService;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonMapper personMapper;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PersonService personService;

    @Value("${rest.path.persons}")
    private URI personControllerPath;

    @Transactional
    @Test
    void getAllPersons_personsExist_returnsPageOfPersons() throws Exception {
        // Arrange
        PersonnelTestDataFactory.allPersonEntities.forEach(person -> personService.create(person));
        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(personControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(PersonnelTestDataFactory.allPersonEntities.size()))
                .andExpect(jsonPath("$.numberOfElements").value(PersonnelTestDataFactory.allPersonEntities.size()))
                .andExpect(jsonPath("$.content.length()").value(PersonnelTestDataFactory.allPersonEntities.size()));
    }

    @Transactional(readOnly = true)
    @Test
    void getAllPersons_noPersonsExist_returnsEmptyPage() throws Exception {
        // Arrange
        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(personControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.content.length()").value(0));
    }

    @Transactional
    @Test
    void getPersonById_personExists_returnsPerson() throws Exception {
        // Arrange
        Person person = personService.create(PersonnelTestDataFactory.personEntity);
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        personDto.setId(person.getId());

        URI requestUri = UriComponentsBuilder.fromUri(personControllerPath)
                .path("/{id}")
                .buildAndExpand(person.getId().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        PersonDto response = objectMapper.readValue(responseJson, PersonDto.class);

        // Assert
        assertThat(response.getId()).isEqualTo(personDto.getId());
        assertThat(response.getGivenName()).isEqualTo(personDto.getGivenName());
        assertThat(response.getFamilyName()).isEqualTo(personDto.getFamilyName());
        assertThat(response.getDescription()).isEqualTo(personDto.getDescription());
    }

    @Transactional(readOnly = true)
    @Test
    void getPersonById_personDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        URI requestUri = UriComponentsBuilder.fromUri(personControllerPath)
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getPath()).isEqualTo(requestUri.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createPerson_givenNameIsNull_returnsApiError() throws Exception {
        // Arrange

        PersonCreateDto personCreateDto = new PersonCreateDto();
        personCreateDto.setGivenName(null);
        personCreateDto.setFamilyName(PersonnelTestDataFactory.personDto.getFamilyName());
        personCreateDto.setDescription(PersonnelTestDataFactory.personDto.getDescription());

        String personCreateDtoJson = objectMapper.writeValueAsString(personCreateDto);

        // Act
        String responseJson = mockMvc.perform(post(personControllerPath)
                        .with(PersonnelTestDataFactory.mockAdminUser())
                        .content(personCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getPath()).isEqualTo(personControllerPath.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createPerson_familyNameIsNull_returnsApiError() throws Exception {
        // Arrange

        PersonCreateDto personCreateDto = new PersonCreateDto();
        personCreateDto.setGivenName(PersonnelTestDataFactory.personDto.getGivenName());
        personCreateDto.setFamilyName(null);
        personCreateDto.setDescription(PersonnelTestDataFactory.personDto.getDescription());

        String personCreateDtoJson = objectMapper.writeValueAsString(personCreateDto);

        // Act
        String responseJson = mockMvc.perform(post(personControllerPath)
                        .with(PersonnelTestDataFactory.mockAdminUser())
                        .content(personCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getPath()).isEqualTo(personControllerPath.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createPerson_descriptionIsNull_returnsApiError() throws Exception {
        // Arrange

        PersonCreateDto personCreateDto = new PersonCreateDto();
        personCreateDto.setGivenName(PersonnelTestDataFactory.personDto.getGivenName());
        personCreateDto.setFamilyName(PersonnelTestDataFactory.personDto.getFamilyName());
        personCreateDto.setDescription(null);

        String personCreateDtoJson = objectMapper.writeValueAsString(personCreateDto);

        // Act
        String responseJson = mockMvc.perform(post(personControllerPath)
                        .with(PersonnelTestDataFactory.mockAdminUser())
                        .content(personCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getPath()).isEqualTo(personControllerPath.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createPerson_validInput_returnsPersonDto() throws Exception {
        // Arrange
        String personCreateDtoJson = objectMapper.writeValueAsString(PersonnelTestDataFactory.personDto);

        // Act
        String responseJson = mockMvc.perform(post(personControllerPath)
                        .with(PersonnelTestDataFactory.mockAdminUser())
                        .content(personCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        PersonDto response = objectMapper.readValue(responseJson, PersonDto.class);

        // Assert
        assertThat(response.getFamilyName()).isEqualTo(PersonnelTestDataFactory.personDto.getFamilyName());
        assertThat(response.getGivenName()).isEqualTo(PersonnelTestDataFactory.personDto.getGivenName());
        assertThat(response.getDescription()).isEqualTo(PersonnelTestDataFactory.personDto.getDescription());
    }


}

