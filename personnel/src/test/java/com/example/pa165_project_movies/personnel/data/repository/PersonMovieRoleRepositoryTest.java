package com.example.pa165_project_movies.personnel.data.repository;

import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class PersonMovieRoleRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    PersonMovieRoleRepository personMovieRoleRepository;

    private static final UUID movieId = UUID.randomUUID();
    private static final UUID movieId2 = UUID.randomUUID();
    private static final UUID movieId3 = UUID.randomUUID();

    @BeforeEach
    void persistData() {
        MovieRole movieRole = new MovieRole();
        movieRole.setName("Director");
        movieRole.setDescription("Not showing on screen, but takes care of the filming");
        movieRole = entityManager.persist(movieRole);

        MovieRole movieRole2 = new MovieRole();
        movieRole2.setName("Director");
        movieRole2.setDescription("Not showing on screen, but takes care of the filming");
        movieRole2 = entityManager.persist(movieRole);


        Person person = new Person();
        person.setGivenName("Tom");
        person.setFamilyName("Cruise");
        person.setDescription("Tom was a happy actor. His whole life he lived in hopes that one day " +
                "he will die a horrible death during some of his stunts.");

        person = entityManager.persist(person);

        PersonMovieRole personMovieRole = new PersonMovieRole();
        personMovieRole.setDescription("Got on the first Try");
        personMovieRole.setMovieRole(movieRole);
        personMovieRole.setMovieId(movieId);
        personMovieRole.setPerson(person);
        entityManager.persist(personMovieRole);

        personMovieRole = new PersonMovieRole();
        personMovieRole.setDescription("Got on the second Try");
        personMovieRole.setMovieRole(movieRole);
        personMovieRole.setMovieId(movieId2);
        personMovieRole.setPerson(person);
        entityManager.persist(personMovieRole);

        personMovieRole = new PersonMovieRole();
        personMovieRole.setDescription("Got on the third Try");
        personMovieRole.setMovieRole(movieRole);
        personMovieRole.setMovieId(movieId3);
        personMovieRole.setPerson(person);
        entityManager.persist(personMovieRole);

        personMovieRole = new PersonMovieRole();
        personMovieRole.setDescription("Got on the First Try");
        personMovieRole.setMovieRole(movieRole2);
        personMovieRole.setMovieId(movieId3);
        personMovieRole.setPerson(person);
        entityManager.persist(personMovieRole);


        entityManager.flush();
    }

    @Test
    void findAllByMovieId_randomId_noResult() {
        Pageable pageable = PageRequest.of(0, 20);
        Page<PersonMovieRole> result = personMovieRoleRepository.findAllByMovieId(pageable, UUID.randomUUID());
        assertThat(result.getTotalElements()).isEqualTo(0);
    }

    @Test
    void findAllByMovieId_nullId_noResult() {
        Pageable pageable = PageRequest.of(0, 20);
        Page<PersonMovieRole> result = personMovieRoleRepository.findAllByMovieId(pageable, null);
        assertThat(result.getTotalElements()).isEqualTo(0);
    }

    @Test
    void findAllByMovieId_nullPageable_usesDefault() {
        Page<PersonMovieRole> result = personMovieRoleRepository.findAllByMovieId(null, movieId2);
        assertThat(result.getTotalElements()).isEqualTo(1);
    }

    @Test
    void findAllByMovieId_existingId_oneResult() {
        Pageable pageable = PageRequest.of(0, 20);
        Page<PersonMovieRole> result = personMovieRoleRepository.findAllByMovieId(pageable, movieId);
        assertThat(result.getTotalElements()).isEqualTo(1);
    }

    @Test
    void findAllByMovieId_existingId_twoResults() {
        Pageable pageable = PageRequest.of(0, 20);
        Page<PersonMovieRole> result = personMovieRoleRepository.findAllByMovieId(pageable, movieId3);
        assertThat(result.getTotalElements()).isEqualTo(2);
    }

    @Test
    void findAll_fourResults() {
        Pageable pageable = PageRequest.of(0, 20);
        Page<PersonMovieRole> result = personMovieRoleRepository.findAll(pageable);
        assertThat(result.getTotalElements()).isEqualTo(4);
    }
}
