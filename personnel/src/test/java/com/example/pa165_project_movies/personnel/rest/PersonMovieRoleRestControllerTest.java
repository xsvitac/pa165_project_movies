package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.personnel.dto.PersonRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.facade.PersonMovieRoleFacade;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonMovieRoleRestControllerTest {

    @Mock
    private PersonMovieRoleFacade personMovieRoleFacade;
    @InjectMocks
    private PersonMovieRoleRestController personMovieRoleRestController;

    @Test
    void create_validInput_returns_personMovieRole() {
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        PersonRoleCreateDto personRoleCreateDto = new PersonRoleCreateDto();
        personRoleCreateDto.setDescription(personRoleDto.getDescription());
        personRoleCreateDto.setMovieRole(personRoleDto.getMovieRole());
        personRoleCreateDto.setMovieId(personRoleDto.getMovieId());
        personRoleCreateDto.setPerson(personRoleDto.getPerson());
        Mockito.when(personMovieRoleFacade.create(personRoleCreateDto)).thenReturn(personRoleDto);

        PersonRoleDto savedPersonRoleDto = personMovieRoleRestController.createMovieRole(personRoleCreateDto).getBody();

        assertThat(savedPersonRoleDto).isEqualTo(personRoleDto);
    }

    @Test
    void update_validInput_returns_personMovieRole() {
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        Mockito.when(personMovieRoleFacade.update(personRoleDto.getId(), personRoleDto)).thenReturn(personRoleDto);

        PersonRoleDto updatedDto = personMovieRoleRestController.updateMovieRole(personRoleDto.getId(), personRoleDto).getBody();

        assertThat(updatedDto).isEqualTo(personRoleDto);
        Mockito.verify(personMovieRoleFacade, Mockito.times(1)).update(personRoleDto.getId(), personRoleDto);

    }

    @Test
    void delete_validInput_noReturn() {
        Person person = PersonnelTestDataFactory.personEntity;
        UUID id = person.getId();
        Mockito.doNothing().when(personMovieRoleFacade).delete(id);

        personMovieRoleRestController.deleteMovieRole(id);

        Mockito.verify(personMovieRoleFacade, Mockito.times(1)).delete(id);
    }

    @Test
    void findById_personMovieRoleFound_returnsPersonMovieRole() {
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        UUID personId = personRoleDto.getId();
        // Arrange
        Mockito.when(personMovieRoleFacade.findById(personId)).thenReturn(personRoleDto);

        // Act
        PersonRoleDto foundDto = personMovieRoleRestController.getMovieRoleById(personId).getBody();

        // Assert
        assertThat(foundDto).isEqualTo(personRoleDto);
    }

    @Test
    void findById_personMovieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.MOVIE_ID);
        Mockito.when(personMovieRoleFacade.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> personMovieRoleRestController.getMovieRoleById(notPersonId));
    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRole() {
        Page<PersonRoleDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(personMovieRoleFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<PersonRoleDto> foundDtos = personMovieRoleRestController.getMovieRoles(PageRequest.of(0, 20), Optional.empty()).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRoleData() {
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        Page<PersonRoleDto> pageDtos = new PageImpl<>(List.of(personRoleDto));
        Mockito.when(personMovieRoleFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<PersonRoleDto> foundDtos = personMovieRoleRestController.getMovieRoles(PageRequest.of(0, 1), Optional.empty()).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos);

    }
}
