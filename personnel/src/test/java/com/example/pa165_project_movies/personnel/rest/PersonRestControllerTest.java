package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.facade.PersonFacade;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonRestControllerTest {

    @Mock
    private PersonFacade personFacade;

    @InjectMocks
    private PersonRestController personRestController;

    @Test
    void create_validInput_returns_person() {
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        PersonCreateDto personCreateDto = new PersonCreateDto();
        personCreateDto.setGivenName(personDto.getGivenName());
        personCreateDto.setFamilyName(personDto.getFamilyName());
        personCreateDto.setDescription(personDto.getDescription());
        Mockito.when(personFacade.create(personCreateDto)).thenReturn(personDto);

        PersonDto savedPersonDto = personRestController.createPerson(personCreateDto).getBody();

        assertThat(savedPersonDto).isEqualTo(personDto);
    }

    @Test
    void update_validInput_returns_person() {
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        Mockito.when(personFacade.update(personDto.getId(), personDto)).thenReturn(personDto);

        PersonDto updatedDto = personRestController.updatePerson(personDto.getId(), personDto).getBody();

        assertThat(updatedDto).isEqualTo(personDto);
        Mockito.verify(personFacade, Mockito.times(1)).update(personDto.getId(), personDto);

    }

    @Test
    void delete_validInput_noReturn() {
        Person person = PersonnelTestDataFactory.personEntity;
        UUID id = person.getId();
        Mockito.doNothing().when(personFacade).delete(id);

        personRestController.deletePerson(id);

        Mockito.verify(personFacade, Mockito.times(1)).delete(id);
    }

    @Test
    void findById_personFound_returnsPerson() {
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        UUID personId = personDto.getId();
        // Arrange
        Mockito.when(personFacade.findById(personId)).thenReturn(personDto);

        // Act
        PersonDto foundDto = personRestController.getPersonById(personId).getBody();

        // Assert
        assertThat(foundDto).isEqualTo(personDto);
    }

    @Test
    void findById_personNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.ACTOR_MOVIE_ROLE_ID);
        Mockito.when(personFacade.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> personRestController.getPersonById(notPersonId));
    }

    @Test
    void findAll_personsExist_returnsPageOfPerson() {
        Page<PersonDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(personFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<PersonDto> foundDtos = personRestController.getPersons(PageRequest.of(0, 20)).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_personsExist_returnsPageOfPersonData() {
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        Page<PersonDto> pageDtos = new PageImpl<>(List.of(personDto));
        Mockito.when(personFacade.findAll(Mockito.any(Pageable.class))).thenReturn(pageDtos);

        Page<PersonDto> foundDtos = personRestController.getPersons(PageRequest.of(0, 1)).getBody();

        assertThat(foundDtos).isEqualTo(pageDtos);

    }
}
