package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.data.repository.PersonRepository;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;
    @InjectMocks
    private PersonService personService;

    @Test
    void create_validInput_returns_person() {
        Person person = PersonnelTestDataFactory.personEntity;
        Mockito.when(personRepository.save(person)).thenReturn(person);

        Person savedPerson = personService.create(person);

        assertThat(savedPerson).isEqualTo(person);
    }

    @Test
    void update() {
        Person person = PersonnelTestDataFactory.personEntity;
        Mockito.when(personRepository.findById(person.getId())).thenReturn(Optional.of(person));
        Mockito.when(personRepository.save(person)).thenReturn(person);

        Person updatedEntity = personService.update(person);

        assertThat(updatedEntity).isEqualTo(person);
        Mockito.verify(personRepository, Mockito.times(1)).findById(person.getId());
        Mockito.verify(personRepository, Mockito.times(1)).save(person);

    }

    @Test
    void delete_validInput_noReturn() {
        Person person = PersonnelTestDataFactory.personEntity;
        Mockito.doNothing().when(personRepository).delete(person);

        personService.delete(person);

        Mockito.verify(personRepository, Mockito.times(1)).delete(person);
    }

    @Test
    void findById_personFound_returnsPerson() {
        UUID personId = UUID.fromString(PersonnelTestDataFactory.PERSON_ID);
        // Arrange
        Mockito.when(personRepository.findById(personId))
                .thenReturn(Optional.ofNullable(PersonnelTestDataFactory.personEntity));

        // Act
        Person foundEntity = personService.findById(personId);

        // Assert
        assertThat(foundEntity).isEqualTo(PersonnelTestDataFactory.personEntity);
    }

    @Test
    void findById_personNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.ACTOR_MOVIE_ROLE_ID);
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(personService.findById(notPersonId))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_personsExist_returnsPageOfPerson() {
        Page<Person> page = Mockito.mock(Page.class);
        Mockito.when(personRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<Person> foundEntities = personService.findAll(PageRequest.of(0,20));

        assertThat(foundEntities).isEqualTo(page).isNotNull();

    }

    @Test
    void findAll_personsExist_returnsPageOfPersonData() {
        Person person = PersonnelTestDataFactory.personEntity;
        Page<Person> page = new PageImpl<>(List.of(person));
        Mockito.when(personRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<Person> foundEntities = personService.findAll(PageRequest.of(0,1));

        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(person)));

    }

    @Test
    void deleteAll_noReturn() {
        // Arrange

        Mockito.doNothing().when(personRepository).deleteAll();

        // Act
        personService.deleteAll();

        // Assert
        Mockito.verify(personRepository, Mockito.times(1)).deleteAll();
    }
}
