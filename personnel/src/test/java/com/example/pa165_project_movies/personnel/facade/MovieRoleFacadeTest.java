package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.MovieRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import com.example.pa165_project_movies.personnel.mappers.MovieRoleMapper;
import com.example.pa165_project_movies.personnel.service.MovieRoleService;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class MovieRoleFacadeTest {


    @Mock
    private MovieRoleService movieRoleService;
    @Mock
    private MovieRoleMapper movieRoleMapper;
    @InjectMocks
    private MovieRoleFacade movieRoleFacade;

    @Test
    void create_validInput_returns_movieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        MovieRoleCreateDto movieRoleCreateDto = new MovieRoleCreateDto();
        movieRoleCreateDto.setDescription(movieRoleDto.getDescription());
        movieRoleCreateDto.setName(movieRoleDto.getName());
        Mockito.when(movieRoleMapper.mapToEntity(movieRoleCreateDto)).thenReturn(movieRole);
        Mockito.when(movieRoleService.create(movieRole)).thenReturn(movieRole);
        Mockito.when(movieRoleMapper.mapToDto(movieRole)).thenReturn(movieRoleDto);

        MovieRoleDto savedMovieRoleDto = movieRoleFacade.create(movieRoleCreateDto);

        assertThat(savedMovieRoleDto).isEqualTo(movieRoleDto);
    }

    @Test
    void update_validInput_returns_movieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        Mockito.when(movieRoleMapper.mapToEntity(movieRoleDto)).thenReturn(movieRole);
        Mockito.when(movieRoleService.update(movieRole)).thenReturn(movieRole);
        Mockito.when(movieRoleMapper.mapToDto(movieRole)).thenReturn(movieRoleDto);

        MovieRoleDto updatedDto = movieRoleFacade.update(movieRoleDto.getId(), movieRoleDto);

        assertThat(updatedDto).isEqualTo(movieRoleDto);
        Mockito.verify(movieRoleService, Mockito.times(1)).update(movieRole);

    }

    @Test
    void delete_validInput_noReturn() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        UUID id = movieRole.getId();
        Mockito.when(movieRoleService.findById(id)).thenReturn(movieRole);
        Mockito.doNothing().when(movieRoleService).delete(movieRole);

        movieRoleFacade.delete(id);

        Mockito.verify(movieRoleService, Mockito.times(1)).delete(movieRole);
    }

    @Test
    void findById_movieRoleFound_returnsMovieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        // Arrange
        Mockito.when(movieRoleService.findById(movieRole.getId())).thenReturn(movieRole);
        Mockito.when(movieRoleMapper.mapToDto(movieRole)).thenReturn(movieRoleDto);

        // Act
        MovieRoleDto foundDto = movieRoleFacade.findById(movieRole.getId());

        // Assert
        assertThat(foundDto).isEqualTo(movieRoleDto);
    }

    @Test
    void findById_movieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.MOVIE_ID);
        Mockito.when(movieRoleService.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> movieRoleFacade.findById(notPersonId));
    }

    @Test
    void findAll_movieRolesExist_returnsPageOfMovieRole() {
        Page<MovieRole> page = Mockito.mock(Page.class);
        Page<MovieRoleDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(movieRoleService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(movieRoleMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<MovieRoleDto> foundDtos = movieRoleFacade.findAll(PageRequest.of(0, 20));

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_movieRoleExist_returnsPageOfMovieRoleData() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        MovieRoleDto movieRoleDto = PersonnelTestDataFactory.movieRoleDto;
        Page<MovieRole> page = new PageImpl<>(List.of(movieRole));
        Page<MovieRoleDto> pageDtos = new PageImpl<>(List.of(movieRoleDto));
        Mockito.when(movieRoleService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(movieRoleMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<MovieRoleDto> foundDtos = movieRoleFacade.findAll(PageRequest.of(0, 1));

        assertThat(foundDtos).isEqualTo(pageDtos);

    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(movieRoleService).deleteAll();

        // Act
        movieRoleFacade.deleteAll();

        // Assert
        Mockito.verify(movieRoleService, Mockito.times(1)).deleteAll();
    }
}
