package com.example.pa165_project_movies.personnel.util;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class PersonnelTestDataFactory {

    public static PersonDto personDto = getPersonDtoFactory();
    public static Person personEntity = getPersonEntityFactory();
    public static PersonMovieRole personMovieRoleEntity = getPersonMovieRoleEntityFactory();
    public static MovieRole movieRoleEntity = getActorMovieRoleEntityFactory();

    public static PersonRoleDto personMovieRoleDto = getPersonMovieRoleDtoFactory();
    public static MovieRoleDto movieRoleDto = getMovieRoleDtoFactory();

    public static final String ACTOR_MOVIE_ROLE_ID = "03437baf-d686-4e11-8fe7-bb97be4fcf72";
    public static final String MAIN_LEAD_MOVIE_ROLE_ID = "3037e506-5dea-497d-93ee-3e6436f753e0";
    public static final String DIRECTOR_MOVIE_ROLE_ID = "08f90140-b551-40b7-bccc-ccdf2ec1343f";
    public static final String PERSON_ID = "cb27d8af-84aa-418b-ade7-e6093523e2ed";
    public static final String PERSON_ID1 = "5fc03087-d265-11e7-b8c6-83e29cd24f4c";
    public static final String ACTOR_PERSON_MOVIE_ROLE_ID = "05097105-90bb-4a20-b25c-86229c4318b7";
    public static final String ACTOR_PERSON_MOVIE_ROLE_ID1 = "1016724b-ccf3-45a8-84b4-f580f626804a";
    public static final String MAIN_LEAD_PERSON_MOVIE_ROLE_ID = "7dd459bf-396f-41be-a821-e0b13aeb14ba";
    public static final String MAIN_LEAD_PERSON_MOVIE_ROLE_ID1 = "fcc3644a-fff9-4aa2-b3e4-40d911e4e93a";
    public static final String DIRECTOR_PERSON_MOVIE_ROLE_ID = "9a35d7ee-fcd5-4d5d-b4b0-aa8d0f301ea9";
    public static final String DIRECTOR_PERSON_MOVIE_ROLE_ID1 = "4d89f61f-e81e-4f8d-8cfc-21c790dd0244";
    public static final String MOVIE_ID = "c18532af-2d89-4eca-b1ed-5a2652d269db";
    public static final String MOVIE_ID1 = "ad1d892f-d54d-4430-84a5-d12706a47662";

    public static final OffsetDateTime OFFSET_DATE_TIME_MOMENT_OF_CREATION = OffsetDateTime.now();

    public static final List<Person> allPersonEntities = List.of(getPersonEntityFactory(), getPersonEntityFactory1());
    public static final List<PersonDto> allPersonDtos = List.of(getPersonDtoFactory(), getPersonDtoFactory1());

    public static RequestPostProcessor mockAdminUser() {
        return SecurityMockMvcRequestPostProcessors.user("Admin").authorities(new SimpleGrantedAuthority(Oauth2Scope.ADMIN));
    }

    private static PersonDto getPersonDtoFactory() {
        PersonDto person = new PersonDto();
        person.setId(UUID.fromString(PERSON_ID));
        person.setGivenName("Tom");
        person.setFamilyName("Cruise");
        person.setDescription("Tom was a happy actor. His whole life he lived in hopes that one day " +
                "he will die a horrible death during some of his stunts.");

        return person;
    }

    private static PersonDto getPersonDtoFactory1() {
        PersonDto person = new PersonDto();
        person.setId(UUID.fromString(PERSON_ID1));
        person.setGivenName("Crum");
        person.setFamilyName("Toise");
        person.setDescription("Crum was a happy actor. His whole life he lived in hopes that one day " +
                "he will die a horrible death during some of his stunts.");

        return person;
    }


    private static MovieRole getActorMovieRoleEntityFactory() {
        MovieRole movieRole = new MovieRole();
        movieRole.setId(UUID.fromString(ACTOR_MOVIE_ROLE_ID));
        movieRole.setName("Generic Actor");
        movieRole.setDescription("Generic actor no main lead");

        movieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        movieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);

        return movieRole;
    }
    private static MovieRoleDto getMovieRoleDtoFactory() {
        MovieRoleDto movieRoleDto1 = new MovieRoleDto();
        movieRoleDto1.setId(UUID.fromString(ACTOR_MOVIE_ROLE_ID));
        movieRoleDto1.setName("Generic Actor");
        movieRoleDto1.setDescription("Generic actor no main lead");

        return movieRoleDto1;
    }

    private static MovieRole getMainLeadMovieRoleEntityFactory() {
        MovieRole movieRole = new MovieRole();
        movieRole.setId(UUID.fromString(MAIN_LEAD_MOVIE_ROLE_ID));
        movieRole.setName("MC");
        movieRole.setDescription("Main character maybe hero or the villain");

        movieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        movieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);

        return movieRole;
    }

    private static MovieRole getDirectorMovieRoleEntityFactory() {
        MovieRole movieRole = new MovieRole();
        movieRole.setId(UUID.fromString(DIRECTOR_MOVIE_ROLE_ID));
        movieRole.setName("Director");
        movieRole.setDescription("Not showing on screen, but takes care of the filming");

        movieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        movieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);

        return movieRole;
    }

    private static Person getPersonEntityFactory() {
        Person person = new Person();
        person.setId(UUID.fromString(PERSON_ID));
        person.setGivenName("Tom");
        person.setFamilyName("Cruise");
        person.setDescription("Tom was a happy actor. His whole life he lived in hopes that one day " +
                "he will die a horrible death during some of his stunts.");

        person.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        person.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);

        MovieRole actorRole = getActorMovieRoleEntityFactory();
        PersonMovieRole personMovieRole = new PersonMovieRole();
        personMovieRole.setId(UUID.fromString(ACTOR_PERSON_MOVIE_ROLE_ID));
        personMovieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDescription("Got on the first Try");
        personMovieRole.setMovieRole(actorRole);
        personMovieRole.setMovieId(UUID.fromString(MOVIE_ID));
        personMovieRole.setPerson(person);

        MovieRole McRole = getMainLeadMovieRoleEntityFactory();
        PersonMovieRole personMovieRoleMC = new PersonMovieRole();
        personMovieRoleMC.setId(UUID.fromString(MAIN_LEAD_PERSON_MOVIE_ROLE_ID));
        personMovieRoleMC.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleMC.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleMC.setDescription("Why are there so many young masters around me?");
        personMovieRoleMC.setMovieRole(McRole);
        personMovieRoleMC.setMovieId(UUID.fromString(MOVIE_ID));
        personMovieRoleMC.setPerson(person);

        MovieRole director = getDirectorMovieRoleEntityFactory();
        PersonMovieRole personMovieRoleDirector = new PersonMovieRole();
        personMovieRoleDirector.setId(UUID.fromString(DIRECTOR_PERSON_MOVIE_ROLE_ID));
        personMovieRoleDirector.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleDirector.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleDirector.setDescription("After many years of filming I was bored so I decided to make my own movie");
        personMovieRoleDirector.setMovieRole(director);
        personMovieRoleDirector.setMovieId(UUID.fromString(MOVIE_ID));
        personMovieRoleDirector.setPerson(person);

        actorRole.setPersonRoles(Set.of(personMovieRole));
        McRole.setPersonRoles(Set.of(personMovieRoleMC));
        director.setPersonRoles(Set.of(personMovieRoleDirector));

        person.setMovieRoles(Set.of(personMovieRole, personMovieRoleMC, personMovieRoleDirector));

        return person;
    }

    private static Person getPersonEntityFactory1() {
        Person person = new Person();
        person.setId(UUID.fromString(PERSON_ID1));
        person.setGivenName("Crum");
        person.setFamilyName("Toise");
        person.setDescription("Crum was a happy actor. His whole life he lived in hopes that one day " +
                "he will die a horrible death during some of his stunts.");

        person.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        person.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);

        MovieRole actorRole = getActorMovieRoleEntityFactory();
        PersonMovieRole personMovieRole = new PersonMovieRole();
        personMovieRole.setId(UUID.fromString(ACTOR_PERSON_MOVIE_ROLE_ID1));
        personMovieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDescription("Got on the first Try");
        personMovieRole.setMovieRole(actorRole);
        personMovieRole.setMovieId(UUID.fromString(MOVIE_ID));
        personMovieRole.setPerson(person);

        MovieRole McRole = getMainLeadMovieRoleEntityFactory();
        PersonMovieRole personMovieRoleMC = new PersonMovieRole();
        personMovieRoleMC.setId(UUID.fromString(MAIN_LEAD_PERSON_MOVIE_ROLE_ID1));
        personMovieRoleMC.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleMC.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleMC.setDescription("Why are there so many young masters around me?");
        personMovieRoleMC.setMovieRole(McRole);
        personMovieRoleMC.setMovieId(UUID.fromString(MOVIE_ID));
        personMovieRoleMC.setPerson(person);

        MovieRole director = getDirectorMovieRoleEntityFactory();
        PersonMovieRole personMovieRoleDirector = new PersonMovieRole();
        personMovieRoleDirector.setId(UUID.fromString(DIRECTOR_PERSON_MOVIE_ROLE_ID1));
        personMovieRoleDirector.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleDirector.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRoleDirector.setDescription("After many years of filming I was bored so I decided to make my own movie");
        personMovieRoleDirector.setMovieRole(director);
        personMovieRoleDirector.setMovieId(UUID.fromString(MOVIE_ID1));
        personMovieRoleDirector.setPerson(person);

        actorRole.setPersonRoles(Set.of(personMovieRole));
        McRole.setPersonRoles(Set.of(personMovieRoleMC));
        director.setPersonRoles(Set.of(personMovieRoleDirector));

        person.setMovieRoles(Set.of(personMovieRole, personMovieRoleMC, personMovieRoleDirector));

        return person;
    }

    private static PersonMovieRole getPersonMovieRoleEntityFactory() {
        MovieRole actorRole = getActorMovieRoleEntityFactory();
        PersonMovieRole personMovieRole = new PersonMovieRole();
        personMovieRole.setId(UUID.fromString(ACTOR_PERSON_MOVIE_ROLE_ID));
        personMovieRole.setDateOfCreation(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDateOfLastChange(OFFSET_DATE_TIME_MOMENT_OF_CREATION);
        personMovieRole.setDescription("Got on the first Try");
        personMovieRole.setMovieRole(actorRole);
        personMovieRole.setMovieId(UUID.fromString(MOVIE_ID));
        return personMovieRole;
    }

    private static PersonRoleDto getPersonMovieRoleDtoFactory() {
        PersonRoleDto personRoleDto = new PersonRoleDto();

        personRoleDto.setId(UUID.fromString(ACTOR_PERSON_MOVIE_ROLE_ID));
        personRoleDto.setMovieId(UUID.fromString(MOVIE_ID));
        personRoleDto.setDescription("Got on the first Try");
        personRoleDto.setPerson(getPersonDtoFactory());
        personRoleDto.setMovieRole(getMovieRoleDtoFactory());
        return personRoleDto;
    }
}
