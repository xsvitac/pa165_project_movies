package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.mappers.PersonMapper;
import com.example.pa165_project_movies.personnel.service.PersonService;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonFacadeTest {

    @Mock
    private PersonService personService;

    @Mock
    private PersonMapper personMapper;

    @InjectMocks
    private PersonFacade personFacade;

    @Test
    void create_validInput_returns_person() {
        Person person = PersonnelTestDataFactory.personEntity;
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        PersonCreateDto personCreateDto = new PersonCreateDto();
        personCreateDto.setGivenName(personDto.getGivenName());
        personCreateDto.setFamilyName(personDto.getFamilyName());
        personCreateDto.setDescription(personDto.getDescription());
        Mockito.when(personMapper.mapToEntity(personCreateDto)).thenReturn(person);
        Mockito.when(personService.create(person)).thenReturn(person);
        Mockito.when(personMapper.mapToDto(person)).thenReturn(personDto);

        PersonDto savedPersonDto = personFacade.create(personCreateDto);

        assertThat(savedPersonDto).isEqualTo(personDto);
    }

    @Test
    void update_validInput_returns_person() {
        Person person = PersonnelTestDataFactory.personEntity;
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        Mockito.when(personMapper.mapToEntity(personDto)).thenReturn(person);
        Mockito.when(personService.update(person)).thenReturn(person);
        Mockito.when(personMapper.mapToDto(person)).thenReturn(personDto);

        PersonDto updatedDto = personFacade.update(personDto.getId(), personDto);

        assertThat(updatedDto).isEqualTo(personDto);
        Mockito.verify(personService, Mockito.times(1)).update(person);

    }

    @Test
    void delete_validInput_noReturn() {
        Person person = PersonnelTestDataFactory.personEntity;
        UUID id = person.getId();
        Mockito.when(personService.findById(id)).thenReturn(person);
        Mockito.doNothing().when(personService).delete(person);

        personFacade.delete(id);

        Mockito.verify(personService, Mockito.times(1)).delete(person);
    }

    @Test
    void findById_personFound_returnsPerson() {
        Person person = PersonnelTestDataFactory.personEntity;
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        UUID personId = UUID.fromString(PersonnelTestDataFactory.PERSON_ID);
        // Arrange
        Mockito.when(personService.findById(personId)).thenReturn(person);
        Mockito.when(personMapper.mapToDto(person)).thenReturn(personDto);

        // Act
        PersonDto foundDto = personFacade.findById(personId);

        // Assert
        assertThat(foundDto).isEqualTo(personDto);
    }

    @Test
    void findById_personNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.ACTOR_MOVIE_ROLE_ID);
        Mockito.when(personService.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> personFacade.findById(notPersonId));
    }

    @Test
    void findAll_personsExist_returnsPageOfPerson() {
        Page<Person> page = Mockito.mock(Page.class);
        Page<PersonDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(personService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(personMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<PersonDto> foundDtos = personFacade.findAll(PageRequest.of(0, 20));

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_personsExist_returnsPageOfPersonData() {
        Person person = PersonnelTestDataFactory.personEntity;
        PersonDto personDto = PersonnelTestDataFactory.personDto;
        Page<Person> page = new PageImpl<>(List.of(person));
        Page<PersonDto> pageDtos = new PageImpl<>(List.of(personDto));
        Mockito.when(personService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(personMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<PersonDto> foundDtos = personFacade.findAll(PageRequest.of(0, 1));

        assertThat(foundDtos).isEqualTo(pageDtos);

    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(personService).deleteAll();

        // Act
        personFacade.deleteAll();

        // Assert
        Mockito.verify(personService, Mockito.times(1)).deleteAll();
    }
}
