package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import com.example.pa165_project_movies.personnel.data.repository.MovieRoleRepository;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
class MovieRoleServiceTest {

    @Mock
    private MovieRoleRepository movieRoleRepository;

    @InjectMocks
    private MovieRoleService movieRoleService;

    @Test
    void create_validInput_returns_movieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        Mockito.when(movieRoleRepository.save(movieRole)).thenReturn(movieRole);

        MovieRole savedMovieRole = movieRoleService.create(movieRole);

        assertThat(savedMovieRole).isEqualTo(movieRole);
    }

    @Test
    void update_validInput_returns_movieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        Mockito.when(movieRoleRepository.findById(movieRole.getId())).thenReturn(Optional.of(movieRole));
        Mockito.when(movieRoleRepository.save(movieRole)).thenReturn(movieRole);

        MovieRole updatedEntity = movieRoleService.update(movieRole);

        assertThat(updatedEntity).isEqualTo(movieRole);
        Mockito.verify(movieRoleRepository, Mockito.times(1)).findById(movieRole.getId());
        Mockito.verify(movieRoleRepository, Mockito.times(1)).save(movieRole);

    }

    @Test
    void delete_validInput_noReturn() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        Mockito.doNothing().when(movieRoleRepository).delete(movieRole);

        movieRoleService.delete(movieRole);

        Mockito.verify(movieRoleRepository, Mockito.times(1)).delete(movieRole);
    }

    @Test
    void findById_movieRoleFound_returnsMovieRole() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        UUID movieRoleId = movieRole.getId();
        // Arrange
        Mockito.when(movieRoleRepository.findById(movieRoleId))
                .thenReturn(Optional.ofNullable(movieRole));

        // Act
        MovieRole foundEntity = movieRoleService.findById(movieRoleId);

        // Assert
        assertThat(foundEntity).isEqualTo(movieRole);
    }

    @Test
    void findById_movieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.ACTOR_MOVIE_ROLE_ID);
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(movieRoleService.findById(notPersonId))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_movieRolesExist_returnsPageOfMovieRole() {
        Page<MovieRole> page = Mockito.mock(Page.class);
        Mockito.when(movieRoleRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<MovieRole> foundEntities = movieRoleService.findAll(PageRequest.of(0, 20));

        assertThat(foundEntities).isEqualTo(page).isNotNull();

    }

    @Test
    void findAll_movieRoleExist_returnsPageOfMovieRoleData() {
        MovieRole movieRole = PersonnelTestDataFactory.movieRoleEntity;
        Page<MovieRole> page = new PageImpl<>(List.of(movieRole));
        Mockito.when(movieRoleRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        Page<MovieRole> foundEntities = movieRoleService.findAll(PageRequest.of(0, 1));

        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(movieRole)));

    }
    @Test
    void deleteAll_noReturn() {
        // Arrange

        Mockito.doNothing().when(movieRoleRepository).deleteAll();

        // Act
        movieRoleService.deleteAll();

        // Assert
        Mockito.verify(movieRoleRepository, Mockito.times(1)).deleteAll();
    }
}
