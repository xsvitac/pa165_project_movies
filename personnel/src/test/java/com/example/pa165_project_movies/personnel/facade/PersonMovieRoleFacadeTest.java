package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.PersonRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import com.example.pa165_project_movies.personnel.mappers.PersonMovieRoleMapper;
import com.example.pa165_project_movies.personnel.service.PersonMovieRoleService;
import com.example.pa165_project_movies.personnel.util.PersonnelTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PersonMovieRoleFacadeTest {

    @Mock
    private PersonMovieRoleService personMovieRoleService;
    @Mock
    private PersonMovieRoleMapper personMovieRoleMapper;

    @InjectMocks
    private PersonMovieRoleFacade personMovieRoleFacade;

    @Test
    void create_validInput_returns_personMovieRole() {
        PersonMovieRole personRole = PersonnelTestDataFactory.personMovieRoleEntity;
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        PersonRoleCreateDto personRoleCreateDto = new PersonRoleCreateDto();
        personRoleCreateDto.setDescription(personRoleDto.getDescription());
        personRoleCreateDto.setMovieRole(personRoleDto.getMovieRole());
        personRoleCreateDto.setMovieId(personRoleDto.getMovieId());
        personRoleCreateDto.setPerson(personRoleDto.getPerson());
        Mockito.when(personMovieRoleMapper.mapToEntity(personRoleCreateDto)).thenReturn(personRole);
        Mockito.when(personMovieRoleService.create(personRole)).thenReturn(personRole);
        Mockito.when(personMovieRoleMapper.mapToDto(personRole)).thenReturn(personRoleDto);

        PersonRoleDto savedPersonRoleDto = personMovieRoleFacade.create(personRoleCreateDto);

        assertThat(savedPersonRoleDto).isEqualTo(personRoleDto);
    }

    @Test
    void update_validInput_returns_personMovieRole() {
        PersonMovieRole personRole = PersonnelTestDataFactory.personMovieRoleEntity;
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        Mockito.when(personMovieRoleMapper.mapToEntity(personRoleDto)).thenReturn(personRole);
        Mockito.when(personMovieRoleService.update(personRole)).thenReturn(personRole);
        Mockito.when(personMovieRoleMapper.mapToDto(personRole)).thenReturn(personRoleDto);

        PersonRoleDto updatedDto = personMovieRoleFacade.update(personRoleDto.getId(), personRoleDto);

        assertThat(updatedDto).isEqualTo(personRoleDto);
        Mockito.verify(personMovieRoleService, Mockito.times(1)).update(personRole);

    }

    @Test
    void delete_validInput_noReturn() {
        PersonMovieRole personRole = PersonnelTestDataFactory.personMovieRoleEntity;
        UUID id = personRole.getId();
        Mockito.when(personMovieRoleService.findById(id)).thenReturn(personRole);
        Mockito.doNothing().when(personMovieRoleService).delete(personRole);

        personMovieRoleFacade.delete(id);

        Mockito.verify(personMovieRoleService, Mockito.times(1)).delete(personRole);
    }

    @Test
    void findById_personMovieRoleFound_returnsPersonMovieRole() {
        PersonMovieRole personRole = PersonnelTestDataFactory.personMovieRoleEntity;
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        // Arrange
        Mockito.when(personMovieRoleService.findById(personRole.getId())).thenReturn(personRole);
        Mockito.when(personMovieRoleMapper.mapToDto(personRole)).thenReturn(personRoleDto);

        // Act
        PersonRoleDto foundDto = personMovieRoleFacade.findById(personRole.getId());

        // Assert
        assertThat(foundDto).isEqualTo(personRoleDto);
    }

    @Test
    void findById_personMovieRoleNotFound_throwsResourceNotFoundException() {
        UUID notPersonId = UUID.fromString(PersonnelTestDataFactory.MOVIE_ID);
        Mockito.when(personMovieRoleService.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> personMovieRoleFacade.findById(notPersonId));
    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRole() {
        Page<PersonMovieRole> page = Mockito.mock(Page.class);
        Page<PersonRoleDto> pageDtos = Mockito.mock(Page.class);
        Mockito.when(personMovieRoleService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(personMovieRoleMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<PersonRoleDto> foundDtos = personMovieRoleFacade.findAll(PageRequest.of(0, 20));

        assertThat(foundDtos).isEqualTo(pageDtos).isNotNull();

    }

    @Test
    void findAll_personMovieRolesExist_returnsPageOfPersonMovieRoleData() {
        PersonMovieRole personRole = PersonnelTestDataFactory.personMovieRoleEntity;
        PersonRoleDto personRoleDto = PersonnelTestDataFactory.personMovieRoleDto;
        Page<PersonMovieRole> page = new PageImpl<>(List.of(personRole));
        Page<PersonRoleDto> pageDtos = new PageImpl<>(List.of(personRoleDto));
        Mockito.when(personMovieRoleService.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(personMovieRoleMapper.mapToPageDto(page)).thenReturn(pageDtos);

        Page<PersonRoleDto> foundDtos = personMovieRoleFacade.findAll(PageRequest.of(0, 1));

        assertThat(foundDtos).isEqualTo(pageDtos);

    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(personMovieRoleService).deleteAll();

        // Act
        personMovieRoleFacade.deleteAll();

        // Assert
        Mockito.verify(personMovieRoleService, Mockito.times(1)).deleteAll();
    }
}
