package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.PersonRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.mappers.PersonMovieRoleMapper;
import com.example.pa165_project_movies.personnel.service.PersonMovieRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PersonMovieRoleFacade {
    private final PersonMovieRoleService personMovieRoleService;
    private final PersonMovieRoleMapper personMovieRoleMapper;

    @Autowired
    public PersonMovieRoleFacade(PersonMovieRoleService personMovieRoleService, PersonMovieRoleMapper personMovieRoleMapper) {
        this.personMovieRoleService = personMovieRoleService;
        this.personMovieRoleMapper = personMovieRoleMapper;
    }

    @Transactional
    public PersonRoleDto create(PersonRoleCreateDto personRoleCreateDto) {
        return personMovieRoleMapper.mapToDto(personMovieRoleService.create(personMovieRoleMapper.mapToEntity(personRoleCreateDto)));
    }

    @Transactional
    public PersonRoleDto update(UUID id, PersonRoleDto personDto) throws ResourceNotFoundException {
        return personMovieRoleMapper.mapToDto(personMovieRoleService.update(personMovieRoleMapper.mapToEntity(personDto)));
    }

    @Transactional
    public void delete(UUID id) {
        personMovieRoleService.delete(personMovieRoleService.findById(id));
    }

    @Transactional
    public void deleteAll(){
        personMovieRoleService.deleteAll();
    }

    @Transactional(readOnly = true)
    public PersonRoleDto findById(UUID uuid) throws ResourceNotFoundException {
        return personMovieRoleMapper.mapToDto(personMovieRoleService.findById(uuid));
    }

    @Transactional(readOnly = true)
    public Page<PersonRoleDto> findAll(Pageable pageable) {
        return personMovieRoleMapper.mapToPageDto(personMovieRoleService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public Page<PersonRoleDto> findAllByMovieId(Pageable pageable, UUID uuid) {
        return personMovieRoleMapper.mapToPageDto(personMovieRoleService.findAllByMovieId(pageable, uuid));
    }
}
