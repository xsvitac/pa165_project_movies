package com.example.pa165_project_movies.personnel.mappers;

import com.example.pa165_project_movies.personnel.dto.PersonRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = { PersonMapper.class, MovieRoleMapper.class })
public interface PersonMovieRoleMapper {
    PersonMovieRole mapToEntity(PersonRoleDto personRoleDto);
    PersonMovieRole mapToEntity(PersonRoleCreateDto personRoleCreateDto);
    PersonRoleDto mapToDto(PersonMovieRole personMovieRole);
    List<PersonRoleDto> mapToList(List<PersonMovieRole> personMovieRoles);
    default Page<PersonRoleDto> mapToPageDto(Page<PersonMovieRole> personMovieRoles) {
        return new PageImpl<>(this.mapToList(personMovieRoles.getContent()), personMovieRoles.getPageable(), personMovieRoles.getTotalPages());
    }
}
