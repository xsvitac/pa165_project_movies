package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.personnel.facade.PersonFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@OpenAPIDefinition(
        info = @Info(title = "Movie person management service",
        version = "${project.version}",
        description = """
                Service for managing persons involved in a movie.
                """),
        servers = @Server(description = "custom server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "${server.port}"),
        })
)
@RestController
@RequestMapping(path = "${rest.path.persons}", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonRestController {
    private final PersonFacade personFacade;

    @Autowired
    public PersonRestController(PersonFacade personFacade) {
        this.personFacade = personFacade;
    }

    @Operation(
            summary = "Returns a newly created person",
            description = "Creates a new person from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<PersonDto> createPerson(@Valid @RequestBody PersonCreateDto personDto) {
        return new ResponseEntity<>(personFacade.create(personDto), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Updates person information",
            description = "Updates person information from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping
    public ResponseEntity<PersonDto> updatePerson(@PathVariable UUID id, @Valid @RequestBody PersonDto personDto) {
        return ResponseEntity.ok(personFacade.update(id, personDto));
    }

    @Operation(
            summary = "Deletes a person",
            description = "Deletes person found by id.",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable UUID id) {
        personFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Paged persons",
            description = "Returns a page of persons."
    )
    @GetMapping
    public ResponseEntity<Page<PersonDto>> getPersons(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(personFacade.findAll(pageable));
    }

    @Operation(
            summary = "Returns person's information",
            description = "Finds a person by id and returns their information.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
            }
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<PersonDto> getPersonById(@PathVariable UUID id) {
        return ResponseEntity.ok(personFacade.findById(id));
    }
}
