package com.example.pa165_project_movies.personnel.data.repository;

import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonMovieRoleRepository extends JpaRepository<PersonMovieRole, UUID> {
    Page<PersonMovieRole> findAllByMovieId(Pageable pageable, UUID movieId);
}
