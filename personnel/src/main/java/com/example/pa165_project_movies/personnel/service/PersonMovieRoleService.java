package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.PersonMovieRole;
import com.example.pa165_project_movies.personnel.data.repository.PersonMovieRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PersonMovieRoleService implements ServiceLayer<PersonMovieRole, UUID> {
    private final PersonMovieRoleRepository personMovieRoleRepository;

    @Autowired
    public PersonMovieRoleService(PersonMovieRoleRepository personMovieRoleRepository) {
        this.personMovieRoleRepository = personMovieRoleRepository;
    }

    @Transactional
    @Override
    public PersonMovieRole create(PersonMovieRole entity) {
        return personMovieRoleRepository.save(entity);
    }

    @Transactional
    @Override
    public PersonMovieRole update(PersonMovieRole entity) throws ResourceNotFoundException {
        personMovieRoleRepository.findById(entity.getId()).orElseThrow(() -> new ResourceNotFoundException(String.format("PersonMovieRole(id: '%s') not found", entity.getId().toString())));
        return personMovieRoleRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(PersonMovieRole entity) {
        personMovieRoleRepository.delete(entity);
    }

    @Transactional
    @Override
    public void deleteAll() {
        personMovieRoleRepository.deleteAll();
    }

    @Transactional(readOnly = true)
    @Override
    public PersonMovieRole findById(UUID uuid) throws ResourceNotFoundException {
        return personMovieRoleRepository.findById(uuid).orElseThrow(() -> new ResourceNotFoundException(String.format("PersonMovieRole(id: '%s') not found", uuid.toString())));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PersonMovieRole> findAll(Pageable pageable) {
        return personMovieRoleRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Page<PersonMovieRole> findAllByMovieId(Pageable pageable, UUID uuid) {
        return personMovieRoleRepository.findAllByMovieId(pageable, uuid);
    }
}
