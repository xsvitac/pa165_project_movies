package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.personnel.dto.PersonRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonRoleDto;
import com.example.pa165_project_movies.personnel.facade.PersonMovieRoleFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@OpenAPIDefinition(
        info = @Info(title = "Person's role management service",
                version = "${project.version}",
                description = """
                Service for managing roles of people involved in a movie.
                """),
        servers = @Server(description = "custom server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "${server.port}"),
        })
)
@RestController
@RequestMapping(path = "${rest.path.persons-roles}", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonMovieRoleRestController {
    private final PersonMovieRoleFacade personMovieRoleFacade;

    @Autowired
    public PersonMovieRoleRestController(PersonMovieRoleFacade personMovieRoleFacade) {
        this.personMovieRoleFacade = personMovieRoleFacade;
    }

    @Operation(
            summary = "Returns a newly created person-movie-role binding",
            description = "Creates a new person-movie-role binding from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<PersonRoleDto> createMovieRole(@Valid @RequestBody PersonRoleCreateDto personRoleCreateDto) {
        return new ResponseEntity<>(personMovieRoleFacade.create(personRoleCreateDto), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Updates person-movie-role binding information",
            description = "Updates person-movie-role binding from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping
    public ResponseEntity<PersonRoleDto> updateMovieRole(@PathVariable UUID id, @Valid @RequestBody PersonRoleDto personDto) {
        return ResponseEntity.ok(personMovieRoleFacade.update(id, personDto));
    }

    @Operation(
            summary = "Deletes a person-movie-role binding",
            description = "Deletes person-movie-role binding found by main id.",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteMovieRole(@PathVariable UUID id) {
        personMovieRoleFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Paged person-movie-role bindings",
            description = "Returns a page of person-movie-role bindings."
    )
    @GetMapping
    public ResponseEntity<Page<PersonRoleDto>> getMovieRoles(@ParameterObject Pageable pageable, @RequestParam Optional<UUID> movieId) {
        if (movieId.isPresent()) {
            return ResponseEntity.ok(personMovieRoleFacade.findAllByMovieId(pageable, movieId.get()));
        }
        return ResponseEntity.ok(personMovieRoleFacade.findAll(pageable));
    }

    @Operation(
            summary = "Returns person-movie-role binding",
            description = "Finds a person-movie-role by id and returns its information.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
            }
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<PersonRoleDto> getMovieRoleById(@PathVariable UUID id) {
        return ResponseEntity.ok(personMovieRoleFacade.findById(id));
    }
}
