package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.personnel.dto.MovieRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.personnel.facade.MovieRoleFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@OpenAPIDefinition(
        info = @Info(title = "Movie role management service",
                version = "${project.version}",
                description = """
                Service for managing roles a person can fulfill.
                """),
        servers = @Server(description = "custom server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "${server.port}"),
        })
)
@RestController
@RequestMapping(path = "${rest.path.roles}", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieRoleRestController {
    private final MovieRoleFacade movieRoleFacade;

    @Autowired
    public MovieRoleRestController(MovieRoleFacade movieRoleFacade) {
        this.movieRoleFacade = movieRoleFacade;
    }

    @Operation(
            summary = "Returns a newly created role",
            description = "Creates a new role from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PostMapping
    public ResponseEntity<MovieRoleDto> createMovieRole(@Valid @RequestBody MovieRoleCreateDto movieRoleCreateDto) {
        return new ResponseEntity<>(movieRoleFacade.create(movieRoleCreateDto), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Updates role information",
            description = "Updates role information from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @PutMapping(path = "/{id}")
    public ResponseEntity<MovieRoleDto> updateMovieRole(@PathVariable UUID id, @Valid @RequestBody MovieRoleDto movieRoleDto) {
        return ResponseEntity.ok(movieRoleFacade.update(id, movieRoleDto));
    }

    @Operation(
            summary = "Deletes a role",
            description = "Deletes role found by id.",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteMovieRole(@PathVariable UUID id) {
        movieRoleFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Paged roles",
            description = "Returns a page of roles."
    )
    @GetMapping
    public ResponseEntity<Page<MovieRoleDto>> getMovieRoles(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(movieRoleFacade.findAll(pageable));
    }

    @Operation(
            summary = "Returns role information",
            description = "Finds a role by id and returns its information.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
            }
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<MovieRoleDto> getMovieRoleById(@PathVariable UUID id) {
        return ResponseEntity.ok(movieRoleFacade.findById(id));
    }
}
