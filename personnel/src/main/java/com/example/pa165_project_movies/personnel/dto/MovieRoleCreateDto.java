package com.example.pa165_project_movies.personnel.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Schema(title = "New movie role", description = "Role of a person involved in a movie")
@Data
public class MovieRoleCreateDto {
    @Schema(description = "Role title")
    @NotBlank
    private String name;

    @Schema(description = "Role description")
    @NotNull
    private String description;
}
