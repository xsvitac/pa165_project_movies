package com.example.pa165_project_movies.personnel.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Schema(title = "New person", description = "New person involved in a movie's making")
@Data
public class PersonCreateDto {
    @Schema(description = "Person's given (first) name")
    @NotBlank
    private String givenName;

    @Schema(description = "Person's family (last) name")
    @NotBlank
    private String familyName;

    @Schema(description = "Person's description")
    @NotNull
    private String description;
}
