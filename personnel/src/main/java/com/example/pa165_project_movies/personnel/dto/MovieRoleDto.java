package com.example.pa165_project_movies.personnel.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "Movie role", description = "Role of a person involved in a movie")
@Data
public class MovieRoleDto {
    @Schema(description = "ID of the role")
    @NotNull
    private UUID id;

    @Schema(description = "Role title")
    @NotBlank
    private String name;

    @Schema(description = "Role description")
    @NotBlank
    private String description;
}
