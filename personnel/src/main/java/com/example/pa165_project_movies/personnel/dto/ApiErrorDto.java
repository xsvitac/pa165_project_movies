// This class is based on the one presented in seminars
// https://gitlab.fi.muni.cz/pa165/seminar-spring-boot/-/blob/a2efd114fc35106d97f1ba985378bfa8133433f4/spring-boot-social-network/src/main/java/cz/muni/fi/pa165/socialnetwork/rest/exceptionhandling/ApiError.java

package com.example.pa165_project_movies.personnel.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;

@Schema(title = "API Error", description = "Information about an error caused by an exception.")
@Data
public class ApiErrorDto {
    @Schema(description = "UTC timestamp of the error")
    private OffsetDateTime timestamp;

    @Schema(description = "HTTP status code")
    private HttpStatus status;

    @Schema(description = "Error message")
    private String message;

    @Schema(description = "HTTP URL of the request that caused the error")
    private String path;

    public ApiErrorDto() {
        this.timestamp = OffsetDateTime.now();
    }

    public ApiErrorDto(HttpStatus status, String message, String path) {
        this.timestamp = OffsetDateTime.now();
        this.status = status;
        this.message = message;
        this.path = path;
    }

    public ApiErrorDto(OffsetDateTime timestamp, HttpStatus status, String message, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.path = path;
    }
}
