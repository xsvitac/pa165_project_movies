package com.example.pa165_project_movies.personnel.mappers;

import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.personnel.data.model.Person;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonMapper {
    Person mapToEntity(PersonDto personDto);
    Person mapToEntity(PersonCreateDto personDto);
    PersonDto mapToDto(Person person);
    List<PersonDto> mapToList(List<Person> personList);
    default Page<PersonDto> mapToPageDto(Page<Person> personPage) {
        return new PageImpl<>(this.mapToList(personPage.getContent()), personPage.getPageable(), personPage.getTotalPages());
    }
}
