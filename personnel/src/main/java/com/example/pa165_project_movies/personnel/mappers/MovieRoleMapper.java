package com.example.pa165_project_movies.personnel.mappers;

import com.example.pa165_project_movies.personnel.dto.MovieRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovieRoleMapper {
    MovieRole mapToEntity(MovieRoleDto movieRoleDto);
    MovieRole mapToEntity(MovieRoleCreateDto movieRoleCreateDto);
    MovieRoleDto mapToDto(MovieRole movieRole);
    List<MovieRoleDto> mapToList(List<MovieRole> movieRoles);
    default Page<MovieRoleDto> mapToPageDto(Page<MovieRole> movieRoles) {
        return new PageImpl<>(this.mapToList(movieRoles.getContent()), movieRoles.getPageable(), movieRoles.getTotalPages());
    }
}
