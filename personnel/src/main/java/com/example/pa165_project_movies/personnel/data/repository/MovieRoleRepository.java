package com.example.pa165_project_movies.personnel.data.repository;

import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieRoleRepository extends JpaRepository<MovieRole, UUID> {
}
