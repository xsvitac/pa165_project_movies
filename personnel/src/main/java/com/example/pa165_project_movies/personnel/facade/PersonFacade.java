package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.PersonCreateDto;
import com.example.pa165_project_movies.personnel.dto.PersonDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.mappers.PersonMapper;
import com.example.pa165_project_movies.personnel.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PersonFacade {
    private final PersonService personService;
    private final PersonMapper personMapper;

    @Autowired
    public PersonFacade(PersonService personService, PersonMapper personMapper) {
        this.personService = personService;
        this.personMapper = personMapper;
    }

    @Transactional
    public PersonDto create(PersonCreateDto personDto) {
        return personMapper.mapToDto(personService.create(personMapper.mapToEntity(personDto)));
    }

    @Transactional
    public PersonDto update(UUID id, PersonDto personDto) throws ResourceNotFoundException {
        return personMapper.mapToDto(personService.update(personMapper.mapToEntity(personDto)));
    }

    @Transactional
    public void delete(UUID id) {
        personService.delete(personService.findById(id));
    }

    @Transactional
    public void deleteAll(){
        personService.deleteAll();
    }

    @Transactional(readOnly = true)
    public PersonDto findById(UUID uuid) throws ResourceNotFoundException {
        return personMapper.mapToDto(personService.findById(uuid));
    }

    @Transactional(readOnly = true)
    public Page<PersonDto> findAll(Pageable pageable) {
        return personMapper.mapToPageDto(personService.findAll(pageable));
    }
}
