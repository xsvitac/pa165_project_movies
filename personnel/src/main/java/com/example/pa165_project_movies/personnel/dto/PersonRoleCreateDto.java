package com.example.pa165_project_movies.personnel.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Schema(title = "New person's role binding", description = "Binding of a person and their role in the movie's production")
@Data
public class PersonRoleCreateDto {
    @Schema(description = "Narrower description of the person's role")
    @NotNull
    private String description;

    @NotNull
    PersonDto person;

    @NotNull
    MovieRoleDto movieRole;

    @Schema(description = "ID of the movie tied to the person's role")
    @NotNull
    UUID movieId;
}
