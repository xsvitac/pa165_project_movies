package com.example.pa165_project_movies.personnel.facade;

import com.example.pa165_project_movies.personnel.dto.MovieRoleCreateDto;
import com.example.pa165_project_movies.personnel.dto.MovieRoleDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.mappers.MovieRoleMapper;
import com.example.pa165_project_movies.personnel.service.MovieRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class MovieRoleFacade {
    private final MovieRoleService movieRoleService;
    private final MovieRoleMapper movieRoleMapper;

    @Autowired
    public MovieRoleFacade(MovieRoleService movieRoleService, MovieRoleMapper movieRoleMapper) {
        this.movieRoleService = movieRoleService;
        this.movieRoleMapper = movieRoleMapper;
    }

    @Transactional
    public MovieRoleDto create(MovieRoleCreateDto movieRoleCreateDto) {
        return movieRoleMapper.mapToDto(movieRoleService.create(movieRoleMapper.mapToEntity(movieRoleCreateDto)));
    }

    @Transactional
    public MovieRoleDto update(UUID id, MovieRoleDto movieRoleDto) throws ResourceNotFoundException {
        return movieRoleMapper.mapToDto(movieRoleService.update(movieRoleMapper.mapToEntity(movieRoleDto)));
    }

    @Transactional
    public void delete(UUID id) {
        movieRoleService.delete(movieRoleService.findById(id));
    }

    @Transactional
    public void deleteAll() {
        movieRoleService.deleteAll();
    }

    @Transactional(readOnly = true)
    public MovieRoleDto findById(UUID uuid) throws ResourceNotFoundException {
        return movieRoleMapper.mapToDto(movieRoleService.findById(uuid));
    }

    @Transactional(readOnly = true)
    public Page<MovieRoleDto> findAll(Pageable pageable) {
        return movieRoleMapper.mapToPageDto(movieRoleService.findAll(pageable));
    }
}
