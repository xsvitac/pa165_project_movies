package com.example.pa165_project_movies.personnel.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.personnel.facade.MovieRoleFacade;
import com.example.pa165_project_movies.personnel.facade.PersonFacade;
import com.example.pa165_project_movies.personnel.facade.PersonMovieRoleFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.dbclear}", produces = MediaType.APPLICATION_JSON_VALUE)
public class DBClearRestController {

    private final PersonFacade personFacade;
    private final PersonMovieRoleFacade personMovieRoleFacade;
    private final MovieRoleFacade movieRoleFacade;

    @Autowired
    public DBClearRestController(PersonFacade personFacade, PersonMovieRoleFacade personMovieRoleFacade, MovieRoleFacade movieRoleFacade) {
        this.personFacade = personFacade;
        this.personMovieRoleFacade = personMovieRoleFacade;
        this.movieRoleFacade = movieRoleFacade;
    }

    @Operation(
            summary = "Delete all personnel, movie roles and bindings in the database",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping
    public ResponseEntity<Page<UUID>> deletePersonnelAndRoles() {
        personMovieRoleFacade.deleteAll();
        personFacade.deleteAll();
        movieRoleFacade.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
