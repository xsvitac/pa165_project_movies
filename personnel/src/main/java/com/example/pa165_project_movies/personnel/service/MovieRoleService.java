package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.MovieRole;
import com.example.pa165_project_movies.personnel.data.repository.MovieRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class MovieRoleService implements ServiceLayer<MovieRole, UUID> {
    private final MovieRoleRepository movieRoleRepository;

    @Autowired
    public MovieRoleService(MovieRoleRepository movieRoleRepository) {
        this.movieRoleRepository = movieRoleRepository;
    }

    @Transactional
    @Override
    public MovieRole create(MovieRole entity) {
        return movieRoleRepository.save(entity);
    }

    @Transactional
    @Override
    public MovieRole update(MovieRole entity) throws ResourceNotFoundException {
        movieRoleRepository.findById(entity.getId()).orElseThrow(() -> new ResourceNotFoundException(String.format("MovieRole(id: '%s') not found", entity.getId().toString())));
        return movieRoleRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(MovieRole entity) {
        movieRoleRepository.delete(entity);
    }

    @Transactional
    @Override
    public void deleteAll() {
        movieRoleRepository.deleteAll();
    }

    @Transactional(readOnly = true)
    @Override
    public MovieRole findById(UUID uuid) throws ResourceNotFoundException {
        return movieRoleRepository.findById(uuid).orElseThrow(() -> new ResourceNotFoundException(String.format("MovieRole(id: '%s') not found", uuid.toString())));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MovieRole> findAll(Pageable pageable) {
        return movieRoleRepository.findAll(pageable);
    }
}
