package com.example.pa165_project_movies.personnel.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * {@code ServiceLayer} declares CRUD operations that service layer should implement.
 *
 * @param <T> Entity type
 * @param <ID> Entity ID type
 */
public interface ServiceLayer<T, ID> {
    T create(T entity);
    T update(T entity);
    void delete(T entity);
    void deleteAll();
    T findById(ID id);
    Page<T> findAll(Pageable pageable);
}
