package com.example.pa165_project_movies.personnel.service;

import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.personnel.data.model.Person;
import com.example.pa165_project_movies.personnel.data.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PersonService implements ServiceLayer<Person, UUID> {
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Transactional
    @Override
    public Person create(Person entity) {
        return personRepository.save(entity);
    }

    @Transactional
    @Override
    public Person update(Person entity) throws ResourceNotFoundException {
        personRepository.findById(entity.getId()).orElseThrow(() -> new ResourceNotFoundException(String.format("Person(id: '%s') not found", entity.getId().toString())));
        return personRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(Person entity) {
        personRepository.delete(entity);
    }

    @Transactional
    @Override
    public void deleteAll() {
        personRepository.deleteAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Person findById(UUID uuid) throws ResourceNotFoundException {
        return personRepository.findById(uuid).orElseThrow(() -> new ResourceNotFoundException(String.format("Person(id: '%s') not found", uuid.toString())));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Person> findAll(Pageable pageable) {
        return personRepository.findAll(pageable);
    }
}
