import json
import random
import locust

movies_path = ':8083/api/v1/movies'
movie_reviews_path = ':8082/api/v1/movie-reviews'
movie_average_path = ':8082/api/v1/movie-average'
recommend_path = ':8082/api/v1/recommend'

class AnonymousUser(locust.HttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.movie_id = ''
        self.movie_rating = {}

    def _get_a_random_movie(self):
        with self.client.get(f'{movies_path}?size=100', catch_response=True) as res:
            number_of_elements = res.json()["numberOfElements"]
            movie_offset = random.randrange(0, number_of_elements)
            return res.json()["content"][movie_offset]["id"]

    @locust.task
    def get_a_random_movie(self):
        self._get_a_random_movie()

    @locust.task
    def get_movie_reviews(self):
        movie_id = self._get_a_random_movie()
        self.client.get(f'{movie_reviews_path}/{movie_id}')

    def _get_average_rating_of_a_movie(self):
        movie_id = self._get_a_random_movie()
        with self.client.get(f'{movie_average_path}/{movie_id}', catch_response=True) as res:
            return res.json()

    @locust.task
    def get_average_rating_of_a_movie(self):
        self._get_average_rating_of_a_movie()

    @locust.task
    def get_movie_recommendations(self):
        movie_criteria = self._get_average_rating_of_a_movie()
        del movie_criteria['movieId']
        self.client.get(f'{recommend_path}', headers={'Content-Type': 'application/json'}, data=json.dumps(movie_criteria))
