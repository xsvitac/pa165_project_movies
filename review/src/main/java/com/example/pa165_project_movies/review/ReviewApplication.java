package com.example.pa165_project_movies.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class ReviewApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewApplication.class);
    }
}