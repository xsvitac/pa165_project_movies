package com.example.pa165_project_movies.review.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.proxy.HibernateProxy;

import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column
    @NotNull
    private UUID userId;

    @Column
    @NotNull
    private UUID movieId;

    @Column
    private Float noveltyRating;

    @Column
    private Float footageRating;

    @Column
    private Float scriptRating;

    @Column
    private Float experienceRating;

    @Column
    @NotNull
    private Float overallRating;

    @Column(columnDefinition = "LONGTEXT")
    private String reviewText;

    @Column
    @NotNull
    private OffsetDateTime dateOfCreation;

    @Column
    private OffsetDateTime dateOfLastChange;

    @PrePersist
    private void onCreate() {
        this.dateOfCreation = this.dateOfLastChange = OffsetDateTime.now();
    }

    @PreUpdate
    private void onUpdate() {
        this.dateOfLastChange = OffsetDateTime.now();
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ?
                ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ?
                ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Review entity = (Review) o;
        return getId() != null && Objects.equals(getId(), entity.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ?
                ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() :
                getClass().hashCode();
    }
}
