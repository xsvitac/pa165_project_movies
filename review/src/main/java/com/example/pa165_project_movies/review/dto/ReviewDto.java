package com.example.pa165_project_movies.review.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.UUID;

@Schema(title = "Movie review", description = "Review of a movie. Used for retrieving a review.")
@Data
public class ReviewDto {
    @Schema(description = "ID of the review")
    @NotNull
    private UUID id;

    @Schema(description = "ID of the reviewed movie")
    @NotNull
    private UUID movieId;

    @Schema(description = "ID of the user who reviewed the movie")
    @NotNull
    private UUID userId;

    @Schema(description = "Movie's novelty rating")
    @Nullable
    private Float noveltyRating;

    @Schema(description = "Movie's cinematography rating")
    @Nullable
    private Float footageRating;

    @Schema(description = "Movie's script rating")
    @Nullable
    private Float scriptRating;

    @Schema(description = "Movie's experience rating")
    @Nullable
    private Float experienceRating;

    @Schema(description = "Movie's overall rating")
    @NotNull
    private Float overallRating;

    @Schema(description = "Review text")
    @NotNull
    @Size(max = 10_000)
    private String reviewText;

    @Schema(description = "Date and time of review's creation")
    @Nullable
    private OffsetDateTime dateOfCreation;

    @Schema(description = "Date of last change to the review")
    @NotNull
    private OffsetDateTime dateOfLastChange;
}
