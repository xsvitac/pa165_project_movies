package com.example.pa165_project_movies.review.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Schema(title = "Movie review rating", description = "Movie criteria rating. Used for retrieving rating criteria values.")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReviewRatingDto {
    @Schema(description = "ID of the reviewed movie")
    @NotNull
    private UUID movieId;

    @Schema(description = "Movie's novelty rating")
    @Nullable
    private Double noveltyRating;

    @Schema(description = "Movie's cinematography rating")
    @Nullable
    private Double footageRating;

    @Schema(description = "Movie's script rating")
    @Nullable
    private Double scriptRating;

    @Schema(description = "Movie's experience rating")
    @Nullable
    private Double experienceRating;

    @Schema(description = "Movie's overall rating")
    @Nullable
    private Double overallRating;
}
