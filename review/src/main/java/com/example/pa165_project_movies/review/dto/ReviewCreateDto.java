package com.example.pa165_project_movies.review.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.UUID;

@Schema(title = "New movie review", description = "Review of a movie. Used for creating a review.")
@Data
public class ReviewCreateDto {
    @Schema(description = "ID of the reviewed movie")
    @NotNull
    private UUID movieId;

    @Schema(description = "ID of the user who reviewed the movie")
    @NotNull
    private UUID userId;

    @Schema(description = "Movie's novelty rating")
    @Nullable
    private Float noveltyRating;

    @Schema(description = "Movie's cinematography rating")
    @Nullable
    private Float footageRating;

    @Schema(description = "Movie's script rating")
    @Nullable
    private Float scriptRating;

    @Schema(description = "Movie's experience rating")
    @Nullable
    private Float experienceRating;

    @Schema(description = "Movie's overall rating")
    @NotNull
    private Float overallRating;

    @Schema(description = "Review text")
    @NotNull
    @Size(max = 10_000)
    private String reviewText;
}
