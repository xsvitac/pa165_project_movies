package com.example.pa165_project_movies.review.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import lombok.Data;

@Schema(title = "Movie review criteria", description = "Criteria of a movie review.")
@Data
public class ReviewCriteriaDto {
    @Schema(description = "Movie's novelty rating")
    @Nullable
    private Float noveltyRating;

    @Schema(description = "Movie's cinematography rating")
    @Nullable
    private Float footageRating;

    @Schema(description = "Movie's script rating")
    @Nullable
    private Float scriptRating;

    @Schema(description = "Movie's experience rating")
    @Nullable
    private Float experienceRating;

    @Schema(description = "Movie's overall rating")
    @Nullable
    private Float overallRating;
}
