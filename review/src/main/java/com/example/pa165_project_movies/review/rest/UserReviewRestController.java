package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.user-reviews}", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserReviewRestController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public UserReviewRestController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Paged Reviews by user id",
            description = "Returns a page containing all reviews made by a user."
    )
    @GetMapping(path = "/{userId}")
    public ResponseEntity<Page<ReviewDto>> listReviewsByUserId(@PathVariable UUID userId, @ParameterObject Pageable pageable) {
        return ResponseEntity.ok(reviewFacade.listByUserId(userId, pageable));
    }
}
