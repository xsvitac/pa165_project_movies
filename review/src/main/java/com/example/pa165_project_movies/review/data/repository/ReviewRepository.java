package com.example.pa165_project_movies.review.data.repository;

import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.data.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
@Repository
public interface ReviewRepository extends JpaRepository<Review, UUID> {

    @Query("SELECT r FROM Review r WHERE r.userId = :id")
    Page<Review> listByUserId(@Param("id") UUID id, Pageable pageable);

    @Query("SELECT r FROM Review r WHERE r.movieId = :id")
    Page<Review> listByMovieId(@Param("id") UUID id, Pageable pageable);

    @Query("SELECT r.movieId FROM Review r GROUP BY r.movieId HAVING " +
            "(:noveltyRating is NULL or (AVG(r.noveltyRating) <= :noveltyRating+1.0 and AVG(r.noveltyRating) >= :noveltyRating-1.0)) AND " +
            "(:footageRating is NULL or (AVG(r.footageRating) <= :footageRating+1.0 and AVG(r.footageRating) >= :footageRating-1.0)) AND " +
            "(:scriptRating is NULL or (AVG(r.scriptRating) <= :scriptRating+1.0 and AVG(r.scriptRating) >= :scriptRating-1.0)) AND " +
            "(:experienceRating is NULL or (AVG(r.experienceRating) <= :experienceRating+1.0 and AVG(r.experienceRating) >= :experienceRating-1.0)) AND " +
            "(:overallRating is NULL or (AVG(r.overallRating) <= :overallRating+1.0 and AVG(r.overallRating) >= :overallRating-1.0))")
    Page<UUID> getReviewsOfSimilarMovies(
            @Param("noveltyRating") Float noveltyRating,
            @Param("footageRating") Float footageRating,
            @Param("scriptRating") Float scriptRating,
            @Param("experienceRating") Float experienceRating,
            @Param("overallRating") Float overallRating,
            Pageable pageable
            );

    @Query("SELECT NEW com.example.pa165_project_movies.review.dto.ReviewRatingDto(r.movieId, ROUND(AVG(r.noveltyRating), 1), ROUND(AVG(r.footageRating), 1), ROUND(AVG(r.scriptRating), 1), ROUND(AVG(r.experienceRating), 1), ROUND(AVG(r.overallRating), 1)) FROM Review r WHERE r.movieId = :movieId GROUP BY r.movieId")
    Optional<ReviewRatingDto> getMovieAverageRating(@Param("movieId") UUID movieId);
}
