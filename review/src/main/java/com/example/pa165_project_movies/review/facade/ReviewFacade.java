package com.example.pa165_project_movies.review.facade;


import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.mappers.ReviewMapper;
import com.example.pa165_project_movies.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class ReviewFacade {

    private final ReviewService reviewService;
    private final ReviewMapper reviewMapper;

    @Autowired
    public ReviewFacade(ReviewService reviewService, ReviewMapper reviewMapper) {
        this.reviewService = reviewService;
        this.reviewMapper = reviewMapper;
    }

    @Transactional(readOnly = true)
    public ReviewDto findById(UUID id) {
        return reviewMapper.mapToDto(reviewService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<ReviewDto> findAll(Pageable pageable) {
        return reviewMapper.mapToPageDto(
                reviewService.findAll(pageable)
        );
    }

    @Transactional(readOnly = true)
    public Page<UUID> getReviewsOfSimilarMovies(ReviewCriteriaDto reviewCriteriaDto, Pageable pageable) {
        return reviewService.getReviewsOfSimilarMovies(reviewCriteriaDto, pageable);
    }

    @Transactional
    public ReviewRatingDto getMovieAverageRating(UUID movieId) {
        return reviewService.getMovieAverageRating(movieId);
    }

    @Transactional(readOnly = true)
    public Page<ReviewDto> listByUserId(UUID userId, Pageable pageable) {
        return reviewMapper.mapToPageDto(
                reviewService.listByUserId(userId, pageable)
        );
    }

    @Transactional(readOnly = true)
    public Page<ReviewDto> listByMovieId(UUID movieId, Pageable pageable) {
        return reviewMapper.mapToPageDto(
                reviewService.listByMovieId(movieId, pageable)
        );
    }

    @Transactional
    public ReviewDto create(ReviewCreateDto review) {
        return reviewMapper.mapToDto(
                reviewService.create(
                        reviewMapper.mapToEntity(review)
                )
        );
    }

    @Transactional
    public ReviewDto update(UUID id, ReviewDto review) {
        review.setId(id);
        return reviewMapper.mapToDto(
                reviewService.update(
                        reviewMapper.mapToEntity(review)
                )
        );
    }

    @Transactional
    public void delete(UUID id) {
        reviewService.delete(reviewService.findById(id));
    }

    @Transactional
    public void deleteAll() {
        reviewService.deleteAll();
    }
}
