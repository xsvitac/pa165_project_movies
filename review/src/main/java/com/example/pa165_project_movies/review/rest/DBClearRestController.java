package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${rest.path.dbclear}", produces = MediaType.APPLICATION_JSON_VALUE)
public class DBClearRestController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public DBClearRestController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Delete all reviews in the database",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.ADMIN + "'"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.ADMIN})
    )
    @DeleteMapping
    public ResponseEntity<Void> deleteAllReviews() {
        reviewFacade.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
