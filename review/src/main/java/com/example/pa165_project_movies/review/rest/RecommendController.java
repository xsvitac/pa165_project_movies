package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.recommend}", produces = MediaType.APPLICATION_JSON_VALUE)
public class RecommendController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public RecommendController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Get reviews of similar movies",
            description = "Get reviews of movies whose average reviews are similar to the one provided.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
            }
    )
    @GetMapping
    public ResponseEntity<Page<UUID>> getReviewsOfSimilarMovies(
            @Valid @RequestBody ReviewCriteriaDto reviewCriteriaDto,
            @ParameterObject Pageable pageable
    ) {
        return ResponseEntity.ok(reviewFacade.getReviewsOfSimilarMovies(reviewCriteriaDto, pageable));
    }

}
