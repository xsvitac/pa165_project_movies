package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.movie-average}", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieAverageRestController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public MovieAverageRestController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Get the average rating of a movie",
            description = "Get the average rating of a movie's rating criteria",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "no movie reviews found"),
            }
    )
    @GetMapping(path = "/{movieId}")
    public ResponseEntity<ReviewRatingDto> getMovieAverageRating(@PathVariable UUID movieId) {
        return ResponseEntity.ok(reviewFacade.getMovieAverageRating(movieId));
    }
}
