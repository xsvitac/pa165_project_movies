package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "${rest.path.movie-reviews}", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieReviewRestController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public MovieReviewRestController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Paged Reviews by user id",
            description = "Returns a page containing all reviews of a movie."
    )
    @GetMapping(path = "/{movieId}")
    public ResponseEntity<Page<ReviewDto>> listReviewsByMovieId(@PathVariable UUID movieId, @ParameterObject Pageable pageable) {
        return ResponseEntity.ok(reviewFacade.listByMovieId(movieId, pageable));
    }
}
