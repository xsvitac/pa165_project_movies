package com.example.pa165_project_movies.review.rest;


import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.common.config.SecurityConst;
import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@OpenAPIDefinition(
        info = @Info(title = "Review management service",
                version = "${project.version}",
                description = """
                Service for managing reviews of movies.
                """),
        servers = @Server(description = "custom server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "${server.port}"),
        })
)
@RestController
@RequestMapping(path = "${rest.path.reviews}", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReviewRestController {

    private final ReviewFacade reviewFacade;

    @Autowired
    public ReviewRestController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @Operation(
            summary = "Paged Reviews",
            description = "Returns a page containing all reviews on site."
    )
    @GetMapping
    public ResponseEntity<Page<ReviewDto>> getAllReviews(
            @ParameterObject Pageable pageable
    ) {
        return ResponseEntity.ok(reviewFacade.findAll(pageable));
    }

    @Operation(
            summary = "Returns review's information",
            description = "Finds a review by id and returns its information.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
            }
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<ReviewDto> getReviewById(@PathVariable UUID id) {
        return ResponseEntity.ok(reviewFacade.findById(id));
    }

    @Operation(
            summary = "Returns a newly created review",
            description = "Creates a new review from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "201"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.AUTH_WRITE + "' or 'userId' field does not match the caller's ID"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.AUTH_WRITE})
    )
    @PreAuthorize("hasAuthority(T(com.example.pa165_project_movies.common.config.Oauth2Scope).ADMIN) or #review.getUserId().equals(T(com.example.pa165_project_movies.common.util.UUID5).from(T(com.example.pa165_project_movies.common.config.SecurityConst).UUID5_NAMESPACE_EMAIL, authentication.getName()))")
    @PostMapping
    public ResponseEntity<ReviewDto> createReview(
            @Valid @RequestBody ReviewCreateDto review
            ) {
        return new ResponseEntity<>(reviewFacade.create(review), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Updates review information",
            description = "Updates review information from a DTO.",
            responses = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", description = "wrong DTO data passed"),
                    @ApiResponse(responseCode = "404", description = "entity for update not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.AUTH_WRITE + "' or 'userId' field does not match the caller's ID"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.AUTH_WRITE})
    )
    @PreAuthorize("hasAuthority(T(com.example.pa165_project_movies.common.config.Oauth2Scope).ADMIN) or #review.getUserId().equals(T(com.example.pa165_project_movies.common.util.UUID5).from(T(com.example.pa165_project_movies.common.config.SecurityConst).UUID5_NAMESPACE_EMAIL, authentication.getName()))")
    @PutMapping(path = "/{id}")
    public ResponseEntity<ReviewDto> updateReview(
            @PathVariable UUID id,
            @RequestBody ReviewDto review
    ) {
        return ResponseEntity.ok(reviewFacade.update(id, review));
    }

    @Operation(
            summary = "Deletes a review",
            description = "Deletes review found by id.",
            responses = {
                    @ApiResponse(responseCode = "204"),
                    @ApiResponse(responseCode = "400", description = "wrong id format passed"),
                    @ApiResponse(responseCode = "404", description = "entity for deletion not found"),
                    @ApiResponse(responseCode = "403", description = "access token does not have scope '" + Oauth2Scope.AUTH_WRITE + "' or 'userId' field does not match the caller's ID"),
            },
            security = @SecurityRequirement(name = SecurityConst.SECURITY_SCHEME_NAME, scopes = {Oauth2Scope.AUTH_WRITE})
    )
    @PreAuthorize("hasAuthority(T(com.example.pa165_project_movies.common.config.Oauth2Scope).ADMIN) or @reviewFacade.findById(#id).getUserId().equals(T(com.example.pa165_project_movies.common.util.UUID5).from(T(com.example.pa165_project_movies.common.config.SecurityConst).UUID5_NAMESPACE_EMAIL, authentication.getName()))")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteReview(@PathVariable UUID id) {
        reviewFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}

