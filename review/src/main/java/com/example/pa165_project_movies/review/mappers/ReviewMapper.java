package com.example.pa165_project_movies.review.mappers;

import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.data.model.Review;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
@Mapper(componentModel = "spring")
public interface ReviewMapper {

    ReviewDto mapToDto(Review review);

    Review mapToEntity(ReviewDto review);

    Review mapToEntity(ReviewCreateDto review);

    List<ReviewDto> mapToList(List<Review> reviews);

    default Page<ReviewDto> mapToPageDto(Page<Review> reviews) {
        return new PageImpl<>(mapToList(reviews.getContent()), reviews.getPageable(), reviews.getTotalPages());
    }
}
