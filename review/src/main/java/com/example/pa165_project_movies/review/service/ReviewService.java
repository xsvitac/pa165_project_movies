package com.example.pa165_project_movies.review.service;

import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.review.data.model.Review;
import com.example.pa165_project_movies.review.data.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;


@Service
public class ReviewService {

    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Transactional(readOnly = true)
    public Review findById(UUID id) {
        return reviewRepository.findById(id)
                .orElseThrow(() ->
                        new ResourceNotFoundException(
                                "Review with id: " + id + " was not found."
                        )
                );
    }

    @Transactional(readOnly = true)
    public Page<Review> findAll(Pageable pageable) {
        return reviewRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Page<Review> listByUserId(UUID userId, Pageable pageable) {
        return Optional.ofNullable(reviewRepository.listByUserId(userId, pageable))
                .orElseThrow(() ->
                        new ResourceNotFoundException(
                                "No reviews from user with id: " + userId + " were found."
                        )
                );
    }

    @Transactional(readOnly = true)
    public Page<Review> listByMovieId(UUID movieId, Pageable pageable) {
        return Optional.ofNullable(reviewRepository.listByMovieId(movieId, pageable))
                .orElseThrow(() ->
                        new ResourceNotFoundException(
                                "No reviews of a movie with id: " + movieId + " were found."
                        )
                );
    }

    @Transactional(readOnly = true)
    public Page<UUID> getReviewsOfSimilarMovies(ReviewCriteriaDto reviewCriteriaDto, Pageable pageable) {
        return reviewRepository.getReviewsOfSimilarMovies(
                reviewCriteriaDto.getNoveltyRating(),
                reviewCriteriaDto.getFootageRating(),
                reviewCriteriaDto.getScriptRating(),
                reviewCriteriaDto.getExperienceRating(),
                reviewCriteriaDto.getOverallRating(),
                pageable);
    }

    @Transactional
    public ReviewRatingDto getMovieAverageRating(UUID movieId) throws ResourceNotFoundException {
        return reviewRepository.getMovieAverageRating(movieId).orElseThrow(() -> new ResourceNotFoundException("No reviews for movie with id " + movieId.toString() + " exist."));
    }

    @Transactional
    public Review create(Review review) {
        return reviewRepository.save(review);
    }

    @Transactional
    public Review update(Review review) {
        findById(review.getId());
        return reviewRepository.save(review);
    }

    @Transactional
    public void delete(Review review) {
        reviewRepository.delete(review);
    }

    @Transactional
    public void deleteAll() {
        reviewRepository.deleteAll();
    }
}
