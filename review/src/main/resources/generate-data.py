#!/usr/bin/env python3

import datetime
from datetime import timedelta
import random
import sys
import pandas as pd
import scipy.stats as stats
import uuid

def random_datetime(start, end):
    diff = end - start
    diff_us = int(diff.total_seconds() // 1) * 1_000_000 + diff.microseconds
    return start + timedelta(microseconds=random.randrange(0, diff_us))

def truncnorm(lower, upper, mean=0.0, sd=1.0, size=1):
    a = (lower - mean) / sd
    b = (upper - mean) / sd
    return stats.truncnorm.rvs(a, b, loc=mean, scale=sd, size=size)

def main():
    pd.set_option('display.max_columns', 999)
    pd.set_option('display.width', 9999)

    now = datetime.datetime.now().astimezone()
    year_ago = (now - timedelta(days=365)).astimezone()

    lower, upper = 1, 10

    rating_criteria = [
        'EXPERIENCE_RATING',
        'FOOTAGE_RATING',
        'NOVELTY_RATING',
        'SCRIPT_RATING',
    ]

    movie_list = (line.rstrip('\n') for line in sys.stdin)
    reviews = []

    for movie_id in movie_list:
        reviews_per_movie = random.randint(10, 20)

        criteria_values = {
            'OVERALL_RATING': truncnorm(lower, upper, mean=8, sd=2, size=reviews_per_movie),
        }
        overall_mean = criteria_values['OVERALL_RATING'].mean()

        for criteria in rating_criteria:
            mean = truncnorm(lower, upper, mean=max(overall_mean+1, 10), sd=2, size=1)
            criteria_values[criteria] = truncnorm(lower, upper, mean, sd=1.5, size=reviews_per_movie)

        dates_of_creation = [random_datetime(year_ago, now).astimezone() for _ in range(reviews_per_movie)]
        dates_of_last_change = [random_datetime(x, now).astimezone() if random.choice([True, False]) else x for x in dates_of_creation]

        criteria_values.update(
            {
                'DATE_OF_CREATION': dates_of_creation,
                'DATE_OF_LAST_CHANGE': dates_of_last_change,
                'ID': [str(uuid.uuid4()) for _ in range(reviews_per_movie)],
                'MOVIE_ID': movie_id,
                'USER_ID': random.sample(user_list, reviews_per_movie),
            })

        df = pd.DataFrame(criteria_values)
        reviews.append(df)

    reviews = pd.concat(reviews)
    # print(reviews)
    # print(reviews.groupby('MOVIE_ID').describe())

    for _, review in reviews.iterrows():
        experience_rating = round(review['EXPERIENCE_RATING'], 1)
        footage_rating = round(review['FOOTAGE_RATING'], 1)
        novelty_rating = round(review['NOVELTY_RATING'], 1)
        overall_rating = round(review['OVERALL_RATING'], 1)
        script_rating = round(review['SCRIPT_RATING'], 1)

        review_sentences = []

        if overall_rating >= 9:
            review_sentences.append('Overall the movie was amazing!')
        elif overall_rating >= 8 and overall_rating < 9:
            review_sentences.append('Overall the movie was great!')
        elif overall_rating >= 6 and overall_rating < 8:
            review_sentences.append('Overall the movie was good.')
        elif overall_rating >= 5 and overall_rating < 6:
            review_sentences.append('Overall the movie was okay.')
        elif overall_rating >= 4 and overall_rating < 5:
            review_sentences.append('Overall the movie was pretty bad.')
        elif overall_rating >= 0 and overall_rating < 4:
            review_sentences.append('Overall the movie was terrible.')

        if experience_rating >= 9:
            review_sentences.append('The experience was amazing!')
        elif experience_rating >= 8 and experience_rating < 9:
            review_sentences.append('The experience was great!')
        elif experience_rating >= 6 and experience_rating < 8:
            review_sentences.append('The experience was good.')
        elif experience_rating >= 5 and experience_rating < 6:
            review_sentences.append('The experience was okay.')
        elif experience_rating >= 4 and experience_rating < 5:
            review_sentences.append('The experience was pretty bad.')
        elif experience_rating >= 0 and experience_rating < 4:
            review_sentences.append('The experience was terrible.')

        if footage_rating >= 9:
            review_sentences.append('The footage was amazing!')
        elif footage_rating >= 8 and footage_rating < 9:
            review_sentences.append('The footage was great!')
        elif footage_rating >= 6 and footage_rating < 8:
            review_sentences.append('The footage was good.')
        elif footage_rating >= 5 and footage_rating < 6:
            review_sentences.append('The footage was okay.')
        elif footage_rating >= 4 and footage_rating < 5:
            review_sentences.append('The footage was pretty bad.')
        elif footage_rating >= 0 and footage_rating < 4:
            review_sentences.append('The footage was terrible.')

        if footage_rating < 5:
            if script_rating >= 7:
                review_sentences.append('But at least the script made up for it.')

        if novelty_rating >= 9:
            review_sentences.append('The movie was very novel!')
        elif novelty_rating >= 8 and novelty_rating < 9:
            review_sentences.append('The movie had some novel aspects!')
        elif novelty_rating >= 5 and novelty_rating < 8:
            review_sentences.append('The movie did not have any particularly novel moments.')
        elif novelty_rating >= 4 and novelty_rating < 5:
            review_sentences.append('The movie was pretty cliche.')
        elif novelty_rating >= 0 and novelty_rating < 4:
            review_sentences.append('The movie was so cliche and predictible it hurt while watching.')

        if script_rating >= 9:
            review_sentences.append('The writing was amazing!')
        elif script_rating >= 8 and script_rating < 9:
            review_sentences.append('The writing was great!')
        elif script_rating >= 6 and script_rating < 8:
            review_sentences.append('The writing was good.')
        elif script_rating >= 5 and script_rating < 6:
            review_sentences.append('The writing was okay.')
        elif script_rating >= 4 and script_rating < 5:
            review_sentences.append('The writing was pretty bad.')
        elif script_rating >= 0 and script_rating < 4:
            review_sentences.append('The writing was absolutely horrible.')

        if script_rating < 5:
            if footage_rating >= 7:
                review_sentences.append('But at least the camerawork made up for it.')

        print(f"INSERT INTO REVIEW(EXPERIENCE_RATING, FOOTAGE_RATING, NOVELTY_RATING, OVERALL_RATING, SCRIPT_RATING, DATE_OF_CREATION, DATE_OF_LAST_CHANGE, ID, MOVIE_ID, USER_ID, REVIEW_TEXT) VALUES ({experience_rating}, {footage_rating}, {novelty_rating}, {overall_rating}, {script_rating}, '{review['DATE_OF_CREATION'].isoformat()}', '{review['DATE_OF_LAST_CHANGE'].isoformat()}', '{review['ID']}', '{review['MOVIE_ID']}', '{review['USER_ID']}', '{' '.join(review_sentences)}');")

# movie_list = {
#     '6f4d5db3-33fd-4292-a5b7-bb6a7e25b142',
# }

user_list = [
    'b7cd5862-ad1f-5258-abc0-b0bc72ede032',
    '355eca9d-6b67-5dea-9c93-72d7707938db',
    'bcb4b218-0839-5186-afb8-a36bff49bac5',
    'b6040083-2807-51fa-9c7f-856bbe47b4b9',
    'fda4a2e0-4f20-5b63-a283-1a05cfe11871',
    'cd0bfa92-57ec-5884-8cd2-575ab09ad4f2',
    '823acb1b-949f-5d3e-a526-0d6aace2924f',
    'bdf5b503-66e3-553c-bf41-24251c3b6399',
    '7091613a-7a3c-57fa-8ffc-b5f698072a5c',
    'c40103c0-cef7-5d4f-9f2f-77db45f7c829',
    '2ce75e25-ae1d-5d3e-9181-662e197d02ac',
    '90145afa-8016-558e-90fa-d3c5b467bceb',
    'a159ac3a-7a8f-5f91-8759-490940aeaaeb',
    'bfeabe0d-fb01-559f-8c37-ea15155ca7bd',
    '8423e8ce-4566-5f67-987f-b091fd7b5ea2',
    'de89f304-9117-5952-a1f7-9db8ea406e71',
    'c6f4e095-c9a9-5049-bbbc-ed815e4f69ae',
    '58d67b4f-742c-5832-8de0-2100ee6e1e60',
    'ab9abb74-36f5-592c-a6f0-0834a6b58b38',
    '80504b3b-9e9c-5e2f-8387-8932008fcf0b',
    '2c352364-489b-5881-9ac8-a26081555b09',
    '748eb5af-f2ff-5285-a54f-566b154a0a2c',
    '610b89e8-a05d-54a4-8b3b-99d5aa160eaa',
    'a86e0d94-2984-5205-91f6-e66dcb0832af',
    'b68d492b-f6a3-5bcb-9665-7b1d76f09308',
    'b46caa01-a857-54d3-894f-a7cfa82394d4',
    '11cadcc3-9477-5929-a83f-0bd07f58b262',
    '9f7c7fe3-f45f-5676-9504-46a9b4d9ac3a',
    'c8cd4507-b539-5fd9-b765-c2aba8caee42',
    '58dbc892-9607-566c-8d91-86012f34f726',
    'f1bbb6b2-94d4-541d-b9da-928b8f033692',
]

if __name__ == '__main__':
    main()
