package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MovieReviewRestControllerTest {
    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private MovieReviewRestController movieReviewRestController;

    @Test
    void listReviewsByMovieId_movieFound_returnsPageOfReviewWithData() {
        // Arrange
        Page<ReviewDto> reviews = new PageImpl<>(List.of(TestDataFactory.reviewDto1, TestDataFactory.reviewDto2));
        Mockito.when(reviewFacade.listByMovieId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenReturn(reviews);

        // Act
        Page<ReviewDto> foundDtos = movieReviewRestController.listReviewsByMovieId(UUID.randomUUID(), PageRequest.of(0, 20)).getBody();

        // Assert
        assertThat(foundDtos).isNotNull();
        assertThat(foundDtos).isEqualTo(reviews);
    }

    @Test
    void listReviewsByMovieId_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewFacade.listByMovieId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> movieReviewRestController.listReviewsByMovieId(UUID.randomUUID(), PageRequest.of(0, 20)));
    }
}
