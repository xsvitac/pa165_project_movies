package com.example.pa165_project_movies.review.data.repository;

import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.data.model.Review;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ReviewRepositoryTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private final List<Review> allReviewEntitiesWithCorrectTime = new ArrayList<>();

    private final Map<UUID, UUID> testEntityToRealEntityUuidMapping = HashMap.newHashMap(TestDataFactory.allReviewEntities.size());

    @BeforeEach
    void initData() {
        for (Review tmpReview : TestDataFactory.allReviewEntities) {
            Review review = new Review();
            review.setUserId(tmpReview.getUserId());
            review.setMovieId(tmpReview.getMovieId());
            review.setNoveltyRating(tmpReview.getNoveltyRating());
            review.setFootageRating(tmpReview.getFootageRating());
            review.setScriptRating(tmpReview.getScriptRating());
            review.setExperienceRating(tmpReview.getExperienceRating());
            review.setOverallRating(tmpReview.getOverallRating());
            review.setReviewText(tmpReview.getReviewText());
            review = testEntityManager.persist(review);
            allReviewEntitiesWithCorrectTime.add(review);
            testEntityToRealEntityUuidMapping.put(tmpReview.getId(), review.getId());
        }
    }

    @Test
    void getReviewsOfSimilarMovies_allCriteriaUsed_returnsPageOfSimilarMovies() throws Exception {
        // Act
        Page<UUID> similarMovies = reviewRepository.getReviewsOfSimilarMovies(
                TestDataFactory.reviewCriteriaDto1.getNoveltyRating(),
                TestDataFactory.reviewCriteriaDto1.getFootageRating(),
                TestDataFactory.reviewCriteriaDto1.getScriptRating(),
                TestDataFactory.reviewCriteriaDto1.getExperienceRating(),
                TestDataFactory.reviewCriteriaDto1.getOverallRating(),
                PageRequest.of(0, 20));

        // Assert
        assertThat(similarMovies.isEmpty()).isFalse();
        assertThat(similarMovies).containsExactlyInAnyOrder(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
    }

    @Test
    void getReviewsOfSimilarMovies_onlyOverallUsed_returnsPageOfSimilarMovies() throws Exception {
        // Act
        Page<UUID> similarMovies = reviewRepository.getReviewsOfSimilarMovies(
                null,
                null,
                null,
                null,
                TestDataFactory.reviewCriteriaDto1.getOverallRating(),
                PageRequest.of(0, 20));

        // Assert
        assertThat(similarMovies.isEmpty()).isFalse();
        assertThat(similarMovies).containsExactlyInAnyOrder(
                UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"),
                UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
    }

    @Test
    void getReviewsOfSimilarMovies_noSimilarMoviesFound_returnsEmptyPage() throws Exception {
        // Act
        Page<UUID> similarMovies = reviewRepository.getReviewsOfSimilarMovies(
                null,
                null,
                null,
                null,
                10f,
                PageRequest.of(0, 20));

        // Assert
        assertThat(similarMovies.isEmpty()).isTrue();
    }

    @Test
    void getMovieAverageRating_movieFound_returnsReviewRatingDto() throws Exception {
        // Act
        Optional<ReviewRatingDto> dto = reviewRepository.getMovieAverageRating(TestDataFactory.reviewEntity1.getMovieId());

        // Assert
        assertThat(dto).isNotEmpty();
        assertThat(dto.get().getExperienceRating()).isCloseTo(6.2, Offset.offset(0.1));
        assertThat(dto.get().getScriptRating()).isCloseTo(7.1, Offset.offset(0.1));
        assertThat(dto.get().getFootageRating()).isCloseTo(6.3, Offset.offset(0.1));
        assertThat(dto.get().getNoveltyRating()).isCloseTo(6.5, Offset.offset(0.1));
        assertThat(dto.get().getOverallRating()).isCloseTo(7.0, Offset.offset(0.1));
    }

    @Test
    void getMovieAverageRating_movieNotFound_throwsResourceNotFoundException() throws Exception {
        // Act
        Optional<ReviewRatingDto> dto = reviewRepository.getMovieAverageRating(UUID.randomUUID());

        // Assert
        assertThat(dto).isEmpty();
    }

    @Test
    void listReviewsByUserId_reviewsExist_returnsPageOfReviews() throws Exception {
        // Arrange
        List<Review> expectedEntities = new ArrayList<>();
        Set<UUID> expectedEntityUuids = Set.of(
                    TestDataFactory.reviewEntity1.getId(),
                    TestDataFactory.reviewEntity3.getId(),
                    TestDataFactory.reviewEntity5.getId())
                .stream().map(testEntityToRealEntityUuidMapping::get).collect(Collectors.toSet());

        for (Review review : this.allReviewEntitiesWithCorrectTime) {
            if (expectedEntityUuids.contains(review.getId())) {
                expectedEntities.add(review);
            }
        }

        // Act
        Page<Review> reviews = reviewRepository.listByUserId(TestDataFactory.reviewEntity1.getUserId(), PageRequest.of(0, 20));

        // Assert
        assertThat(reviews.isEmpty()).isFalse();
        assertThat(reviews.getContent()).containsExactlyInAnyOrderElementsOf(expectedEntities);
    }

    @Test
    void listReviewsByUserId_noUserReviewsExist_returnsEmptyPage() throws Exception {
        // Act
        Page<Review> reviews = reviewRepository.listByUserId(UUID.randomUUID(), PageRequest.of(0, 20));

        // Assert
        assertThat(reviews.isEmpty()).isTrue();
    }

    @Test
    void listReviewsByMovieId_reviewsExist_returnsPageOfReviews() throws Exception {
        // Arrange
        List<Review> expectedEntities = new ArrayList<>();
        Set<UUID> expectedEntityUuids = Set.of(
                    TestDataFactory.reviewEntity1.getId(),
                    TestDataFactory.reviewEntity2.getId())
                .stream().map(testEntityToRealEntityUuidMapping::get).collect(Collectors.toSet());
        for (Review review : this.allReviewEntitiesWithCorrectTime) {
            if (expectedEntityUuids.contains(review.getId())) {
                expectedEntities.add(review);
            }
        }

        // Act
        Page<Review> reviews = reviewRepository.listByMovieId(TestDataFactory.reviewEntity1.getMovieId(), PageRequest.of(0, 20));

        // Assert
        assertThat(reviews.isEmpty()).isFalse();
        assertThat(reviews.getContent()).containsExactlyInAnyOrderElementsOf(expectedEntities);
    }

    @Test
    void listReviewsByMovieId_noUserReviewsExist_returnsEmptyPage() throws Exception {
        // Act
        Page<Review> reviews = reviewRepository.listByMovieId(UUID.randomUUID(), PageRequest.of(0, 20));

        // Assert
        assertThat(reviews.isEmpty()).isTrue();
    }
}
