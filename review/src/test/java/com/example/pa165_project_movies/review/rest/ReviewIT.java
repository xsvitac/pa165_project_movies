package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ApiErrorDto;
import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.data.model.Review;
import com.example.pa165_project_movies.review.mappers.ReviewMapper;
import com.example.pa165_project_movies.review.service.ReviewService;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ReviewIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReviewMapper reviewMapper;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ReviewService reviewService;

    @Value("${rest.path.reviews}")
    private URI reviewControllerPath;

    @Value("${rest.path.recommend}")
    private URI recommendControllerPath;

    @Value("${rest.path.movie-reviews}")
    private URI movieReviewControllerPath;

    @Value("${rest.path.user-reviews}")
    private URI userReviewControllerPath;

    @Value("${rest.path.movie-average}")
    private URI movieAverageControllerPath;

    @Value("${rest.path.dbclear}")
    private URI dbClearRestPath;

    @Transactional
    @Test
    void getAllReviews_reviewsExist_returnsPageOfReviews() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));
        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(TestDataFactory.allReviewEntities.size()))
                .andExpect(jsonPath("$.numberOfElements").value(TestDataFactory.allReviewEntities.size()))
                .andExpect(jsonPath("$.content.length()").value(TestDataFactory.allReviewEntities.size()));
    }

    @Transactional(readOnly = true)
    @Test
    void getAllReviews_noReviewsExist_returnsEmptyPage() throws Exception {
        // Arrange
        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.content.length()").value(0));
    }

    @Transactional
    @Test
    void getReviewById_reviewExists_returnsReview() throws Exception {
        // Arrange
        Review review = reviewService.create(TestDataFactory.reviewEntity1);
        ReviewDto reviewDto = TestDataFactory.reviewDto1;
        reviewDto.setId(review.getId());
        reviewDto.setDateOfCreation(review.getDateOfCreation());
        reviewDto.setDateOfLastChange(review.getDateOfLastChange());

        URI requestUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .path("/{id}")
                .buildAndExpand(review.getId().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ReviewDto response = objectMapper.readValue(responseJson, ReviewDto.class);

        // Assert
        assertThat(response.getId()).isEqualTo(reviewDto.getId());
        assertThat(response.getMovieId()).isEqualTo(reviewDto.getMovieId());
        assertThat(response.getUserId()).isEqualTo(reviewDto.getUserId());
        assertThat(response.getNoveltyRating()).isEqualTo(reviewDto.getNoveltyRating());
        assertThat(response.getFootageRating()).isEqualTo(reviewDto.getFootageRating());
        assertThat(response.getScriptRating()).isEqualTo(reviewDto.getScriptRating());
        assertThat(response.getExperienceRating()).isEqualTo(reviewDto.getExperienceRating());
        assertThat(response.getOverallRating()).isEqualTo(reviewDto.getOverallRating());
        assertThat(response.getReviewText()).isEqualTo(reviewDto.getReviewText());
        assertThat(response.getDateOfCreation()).isCloseTo(reviewDto.getDateOfCreation(), within(1000, ChronoUnit.NANOS));
        assertThat(response.getDateOfLastChange()).isCloseTo(reviewDto.getDateOfLastChange(), within(1000, ChronoUnit.NANOS));
    }

    @Transactional(readOnly = true)
    @Test
    void getReviewById_reviewDoesNotExist_returnsReview() throws Exception {
        // Arrange
        URI requestUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getPath()).isEqualTo(requestUri.toString());
    }

    @Transactional
    @Test
    void getMovieAverageRating_movieFound_returnsReviewRatingDto() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));
        URI requestUri = UriComponentsBuilder.fromUri(movieAverageControllerPath)
                .path("/{id}")
                .buildAndExpand(TestDataFactory.reviewRatingDto1.getMovieId().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ReviewRatingDto response = objectMapper.readValue(responseJson, ReviewRatingDto.class);

        // Assert
        assertThat(response).isNotNull();
        assertThat(response.getExperienceRating()).isCloseTo(6.2, Offset.offset(0.1));
        assertThat(response.getScriptRating()).isCloseTo(7.1, Offset.offset(0.1));
        assertThat(response.getFootageRating()).isCloseTo(6.3, Offset.offset(0.1));
        assertThat(response.getNoveltyRating()).isCloseTo(6.5, Offset.offset(0.1));
        assertThat(response.getOverallRating()).isCloseTo(7.0, Offset.offset(0.1));
    }

    @Transactional
    @Test
    void getMovieAverageRating_movieNotFound_returnsApiError() throws Exception {
        // Arrange
        URI requestUri = UriComponentsBuilder.fromUri(movieAverageControllerPath)
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getPath()).isEqualTo(requestUri.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createReview_overallRatingIsNull_returnsApiError() throws Exception {
        // Arrange

        ReviewCreateDto reviewCreateDto = new ReviewCreateDto();
        reviewCreateDto.setMovieId(TestDataFactory.reviewDto1.getMovieId());
        reviewCreateDto.setUserId(TestDataFactory.reviewDto1.getUserId());
        reviewCreateDto.setReviewText(TestDataFactory.reviewDto1.getReviewText());
        reviewCreateDto.setOverallRating(null);
        reviewCreateDto.setFootageRating(TestDataFactory.reviewCriteriaDto1.getFootageRating());
        reviewCreateDto.setExperienceRating(TestDataFactory.reviewCriteriaDto1.getExperienceRating());
        reviewCreateDto.setScriptRating(TestDataFactory.reviewCriteriaDto1.getScriptRating());
        reviewCreateDto.setNoveltyRating(TestDataFactory.reviewCriteriaDto1.getNoveltyRating());

        String reviewCreateDtoJson = objectMapper.writeValueAsString(reviewCreateDto);

        // Act
        String responseJson = mockMvc.perform(post(reviewControllerPath)
                        .with(TestDataFactory.mockAdminUser())
                        .content(reviewCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ApiErrorDto response = objectMapper.readValue(responseJson, ApiErrorDto.class);

        // Assert
        assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getPath()).isEqualTo(reviewControllerPath.toString());
    }

    @Transactional(readOnly = true)
    @Test
    void createReview_validInput_returnsReviewDto() throws Exception {
        // Arrange
        String reviewCreateDtoJson = objectMapper.writeValueAsString(TestDataFactory.reviewDto1);

        // Act
        String responseJson = mockMvc.perform(post(reviewControllerPath)
                        .with(TestDataFactory.mockAdminUser())
                        .content(reviewCreateDtoJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ReviewDto response = objectMapper.readValue(responseJson, ReviewDto.class);

        // Assert
        assertThat(response.getUserId()).isEqualTo(TestDataFactory.reviewDto1.getUserId());
        assertThat(response.getMovieId()).isEqualTo(TestDataFactory.reviewDto1.getMovieId());
        assertThat(response.getNoveltyRating()).isEqualTo(TestDataFactory.reviewDto1.getNoveltyRating());
        assertThat(response.getFootageRating()).isEqualTo(TestDataFactory.reviewDto1.getFootageRating());
        assertThat(response.getScriptRating()).isEqualTo(TestDataFactory.reviewDto1.getScriptRating());
        assertThat(response.getExperienceRating()).isEqualTo(TestDataFactory.reviewDto1.getExperienceRating());
        assertThat(response.getOverallRating()).isEqualTo(TestDataFactory.reviewDto1.getOverallRating());
        assertThat(response.getReviewText()).isEqualTo(TestDataFactory.reviewDto1.getReviewText());
    }

    @Transactional
    @Test
    void getReviewsOfSimilarMovies_allCriteriaUsed_returnsPageOfSimilarMovies() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        ReviewCriteriaDto reviewCriteriaDto = new ReviewCriteriaDto();
        reviewCriteriaDto.setOverallRating(TestDataFactory.reviewCriteriaDto1.getOverallRating());
        reviewCriteriaDto.setFootageRating(TestDataFactory.reviewCriteriaDto1.getFootageRating());
        reviewCriteriaDto.setExperienceRating(TestDataFactory.reviewCriteriaDto1.getExperienceRating());
        reviewCriteriaDto.setScriptRating(TestDataFactory.reviewCriteriaDto1.getScriptRating());
        reviewCriteriaDto.setNoveltyRating(TestDataFactory.reviewCriteriaDto1.getNoveltyRating());
        String reviewCriteriaJson = objectMapper.writeValueAsString(reviewCriteriaDto);

        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(recommendControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        String responseJson = mockMvc.perform(get(pageUri)
                        .content(reviewCriteriaJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        JsonNode responseRoot = objectMapper.readTree(responseJson);
        List<UUID> similarMovies = Arrays.asList(objectMapper.treeToValue(responseRoot.get("content"), UUID[].class));

        // Assert
        assertThat(similarMovies).containsExactlyInAnyOrder(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
    }

    @Transactional
    @Test
    void getReviewsOfSimilarMovies_onlyOverallUsed_returnsPageOfSimilarMovies() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        ReviewCriteriaDto reviewCriteriaDto = new ReviewCriteriaDto();
        reviewCriteriaDto.setOverallRating(TestDataFactory.reviewCriteriaDto1.getOverallRating());
        String reviewCriteriaJson = objectMapper.writeValueAsString(reviewCriteriaDto);

        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(recommendControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        String responseJson = mockMvc.perform(get(pageUri)
                        .content(reviewCriteriaJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        JsonNode responseRoot = objectMapper.readTree(responseJson);
        List<UUID> similarMovies = Arrays.asList(objectMapper.treeToValue(responseRoot.get("content"), UUID[].class));

        // Assert
        assertThat(similarMovies).containsExactlyInAnyOrder(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"), UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
    }

    @Transactional
    @Test
    void getReviewsOfSimilarMovies_noSimilarMoviesFound_returnsEmptyPage() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        ReviewCriteriaDto reviewCriteriaDto = new ReviewCriteriaDto();
        reviewCriteriaDto.setOverallRating(10f);
        String reviewCriteriaJson = objectMapper.writeValueAsString(reviewCriteriaDto);

        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(recommendControllerPath)
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .build().toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .content(reviewCriteriaJson)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.content.length()").value(0));
    }

    @Transactional
    @Test
    void listReviewsByUserId_reviewsExist_returnsPageOfReviews() throws Exception {
        // Arrange
        List<ReviewDto> expectedDtos = new ArrayList<>();
        Set<UUID> dataFactoryUuids = Set.of(TestDataFactory.reviewEntity1.getId(), TestDataFactory.reviewEntity3.getId(), TestDataFactory.reviewEntity5.getId());
        for (Review review : TestDataFactory.allReviewEntities) {
            Review tmp = reviewService.create(review);
            if (dataFactoryUuids.contains(review.getId())) {
                expectedDtos.add(reviewMapper.mapToDto(tmp));
            }
        }

        URI requestUri = UriComponentsBuilder.fromUri(userReviewControllerPath)
                .path("/{id}")
                .buildAndExpand(TestDataFactory.reviewDto1.getUserId().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        JsonNode responseRoot = objectMapper.readTree(responseJson);
        List<ReviewDto> responseDtos = Arrays.asList(objectMapper.treeToValue(responseRoot.get("content"), ReviewDto[].class));

        // Assert
        // ignore dates because of different precision before and after serialization
        assertThat(responseDtos).usingRecursiveFieldByFieldElementComparatorIgnoringFields("dateOfCreation", "dateOfLastChange").containsExactlyInAnyOrderElementsOf(expectedDtos);
        // compare dates with precision within 1000 ns
        for (ReviewDto responseDto : responseDtos) {
            ReviewDto expectedDto = expectedDtos.stream().filter(r -> r.getId().equals(responseDto.getId())).findFirst().get();
            assertThat(responseDto.getDateOfCreation()).isCloseTo(expectedDto.getDateOfCreation(), within(1000, ChronoUnit.NANOS));
            assertThat(responseDto.getDateOfLastChange()).isCloseTo(expectedDto.getDateOfLastChange(), within(1000, ChronoUnit.NANOS));
        }
    }

    @Transactional
    @Test
    void listReviewsByUserId_noUserReviewsExist_returnsEmptyPage() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(userReviewControllerPath)
                .path("/{id}")
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.content.length()").value(0));
    }

    @Transactional
    @Test
    void listReviewsByMovieId_reviewsExist_returnsPageOfReviews() throws Exception {
        // Arrange
        List<ReviewDto> expectedDtos = new ArrayList<>();
        Set<UUID> dataFactoryUuids = Set.of(TestDataFactory.reviewDto1.getId(), TestDataFactory.reviewDto2.getId());
        for (Review review : TestDataFactory.allReviewEntities) {
            Review tmp = reviewService.create(review);
            if (dataFactoryUuids.contains(review.getId())) {
                expectedDtos.add(reviewMapper.mapToDto(tmp));
            }
        }

        URI requestUri = UriComponentsBuilder.fromUri(movieReviewControllerPath)
                .path("/{id}")
                .buildAndExpand(TestDataFactory.reviewDto1.getMovieId().toString()).toUri();

        // Act
        String responseJson = mockMvc.perform(get(requestUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        JsonNode responseRoot = objectMapper.readTree(responseJson);
        List<ReviewDto> responseDtos = Arrays.asList(objectMapper.treeToValue(responseRoot.get("content"), ReviewDto[].class));

        // Assert
        // ignore dates because of different precision before and after serialization
        assertThat(responseDtos).usingRecursiveFieldByFieldElementComparatorIgnoringFields("dateOfCreation", "dateOfLastChange").containsExactlyInAnyOrderElementsOf(expectedDtos);
        // compare dates with precision within 1000 ns
        for (ReviewDto responseDto : responseDtos) {
            ReviewDto expectedDto = expectedDtos.stream().filter(r -> r.getId().equals(responseDto.getId())).findFirst().get();
            assertThat(responseDto.getDateOfCreation()).isCloseTo(expectedDto.getDateOfCreation(), within(1000, ChronoUnit.NANOS));
            assertThat(responseDto.getDateOfLastChange()).isCloseTo(expectedDto.getDateOfLastChange(), within(1000, ChronoUnit.NANOS));
        }
    }

    @Transactional
    @Test
    void listReviewsByMovieId_noUserReviewsExist_returnsEmptyPage() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        long pageOffset = 0;
        long pageSize = 20;
        URI pageUri = UriComponentsBuilder.fromUri(movieReviewControllerPath)
                .path("/{id}")
                .queryParam("page", pageOffset)
                .queryParam("size", pageSize)
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        mockMvc.perform(get(pageUri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.paged").value(true))
                .andExpect(jsonPath("$.pageable.pageNumber").value(pageOffset))
                .andExpect(jsonPath("$.pageable.pageSize").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.content.length()").value(0));
    }

    @Transactional
    @Test
    void deleteReview_reviewExists_noReturn() throws Exception {
        // Arrange
        Review review = reviewService.create(TestDataFactory.reviewEntity1);

        URI requestUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .path("/{id}")
                .buildAndExpand(review.getId().toString()).toUri();

        // Act
        mockMvc.perform(delete(requestUri)
                        .with(TestDataFactory.mockAdminUser()))
                .andExpect(status().isNoContent());
    }

    @Transactional
    @Test
    void deleteReview_reviewDoesNotExist_noReturn() throws Exception {
        URI requestUri = UriComponentsBuilder.fromUri(reviewControllerPath)
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID().toString()).toUri();

        // Act
        mockMvc.perform(delete(requestUri)
                        .with(TestDataFactory.mockAdminUser()))
                .andExpect(status().isNotFound());
    }

    @Transactional
    @Test
    void deleteAllReviews_reviewsExist_noReturn() throws Exception {
        // Arrange
        TestDataFactory.allReviewEntities.forEach(review -> reviewService.create(review));

        // Act
        mockMvc.perform(delete(dbClearRestPath)
                        .with(TestDataFactory.mockAdminUser()))
                .andExpect(status().isNoContent());
    }
}
