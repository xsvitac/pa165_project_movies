package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.facade.ReviewFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DBClearRestControllerTest {
    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private DBClearRestController dbClearRestController;

    @Test
    void deleteReview_reviewFound_returnsReview() {
        // Arrange
        Mockito.doNothing().when(reviewFacade).deleteAll();

        // Act
        dbClearRestController.deleteAllReviews();

        // Assert
        Mockito.verify(reviewFacade, Mockito.times(1)).deleteAll();
    }
}
