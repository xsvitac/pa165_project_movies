package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MovieAverageRestControllerTest {
    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private MovieAverageRestController movieAverageRestController;

    @Test
    void getMovieAverageRating_movieFound_returnsReviewRatingDto() {
        // Arrange
        Mockito.when(reviewFacade.getMovieAverageRating(Mockito.any(UUID.class))).thenReturn(TestDataFactory.reviewRatingDto1);

        // Act
        ReviewRatingDto dto = movieAverageRestController.getMovieAverageRating(UUID.randomUUID()).getBody();

        // Assert
        assertThat(dto).isNotNull();
        assertThat(dto.getExperienceRating()).isCloseTo(6.2, Offset.offset(0.1));
        assertThat(dto.getScriptRating()).isCloseTo(7.1, Offset.offset(0.1));
        assertThat(dto.getFootageRating()).isCloseTo(6.3, Offset.offset(0.1));
        assertThat(dto.getNoveltyRating()).isCloseTo(6.5, Offset.offset(0.1));
        assertThat(dto.getOverallRating()).isCloseTo(7.0, Offset.offset(0.1));
    }

    @Test
    void getMovieAverageRating_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewFacade.getMovieAverageRating(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> movieAverageRestController.getMovieAverageRating(UUID.randomUUID()));
    }
}
