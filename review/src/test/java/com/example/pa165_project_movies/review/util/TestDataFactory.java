package com.example.pa165_project_movies.review.util;

import com.example.pa165_project_movies.common.config.Oauth2Scope;
import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.review.data.model.Review;
import org.assertj.core.data.Offset;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Component
public class TestDataFactory {
    public static final ReviewDto reviewDto1 = reviewDto1();
    public static final ReviewCreateDto reviewCreateDto1 = reviewCreateDto1();
    public static final ReviewCriteriaDto reviewCriteriaDto1 = reviewCriteriaDto1();
    public static final Review reviewEntity1 = reviewEntity1();
    public static final ReviewRatingDto reviewRatingDto1 = reviewRatingDto1();
    public static final ReviewDto reviewDto2 = reviewDto2();
    public static final Review reviewEntity2 = reviewEntity2();
    public static final ReviewDto reviewDto3 = reviewDto3();
    public static final Review reviewEntity3 = reviewEntity3();
    public static final ReviewDto reviewDto4 = reviewDto4();
    public static final Review reviewEntity4 = reviewEntity4();
    public static final ReviewDto reviewDto5 = reviewDto5();
    public static final Review reviewEntity5 = reviewEntity5();
    public static final ReviewDto reviewDto6 = reviewDto6();
    public static final Review reviewEntity6 = reviewEntity6();

    public static final List<Review> similarReviewEntities = List.of(reviewEntity1, reviewEntity2, reviewEntity3, reviewEntity4);
    public static final List<ReviewDto> similarReviewDtos = List.of(reviewDto1, reviewDto2, reviewDto3, reviewDto4);
    public static final List<UUID> similarMovieUuids = List.of(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"), UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
    public static final List<Review> allReviewEntities = List.of(reviewEntity1, reviewEntity2, reviewEntity3, reviewEntity4, reviewEntity5, reviewEntity6);
    public static final List<ReviewDto> allReviewDtos = List.of(reviewDto1, reviewDto2, reviewDto3, reviewDto4, reviewDto5, reviewDto6);

    public static RequestPostProcessor mockAdminUser() {
        return SecurityMockMvcRequestPostProcessors.user("Admin").authorities(new SimpleGrantedAuthority(Oauth2Scope.ADMIN));
    }

    private static ReviewDto reviewDto1() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("52657669-6577-5265-706f-7369746f7279"));
        reviewDto.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        reviewDto.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        reviewDto.setNoveltyRating(6.4f);
        reviewDto.setFootageRating(6.1f);
        reviewDto.setScriptRating(7.0f);
        reviewDto.setExperienceRating(5.5f);
        reviewDto.setOverallRating(6.8f);
        reviewDto.setReviewText("It was good!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static ReviewCriteriaDto reviewCriteriaDto1() {
        ReviewCriteriaDto reviewCriteria = new ReviewCriteriaDto();
        reviewCriteria.setNoveltyRating(6.4f);
        reviewCriteria.setFootageRating(6.1f);
        reviewCriteria.setScriptRating(7.0f);
        reviewCriteria.setExperienceRating(5.5f);
        reviewCriteria.setOverallRating(6.8f);
        return reviewCriteria;
    }

    private static ReviewCreateDto reviewCreateDto1() {
        ReviewCreateDto reviewCreateDto = new ReviewCreateDto();
        reviewCreateDto.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        reviewCreateDto.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        reviewCreateDto.setNoveltyRating(6.4f);
        reviewCreateDto.setFootageRating(6.1f);
        reviewCreateDto.setScriptRating(7.0f);
        reviewCreateDto.setExperienceRating(5.5f);
        reviewCreateDto.setOverallRating(6.8f);
        reviewCreateDto.setReviewText("It was good!");
        return reviewCreateDto;
    }

    private static Review reviewEntity1() {
        Review review = new Review();
        review.setId(UUID.fromString("52657669-6577-5265-706f-7369746f7279"));
        review.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        review.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        review.setNoveltyRating(6.4f);
        review.setFootageRating(6.1f);
        review.setScriptRating(7.0f);
        review.setExperienceRating(5.5f);
        review.setOverallRating(6.8f);
        review.setReviewText("It was good!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }

    private static ReviewRatingDto reviewRatingDto1() {
        ReviewRatingDto reviewRatingDto = new ReviewRatingDto();
        reviewRatingDto.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        reviewRatingDto.setExperienceRating(6.2);
        reviewRatingDto.setScriptRating(7.1);
        reviewRatingDto.setFootageRating(6.3);
        reviewRatingDto.setNoveltyRating(6.5);
        reviewRatingDto.setOverallRating(7.0);
        return reviewRatingDto;

    }
    private static ReviewDto reviewDto2() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("e5d6f1b1-4a00-4bde-8fc9-66a1b59f3a1a"));
        reviewDto.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        reviewDto.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        reviewDto.setNoveltyRating(6.7f);
        reviewDto.setFootageRating(6.6f);
        reviewDto.setScriptRating(7.2f);
        reviewDto.setExperienceRating(6.8f);
        reviewDto.setOverallRating(7.1f);
        reviewDto.setReviewText("It was good!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static Review reviewEntity2() {
        Review review = new Review();
        review.setId(UUID.fromString("e5d6f1b1-4a00-4bde-8fc9-66a1b59f3a1a"));
        review.setMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"));
        review.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        review.setNoveltyRating(6.7f);
        review.setFootageRating(6.6f);
        review.setScriptRating(7.2f);
        review.setExperienceRating(6.8f);
        review.setOverallRating(7.1f);
        review.setReviewText("It was good!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }

    private static ReviewDto reviewDto3() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("c033990c-2453-442a-8e9a-0897197f3035"));
        reviewDto.setMovieId(UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
        reviewDto.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        reviewDto.setNoveltyRating(7.1f);
        reviewDto.setFootageRating(7.2f);
        reviewDto.setScriptRating(7.0f);
        reviewDto.setExperienceRating(6.1f);
        reviewDto.setOverallRating(6.9f);
        reviewDto.setReviewText("It was good!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static Review reviewEntity3() {
        Review review = new Review();
        review.setId(UUID.fromString("c033990c-2453-442a-8e9a-0897197f3035"));
        review.setMovieId(UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
        review.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        review.setNoveltyRating(7.1f);
        review.setFootageRating(7.2f);
        review.setScriptRating(7.0f);
        review.setExperienceRating(6.1f);
        review.setOverallRating(6.9f);
        review.setReviewText("It was good!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }

    private static ReviewDto reviewDto4() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("c1af2f15-d445-4232-841f-1531bad8c66c"));
        reviewDto.setMovieId(UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
        reviewDto.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        reviewDto.setNoveltyRating(6.5f);
        reviewDto.setFootageRating(7.1f);
        reviewDto.setScriptRating(6.8f);
        reviewDto.setExperienceRating(5.8f);
        reviewDto.setOverallRating(6.7f);
        reviewDto.setReviewText("It was good!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static Review reviewEntity4() {
        Review review = new Review();
        review.setId(UUID.fromString("c1af2f15-d445-4232-841f-1531bad8c66c"));
        review.setMovieId(UUID.fromString("cfa93683-3594-421a-92fd-8aeda0377bbf"));
        review.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        review.setNoveltyRating(6.5f);
        review.setFootageRating(7.1f);
        review.setScriptRating(6.8f);
        review.setExperienceRating(5.8f);
        review.setOverallRating(6.7f);
        review.setReviewText("It was good!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }

    private static ReviewDto reviewDto5() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("d4a91e07-8e8d-4181-b275-246e2e7ee21d"));
        reviewDto.setMovieId(UUID.fromString("138b7316-6b12-4f82-902f-53e00ac0c589"));
        reviewDto.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        reviewDto.setNoveltyRating(8.7f);
        reviewDto.setFootageRating(8.3f);
        reviewDto.setScriptRating(7.9f);
        reviewDto.setExperienceRating(8.5f);
        reviewDto.setOverallRating(8.1f);
        reviewDto.setReviewText("It was great!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static Review reviewEntity5() {
        Review review = new Review();
        review.setId(UUID.fromString("d4a91e07-8e8d-4181-b275-246e2e7ee21d"));
        review.setMovieId(UUID.fromString("138b7316-6b12-4f82-902f-53e00ac0c589"));
        review.setUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"));
        review.setNoveltyRating(8.7f);
        review.setFootageRating(8.3f);
        review.setScriptRating(7.9f);
        review.setExperienceRating(8.5f);
        review.setOverallRating(8.1f);
        review.setReviewText("It was great!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }

    private static ReviewDto reviewDto6() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(UUID.fromString("4af8e05e-59e0-48e6-8629-cffa9b6a9dd2"));
        reviewDto.setMovieId(UUID.fromString("138b7316-6b12-4f82-902f-53e00ac0c589"));
        reviewDto.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        reviewDto.setNoveltyRating(9.0f);
        reviewDto.setFootageRating(8.2f);
        reviewDto.setScriptRating(8.2f);
        reviewDto.setExperienceRating(8.4f);
        reviewDto.setOverallRating(8.6f);
        reviewDto.setReviewText("It was amazing!");
        reviewDto.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        reviewDto.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return reviewDto;
    }

    private static Review reviewEntity6() {
        Review review = new Review();
        review.setId(UUID.fromString("4af8e05e-59e0-48e6-8629-cffa9b6a9dd2"));
        review.setMovieId(UUID.fromString("138b7316-6b12-4f82-902f-53e00ac0c589"));
        review.setUserId(UUID.fromString("6e13c00a-5218-4a6a-883e-68fc10534fc9"));
        review.setNoveltyRating(9.0f);
        review.setFootageRating(8.2f);
        review.setScriptRating(8.2f);
        review.setExperienceRating(8.4f);
        review.setOverallRating(8.6f);
        review.setReviewText("It was amazing!");
        review.setDateOfCreation(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        review.setDateOfLastChange(OffsetDateTime.of(2024, 3, 22, 12, 44, 29, 230243, ZoneOffset.UTC));
        return review;
    }
}
