package com.example.pa165_project_movies.review.service;

import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.review.data.model.Review;
import com.example.pa165_project_movies.review.data.repository.ReviewRepository;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTest {
    @Mock
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewService reviewService;

    @Test
    void findById_reviewFound_returnsReview() {
        // Arrange
        Mockito.when(reviewRepository.findById(UUID.fromString("52657669-6577-5265-706f-7369746f7279"))).thenReturn(Optional.of(TestDataFactory.reviewEntity1));

        // Act
        Review foundEntity = reviewService.findById(UUID.fromString("52657669-6577-5265-706f-7369746f7279"));

        // Assert
        assertThat(foundEntity).isEqualTo(TestDataFactory.reviewEntity1);
    }

    @Test
    void findById_reviewNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewRepository.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewService.findById(UUID.randomUUID()));
    }

    @Test
    void findAll_reviewsExist_returnsPageOfReviews() {
        // Arrange
        Page<Review> reviews = Mockito.mock(Page.class);
        Mockito.when(reviewRepository.findAll(Mockito.any(Pageable.class))).thenReturn(reviews);

        // Act
        Page<Review> foundEntities = reviewService.findAll(PageRequest.of(0, 20));

        // Assert
        assertThat(foundEntities).isEqualTo(reviews).isNotNull();
    }

    @Test
    void findAll_reviewsExist_returnsPageOfReviewsWithData() {
        // Arrange
        Page<Review> reviews = new PageImpl<>(List.of(TestDataFactory.reviewEntity1));
        Mockito.when(reviewRepository.findAll(Mockito.any(Pageable.class))).thenReturn(reviews);

        // Act
        Page<Review> foundEntities = reviewService.findAll(PageRequest.of(0, 1));

        // Assert
        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));
    }

    @Test
    void listByUserId_userFound_returnsPageOfReviews() {
        // Arrange
        Mockito.when(reviewRepository.listByUserId(Mockito.eq(UUID.fromString("72616e64-6f6d-2075-7365-722075756964")),
                Mockito.any(Pageable.class))).thenReturn(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));

        // Act
        Page<Review> foundEntities = reviewService.listByUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"), PageRequest.of(0, 20));

        // Assert
        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));
    }

    @Test
    void listByUserId_userNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewRepository.listByUserId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewService.listByUserId(UUID.randomUUID(), PageRequest.of(0, 20)));
    }

    @Test
    void listByMovieId_movieFound_returnsPageOfReviews() {
        // Arrange
        Mockito.when(reviewRepository.listByMovieId(Mockito.eq(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964")), Mockito.any(Pageable.class))).thenReturn(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));

        // Act
        Page<Review> foundEntities = reviewService.listByMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"), PageRequest.of(0, 20));

        // Assert
        assertThat(foundEntities).isEqualTo(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));
    }

    @Test
    void listByMovieId_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewRepository.listByMovieId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewService.listByMovieId(UUID.randomUUID(), PageRequest.of(0, 20)));
    }

    @Test
    void getReviewsOfSimilarMovies_allRatingCategoriesPresent_returnsPageOfReviews() {
        // Arrange
        ReviewCriteriaDto review = TestDataFactory.reviewCriteriaDto1;
        Page<UUID> similarMoviesPage = new PageImpl<>(TestDataFactory.similarMovieUuids);
        Mockito.when(reviewRepository.getReviewsOfSimilarMovies(Mockito.eq(review.getNoveltyRating()), Mockito.eq(review.getFootageRating()),
                Mockito.eq(review.getScriptRating()), Mockito.eq(review.getExperienceRating()), Mockito.eq(review.getOverallRating()), Mockito.any(Pageable.class))).thenReturn(similarMoviesPage);

        // Act
        Page<UUID> foundMovies = reviewService.getReviewsOfSimilarMovies(review, PageRequest.of(0, 20));

        // Assert
        assertThat(foundMovies).isEqualTo(similarMoviesPage);
    }

    @Test
    void getMovieAverageRating_movieFound_returnsReviewRatingDto() {
        // Arrange
        Mockito.when(reviewRepository.getMovieAverageRating(Mockito.any(UUID.class))).thenReturn(Optional.of(TestDataFactory.reviewRatingDto1));

        // Act
        ReviewRatingDto dto = reviewService.getMovieAverageRating(UUID.randomUUID());

        // Assert
        assertThat(dto.getExperienceRating()).isCloseTo(6.2, Offset.offset(0.1));
        assertThat(dto.getScriptRating()).isCloseTo(7.1, Offset.offset(0.1));
        assertThat(dto.getFootageRating()).isCloseTo(6.3, Offset.offset(0.1));
        assertThat(dto.getNoveltyRating()).isCloseTo(6.5, Offset.offset(0.1));
        assertThat(dto.getOverallRating()).isCloseTo(7.0, Offset.offset(0.1));
    }

    @Test
    void getMovieAverageRating_movieNotFound_throwsResourceNotFoundException() throws Exception {
        // Arrange
        Mockito.when(reviewRepository.getMovieAverageRating(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewService.getMovieAverageRating(UUID.randomUUID()));
    }

    @Test
    void create_validInput_returnsReview() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        Mockito.when(reviewRepository.save(review)).thenReturn(review);

        // Act
        Review createdEntity = reviewService.create(review);

        // Assert
        assertThat(createdEntity).isEqualTo(review);
    }

    @Test
    void update_validInput_returnsReview() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        Mockito.when(reviewRepository.findById(review.getId())).thenReturn(Optional.of(review));
        Mockito.when(reviewRepository.save(review)).thenReturn(review);

        // Act
        Review updatedEntity = reviewService.update(review);

        // Assert
        assertThat(updatedEntity).isEqualTo(review);
        Mockito.verify(reviewRepository, Mockito.times(1)).findById(review.getId());
        Mockito.verify(reviewRepository, Mockito.times(1)).save(review);
    }

    @Test
    void delete_validInput_noReturn() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        Mockito.doNothing().when(reviewRepository).delete(review);

        // Act
        reviewService.delete(review);

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1)).delete(review);
    }

    @Test
    void deleteAll_noReturn() {
        // Arrange
        Mockito.doNothing().when(reviewRepository).deleteAll();

        // Act
        reviewService.deleteAll();

        // Assert
        Mockito.verify(reviewRepository, Mockito.times(1)).deleteAll();
    }
}
