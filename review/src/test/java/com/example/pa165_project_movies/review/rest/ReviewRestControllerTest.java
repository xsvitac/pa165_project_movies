package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ReviewRestControllerTest {
    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private ReviewRestController reviewRestController;

    @Test
    void getAllReviews_reviewsExist_returnsPageOfReviewWithData() {
        // Arrange
        Page<ReviewDto> reviews = new PageImpl<>(TestDataFactory.allReviewDtos);
        Mockito.when(reviewFacade.findAll(Mockito.any(Pageable.class))).thenReturn(reviews);

        // Act
        Page<ReviewDto> foundDtos = reviewRestController.getAllReviews(PageRequest.of(0, 20)).getBody();

        // Assert
        assertThat(foundDtos).isNotNull();
        assertThat(foundDtos).isEqualTo(reviews);
    }

    @Test
    void getReviewById_reviewFound_returnsReview() {
        // Arrange
        ReviewDto review = TestDataFactory.reviewDto1;
        Mockito.when(reviewFacade.findById(Mockito.any(UUID.class))).thenReturn(review);

        // Act
        ReviewDto foundDto = reviewRestController.getReviewById(UUID.randomUUID()).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(review);
    }

    @Test
    void createReview_validInput_returnsReview() {
        // Arrange
        ReviewDto review = TestDataFactory.reviewDto1;
        ReviewCreateDto reviewCreateDto = TestDataFactory.reviewCreateDto1;
        Mockito.when(reviewFacade.create(reviewCreateDto)).thenReturn(review);

        // Act
        ReviewDto foundDto = reviewRestController.createReview(reviewCreateDto).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(review);
    }

    @Test
    void updateReview_validInput_returnsReview() {
        // Arrange
        ReviewDto review = TestDataFactory.reviewDto1;
        Mockito.when(reviewFacade.update(review.getId(), review)).thenReturn(review);

        // Act
        ReviewDto foundDto = reviewRestController.updateReview(review.getId(), review).getBody();

        // Assert
        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualTo(review);
    }

    @Test
    void deleteReview_reviewFound_returnsReview() {
        // Arrange
        ReviewDto review = TestDataFactory.reviewDto1;
        UUID id = UUID.fromString("52657669-6577-5265-706f-7369746f7279");
        Mockito.doNothing().when(reviewFacade).delete(id);

        // Act
        reviewRestController.deleteReview(review.getId());

        // Assert
        Mockito.verify(reviewFacade, Mockito.times(1)).delete(id);
    }
}
