package com.example.pa165_project_movies.review.rest;

import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.facade.ReviewFacade;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class RecommendControllerTest {
    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private RecommendController recommendController;

    @Test
    void getReviewsOfSimilarMovies_validInput_returnsPageOfReviewWithData() {
        // Arrange
        ReviewCriteriaDto reviewCriteriaDto = TestDataFactory.reviewCriteriaDto1;
        Page<UUID> similarMovies = new PageImpl<>(TestDataFactory.similarMovieUuids);
        Mockito.when(reviewFacade.getReviewsOfSimilarMovies(Mockito.eq(reviewCriteriaDto), Mockito.any(Pageable.class))).thenReturn(similarMovies);

        // Act
        Page<UUID> foundMovies = recommendController.getReviewsOfSimilarMovies(reviewCriteriaDto, PageRequest.of(0, 20)).getBody();

        // Assert
        assertThat(foundMovies).isNotNull();
        assertThat(foundMovies).isEqualTo(similarMovies);
    }
}
