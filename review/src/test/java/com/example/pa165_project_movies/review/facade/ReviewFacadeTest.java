package com.example.pa165_project_movies.review.facade;

import com.example.pa165_project_movies.review.dto.ReviewCreateDto;
import com.example.pa165_project_movies.review.dto.ReviewCriteriaDto;
import com.example.pa165_project_movies.review.dto.ReviewDto;
import com.example.pa165_project_movies.review.dto.ReviewRatingDto;
import com.example.pa165_project_movies.common.exception.ResourceNotFoundException;
import com.example.pa165_project_movies.review.data.model.Review;
import com.example.pa165_project_movies.review.mappers.ReviewMapper;
import com.example.pa165_project_movies.review.service.ReviewService;
import com.example.pa165_project_movies.review.util.TestDataFactory;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ReviewFacadeTest {
    @Mock
    private ReviewService reviewService;

    @Mock
    private ReviewMapper reviewMapper;

    @InjectMocks
    private ReviewFacade reviewFacade;

    @Test
    void findById_reviewFound_returnsReview() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        ReviewDto reviewDto = TestDataFactory.reviewDto1;
        Mockito.when(reviewService.findById(UUID.fromString("52657669-6577-5265-706f-7369746f7279"))).thenReturn(review);
        Mockito.when(reviewMapper.mapToDto(review)).thenReturn(reviewDto);

        // Act
        ReviewDto foundDto = reviewFacade.findById(UUID.fromString("52657669-6577-5265-706f-7369746f7279"));

        // Assert
        assertThat(foundDto).isEqualTo(reviewDto);
    }

    @Test
    void findById_reviewNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewService.findById(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewFacade.findById(UUID.randomUUID()));
    }

    @Test
    void findAll_reviewsExist_returnsPageOfReviews() {
        // Arrange
        Page<Review> reviews = Mockito.mock(Page.class);
        Page<ReviewDto> reviewDtos = Mockito.mock(Page.class);
        Mockito.when(reviewService.findAll(Mockito.any(Pageable.class))).thenReturn(reviews);
        Mockito.when(reviewMapper.mapToPageDto(reviews)).thenReturn(reviewDtos);

        // Act
        Page<ReviewDto> foundDtos = reviewFacade.findAll(PageRequest.of(0, 20));

        // Assert
        assertThat(foundDtos).isEqualTo(reviewDtos).isNotNull();
    }

    @Test
    void findAll_reviewsExist_returnsPageOfReviewsWithData() {
        // Arrange
        Page<Review> reviews = new PageImpl<>(TestDataFactory.allReviewEntities);
        Page<ReviewDto> reviewDtos = new PageImpl<>(TestDataFactory.allReviewDtos);
        Mockito.when(reviewService.findAll(Mockito.any(Pageable.class))).thenReturn(reviews);
        Mockito.when(reviewMapper.mapToPageDto(reviews)).thenReturn(reviewDtos);

        // Act
        Page<ReviewDto> foundDtos = reviewFacade.findAll(PageRequest.of(0, TestDataFactory.allReviewEntities.size()));

        // Assert
        assertThat(foundDtos).isEqualTo(reviewDtos);
    }

    @Test
    void listByUserId_userFound_returnsPageOfReviews() {
        // Arrange
        Page<Review> reviews = new PageImpl<>(List.of(TestDataFactory.reviewEntity1));
        Page<ReviewDto> reviewDtos = new PageImpl<>(List.of(TestDataFactory.reviewDto1));
        Mockito.when(reviewService.listByUserId(Mockito.eq(UUID.fromString("72616e64-6f6d-2075-7365-722075756964")), Mockito.any(Pageable.class))).thenReturn(new PageImpl<>(List.of(TestDataFactory.reviewEntity1)));
        Mockito.when(reviewMapper.mapToPageDto(reviews)).thenReturn(reviewDtos);

        // Act
        Page<ReviewDto> foundDtos = reviewFacade.listByUserId(UUID.fromString("72616e64-6f6d-2075-7365-722075756964"), PageRequest.of(0, 20));

        // Assert
        assertThat(foundDtos).isEqualTo(reviewDtos);
    }

    @Test
    void listByUserId_userNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewService.listByUserId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewFacade.listByUserId(UUID.randomUUID(), PageRequest.of(0, 20)));
    }

    @Test
    void listByMovieId_movieFound_returnsPageOfReviews() {
        // Arrange
        Page<Review> reviews = new PageImpl<>(List.of(TestDataFactory.reviewEntity1, TestDataFactory.reviewEntity2));
        Page<ReviewDto> reviewDtos = new PageImpl<>(List.of(TestDataFactory.reviewDto1, TestDataFactory.reviewDto2));
        Mockito.when(reviewService.listByMovieId(Mockito.eq(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964")), Mockito.any(Pageable.class))).thenReturn(reviews);
        Mockito.when(reviewMapper.mapToPageDto(reviews)).thenReturn(reviewDtos);

        // Act
        Page<ReviewDto> foundDtos = reviewFacade.listByMovieId(UUID.fromString("736f6d65-2020-6d6f-7669-652075756964"), PageRequest.of(0, 20));

        // Assert
        assertThat(foundDtos).isEqualTo(reviewDtos);
    }

    @Test
    void listByMovieId_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(reviewService.listByMovieId(Mockito.any(UUID.class), Mockito.any(Pageable.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewFacade.listByMovieId(UUID.randomUUID(), PageRequest.of(0, 20)));
    }

    @Test
    void getReviewsOfSimilarMovies_allRatingCategoriesPresent_returnsPageOfReviews() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        ReviewCriteriaDto reviewCriteriaDto = TestDataFactory.reviewCriteriaDto1;
        Page<UUID> similarMovies = new PageImpl<>(TestDataFactory.similarMovieUuids);
        Mockito.when(reviewService.getReviewsOfSimilarMovies(Mockito.eq(reviewCriteriaDto), Mockito.any(Pageable.class))).thenReturn(similarMovies);

        // Act
        Page<UUID> foundMovies = reviewFacade.getReviewsOfSimilarMovies(reviewCriteriaDto, PageRequest.of(0, 20));

        // Assert
        assertThat(foundMovies).isEqualTo(similarMovies);
    }

    @Test
    void getMovieAverageRating_movieFound_returnsReviewRatingDto() {
        // Arrange
        Mockito.when(reviewService.getMovieAverageRating(Mockito.any(UUID.class))).thenReturn(TestDataFactory.reviewRatingDto1);

        // Act
        ReviewRatingDto dto = reviewFacade.getMovieAverageRating(UUID.randomUUID());

        // Assert
        assertThat(dto.getExperienceRating()).isCloseTo(6.2, Offset.offset(0.1));
        assertThat(dto.getScriptRating()).isCloseTo(7.1, Offset.offset(0.1));
        assertThat(dto.getFootageRating()).isCloseTo(6.3, Offset.offset(0.1));
        assertThat(dto.getNoveltyRating()).isCloseTo(6.5, Offset.offset(0.1));
        assertThat(dto.getOverallRating()).isCloseTo(7.0, Offset.offset(0.1));
    }

    @Test
    void getMovieAverageRating_movieNotFound_throwsResourceNotFoundException() throws Exception {
        // Arrange
        Mockito.when(reviewService.getMovieAverageRating(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException());

        // Act
        // Assert
        assertThrows(ResourceNotFoundException.class, () -> reviewFacade.getMovieAverageRating(UUID.randomUUID()));
    }

    @Test
    void create_validInput_returnsReview() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        ReviewDto reviewDto = TestDataFactory.reviewDto1;
        ReviewCreateDto reviewCreateDto = TestDataFactory.reviewCreateDto1;
        Mockito.when(reviewMapper.mapToEntity(reviewCreateDto)).thenReturn(review);
        Mockito.when(reviewService.create(review)).thenReturn(review);
        Mockito.when(reviewMapper.mapToDto(review)).thenReturn(reviewDto);

        // Act
        ReviewDto createdDto = reviewFacade.create(reviewCreateDto);

        // Assert
        assertThat(createdDto).isEqualTo(reviewDto);
    }

    @Test
    void update_validInput_returnsReview() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        ReviewDto reviewDto = TestDataFactory.reviewDto1;
        Mockito.when(reviewMapper.mapToEntity(reviewDto)).thenReturn(review);
        Mockito.when(reviewService.update(review)).thenReturn(review);
        Mockito.when(reviewMapper.mapToDto(review)).thenReturn(reviewDto);

        // Act
        ReviewDto updatedEntity = reviewFacade.update(reviewDto.getId(), reviewDto);

        // Assert
        assertThat(updatedEntity).isEqualTo(reviewDto);
        Mockito.verify(reviewService, Mockito.times(1)).update(review);
    }

    @Test
    void delete_validInput_noReturn() {
        // Arrange
        Review review = TestDataFactory.reviewEntity1;
        UUID id = UUID.fromString("52657669-6577-5265-706f-7369746f7279");
        Mockito.when(reviewService.findById(id)).thenReturn(review);
        Mockito.doNothing().when(reviewService).delete(review);

        // Act
        reviewFacade.delete(id);

        // Assert
        Mockito.verify(reviewService, Mockito.times(1)).delete(review);
    }

    @Test
    void delete_noReturn() {
        // Arrange
        Mockito.doNothing().when(reviewService).deleteAll();

        // Act
        reviewFacade.deleteAll();

        // Assert
        Mockito.verify(reviewService, Mockito.times(1)).deleteAll();
    }
}
