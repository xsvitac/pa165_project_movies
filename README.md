# pa165_project_movies

This repository contains a project for the PA165 course at FI MUNI.

## Movies Recommender Catalogue

### Authors

- Antonín Svitáček (@xsvitac)
- David Pečenka (@xpecenka)
- Petr Kabelka (@xkabelka) -- *Team Leader*

### Assignment

The web application is a catalogue of movies of different genres. Each movie
has a title, description, film director, and list of actors, and image(s) from
the main playbill. Each movie is categorized in one or more genres. Only the
administrator can create, remove and update the list of movies. Users can rate
the movies according to different criteria (e.g how novel are the ideas of the
movie, their final score, etc…). The main feature of the system is that users
can pick one movie and get the list of similar movies and / or movies that were
liked the most by other users watching the same movie (no need of complex
algorithms, some simple recommendation is enough!).

## Modules

### [common](./common)

*Non-runnable* module containing shared functionality for other modules.

### [movie](./movie)

Microservice for managing movies, movie categories and pictures (e.g. poster).

### [personnel](./personnel)

Microservice for managing people involved in the movie's making and their roles
(director, actor, producer, etc.).

### [review](./review)

Microservice for writing and managing user reviews for movies. This module
contains the code for recommending similar movies based on rating.

### [oauth2-client](./oauth2-client)

Microservice providing OAuth 2 login via MUNI Unified Login. The module allows
to view the access token which can be used when calling authenticated methods
in other modules.

## Roles

The application contains these authorized user roles:

- **User**: Can rate movies. This role is represented by a user with the scopes
  `test_read` and `test_write`.
- **Admin**: Can edit the list of movies, categories and people involved in a
  movie's creation. This role is represented by a user with the scope `test_1`.

## Diagrams

### Use case diagram

![Use case diagram](./diagrams/use_case_diagram/use_case_diagram.png)

### DTO class diagram

![DTO class diagram](./diagrams/dto_class_diagram/dto_class_diagram.png)

### Entity Relationship Diagram

![Entity Relationship Diagram](./diagrams/movie_catalogue_ERD/movie_catalogue_ERD.png)

## Build and run the application

To build the application, run the following command in the project root:

```sh
mvn clean install
```

To run all tests, execute the following command in the project root:

```sh
mvn test
```

### Run without Docker

To start a module, enter its directory and run:

```sh
mvn spring-boot:run
```

### Run with Docker

Build all module images from the project root (make sure `mvn clean install`
finished without errors):

```sh
docker compose build
```

Run all module containers from the project root:

```sh
docker compose up
# or run in background:
docker compose up -d
```

Stop all module containers from the project root:

```sh
docker compose down
```

## Database seeding and clearing

Each module seeds its data automatically upon start. Its data can be cleared
using the `/api/v1/dbclear` endpoint.

## Observability
Prometheus and Grafana are used to gather and view metrics.
They are running by default using `docker compose`.

Prometheus is available at http://localhost:9090.

Grafana is available at http://localhost:3000.

## Scenarios

Before running scenarios, the modules must be running and be seeded.  Scenarios
are performed by *Locust* and can be run in the [scenarios](./scenarios)
directory with (it's necessary to specify the host on which the modules are
running):

```sh
locust --headless --host http://localhost
```

There are scenarios for:

- Retrieving a random movie
- Retrieving reviews of that movie
- Retrieving the average rating of that movie
- Searching similar movies based on the averege rating

All scenarios are testing successful operations.
